package com.pts.j4u.util;

import org.springframework.web.multipart.MultipartFile;

public interface HandleFile {
    String upload(MultipartFile file);

    String uploadFilePDF(MultipartFile file, Long candidateId);
}
