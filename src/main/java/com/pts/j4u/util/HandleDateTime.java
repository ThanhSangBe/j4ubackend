package com.pts.j4u.util;

import java.time.LocalDate;

public interface HandleDateTime {
    void validStartEndTime(String start, String end);

    LocalDate parse(String date);

    String parse(LocalDate localDate);
}
