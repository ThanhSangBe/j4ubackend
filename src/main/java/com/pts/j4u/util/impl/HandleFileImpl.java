package com.pts.j4u.util.impl;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.pts.j4u.common.TypeError;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.util.HandleFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Map;

@Component
public class HandleFileImpl implements HandleFile {
    @Autowired
    private Cloudinary cloudinary;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ServletContext application;


    @Override
    public String upload(MultipartFile file) {
        Map map;
        String url = "";
        try {
            map = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
            url = map.getOrDefault("secure_url", "URL Default").toString();
        } catch (IOException exception) {
            throw new ErrorMessageException(this.messageSource.getMessage("FailUpload", null, null), TypeError.BadRequest);
        }
        return url;
    }

    @Override
    public String uploadFilePDF(MultipartFile file, Long candidateId) {
        String path = application.getRealPath("/");
        String originalNameFile = file.getOriginalFilename();
        String nameFolderPDF = "/upload/";
        File folderPdf = new File(path + nameFolderPDF);
        if (!folderPdf.exists()) {
            folderPdf.mkdir();
        }
        String attributeFile = originalNameFile.substring(originalNameFile.lastIndexOf("."));
        String nameFileNew = getCurrentTime() + attributeFile;
        //Chi dinh duong dan va ten file
        Path savePath = Paths.get(folderPdf.getPath() + "\\" + nameFileNew);
        try (InputStream inputStream = file.getInputStream()) {
            //Luu file
            Files.copy(inputStream, savePath);
        } catch (IOException e) {
            throw new ErrorMessageException("Handle file is failed!", TypeError.BadRequest);
        }
        return nameFileNew;
    }

    public String getCurrentTime() {
        LocalDateTime dateTime = LocalDateTime.now();
        return String.format("%s%s%s-%s%s%s",
                dateTime.getYear(), dateTime.getMonth().getValue(), dateTime.getDayOfMonth(),
                dateTime.getHour(), dateTime.getMinute(), dateTime.getSecond());
    }
}
