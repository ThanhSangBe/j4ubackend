package com.pts.j4u.util.impl;

import com.pts.j4u.common.Constant;
import com.pts.j4u.common.MailConstant;
import com.pts.j4u.util.MailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class MailUtilImpl implements MailUtil {
    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void sendMailForgotPassword(String email, String verifyCode) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        mimeMessageHelper.setTo(email);
        mimeMessageHelper.setSubject(MailConstant.getSubjectForgotPassword());
        String link = String.format("%s/api/user/verify-code?email=%s&verifyCode=%s", Constant.BASE_URL, email, verifyCode);
        String bodyCode = MailConstant.getCodeForgotPasswordHtml(link);
        mimeMessageHelper.setText(bodyCode, true);
        javaMailSender.send(mimeMessage);
    }

    @Override
    public void sendMailVerify(String email, String verifyCode) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        mimeMessageHelper.setTo(email);
        mimeMessageHelper.setSubject(MailConstant.getSubjectVerifyEmail());
        String link = String.format("%s/api/user/verify-email?email=%s&verifyCode=%s", Constant.BASE_URL, email, verifyCode);
        String bodyCode = MailConstant.getCodeVerifyPassword(link);
        mimeMessageHelper.setText(bodyCode, true);
        javaMailSender.send(mimeMessage);
    }
}
