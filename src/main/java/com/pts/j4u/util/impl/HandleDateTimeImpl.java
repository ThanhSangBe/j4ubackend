package com.pts.j4u.util.impl;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.util.HandleDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Component
public class HandleDateTimeImpl implements HandleDateTime {
    @Autowired
    private MessageSource messageSource;
    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    @Override
    public void validStartEndTime(String start, String end) {
        try {
            LocalDate dateStart;
            if (start == null || start.equals("")) {
                dateStart = LocalDate.now();
                start = dateStart.format(DATE_FORMAT);
            }
            else
                dateStart = LocalDate.parse(start, DATE_FORMAT);
            LocalDate dateEnd = LocalDate.parse(end, DATE_FORMAT);
            if (!dateEnd.isAfter(dateStart))
                throw new ErrorMessageException(this.messageSource.getMessage("DateStartNotMatchDateEnd", null, null),
                        TypeError.BadRequest
                );
        } catch (DateTimeParseException ex) {
            throw new ErrorMessageException(
                    String.format(this.messageSource.getMessage("valid.datetime.format", null, null), start +" - " + end)
                    , TypeError.BadRequest);
        }
    }

    @Override
    public LocalDate parse(String date) {
        try {
            return LocalDate.parse(date, DATE_FORMAT);
        } catch (DateTimeParseException ex) {
            throw new ErrorMessageException(
                    String.format(this.messageSource.getMessage("valid.datetime.format", null, null), date)
                    , TypeError.BadRequest);
        }
    }

    @Override
    public String parse(LocalDate localDate) {
        return localDate.format(DATE_FORMAT);
    }

    public String parse(LocalDateTime localDate) {
        return localDate.format(DATE_FORMAT);
    }
}
