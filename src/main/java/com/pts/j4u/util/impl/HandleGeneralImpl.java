package com.pts.j4u.util.impl;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.PaginationDTO;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.util.HandleGeneral;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class HandleGeneralImpl implements HandleGeneral {
    @Autowired
    private MessageSource messageSource;
    public final String PATTERN_PASSWORD = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$";
    @Override
    public void validationPassword(String password)
    {
        boolean isPass = password.matches(PATTERN_PASSWORD);
        if(!isPass)
            throw new ErrorMessageException(this.messageSource.getMessage("valid.password",null,null), TypeError.BadRequest);
    }
}
