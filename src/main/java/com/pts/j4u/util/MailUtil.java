package com.pts.j4u.util;

import javax.mail.MessagingException;

public interface MailUtil {
    void sendMailForgotPassword(String to, String email) throws MessagingException;

    void sendMailVerify(String email, String verifyCode) throws MessagingException;
}
