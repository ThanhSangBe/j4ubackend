package com.pts.j4u.util.social;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pts.j4u.dto.GoogleAccountDTO;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public interface GoogleUtil {


    String getToken(String code) throws ClientProtocolException, IOException;

    GoogleAccountDTO getUserInfo(String accessToken) throws ClientProtocolException, IOException;

    UserDetails buildUser(GoogleAccountDTO googleAccount);
}
