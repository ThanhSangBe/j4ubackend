package com.pts.j4u.util.social.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pts.j4u.dto.GoogleAccountDTO;
import com.pts.j4u.util.social.GoogleUtil;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class GoogleUtilImpl implements GoogleUtil {
    @Value("${google.link.get.token}")
    private String link;
    @Value("${google.app.secret}")
    private String clientSecret;
    @Value("${google.redirect.uri}")
    private String redirectURI;
    @Value("${google.app.id}")
    private String clientId;
    @Value("${google.link.get.user_info}")
    private String linkGetUserInfo;

    @Override
    public String getToken(final String code) throws ClientProtocolException, IOException {
        String response = Request.Post(link).bodyForm(Form.form().add("client_id", clientId)
                        .add("client_secret", clientSecret)
                        .add("redirect_uri", redirectURI)
                        .add("code", code)
                        .add("grant_type", "authorization_code").build())
                .execute().returnContent().asString();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(response).get("access_token");
        return node.textValue();
    }

    @Override
    public GoogleAccountDTO getUserInfo(String accessToken) throws ClientProtocolException, IOException {
        String urlGetUserInfo = linkGetUserInfo + accessToken;
        String response = Request.Get(urlGetUserInfo).execute().returnContent().asString();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        GoogleAccountDTO googleAccount = mapper.readValue(response, GoogleAccountDTO.class);
        return googleAccount;
    }

    @Override
    public UserDetails buildUser(GoogleAccountDTO googleAccount) {
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        return new User(googleAccount.getEmail(), "", enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }
}
