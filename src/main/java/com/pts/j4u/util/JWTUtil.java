package com.pts.j4u.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

public class JWTUtil {
	private static final String keyAlgorithm = "phamthanhsang";
	public static final Algorithm ALGORITHM;
	private static final String ROLES;
	private static final String EMAIL;
	static {
		ALGORITHM = Algorithm.HMAC256(keyAlgorithm.getBytes());
		ROLES = "roles";
		EMAIL = "email";
	}

	public static String createToken() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String access_token = JWT.create().withSubject(user.getUsername())
				.withClaim(ROLES,user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.sign(ALGORITHM);
		return access_token;
	}

	public static String createVerifyCode(String email) {
		long timeAccessToken = 30*60*1000;
		String verifyCode = JWT.create()
				.withSubject(email)
				.withExpiresAt(new Date(timeAccessToken)) 
				.withClaim(EMAIL, email)
				.sign(ALGORITHM);
		return verifyCode;
	}

	public static boolean vertify(String token) {
		try {
			DecodedJWT decodedJWT = preVerifyToken(token);
			String username = decodedJWT.getSubject();
			String[] roles = decodedJWT.getClaim(ROLES).asArray(String.class);
			Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
			addRole(authorities, roles);
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username,
					null, authorities);
			SecurityContextHolder.getContext().setAuthentication(authenticationToken);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static DecodedJWT preVerifyToken(String token) throws Exception
	{
		JWTVerifier verifier = JWT.require(ALGORITHM).build();
		return verifier.verify(token);
	}


	private static void addRole(Collection<SimpleGrantedAuthority> authorities, String[] roles) {
		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
	}

}
