package com.pts.j4u.exception;



import com.pts.j4u.common.TypeError;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorMessageException extends RuntimeException {
	private TypeError typeError;
	private String message;

	public ErrorMessageException(String msg) {
		super(msg);
	}

	public ErrorMessageException(String message, TypeError typeError) {
		this.typeError = typeError;
		this.message = message;
	}
}
