package com.pts.j4u.exception;

import java.util.Map;

import lombok.Data;

@Data
public class ErrorValidationException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private Map<String, String> valids;
	
	public ErrorValidationException(String msg)
	{
		super(msg);
	}
	public ErrorValidationException(Map<String, String> valids)
	{
		this.valids=valids;
	}

}
