package com.pts.j4u.exception;

import lombok.Data;

@Data
public class ResponeErrorMessage {
	private int httpCode;
	private String message;
	private String path;
	public ResponeErrorMessage()
	{
		
	}
	public ResponeErrorMessage(int code, String message)
	{
		this.httpCode =  code;
		this.message =  message;
	}
	public ResponeErrorMessage(int code, String message, String path)
	{
		this.httpCode =  code;
		this.message =  message;
		this.path = path;
	}
}
