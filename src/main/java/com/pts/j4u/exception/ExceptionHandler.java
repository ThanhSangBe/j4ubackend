package com.pts.j4u.exception;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandler {
	@Autowired
	private MessageSource messageSource;

	@org.springframework.web.bind.annotation.ExceptionHandler(value = ErrorMessageException.class)
	public ResponseEntity<?> handlerNotFound(ErrorMessageException ex, HttpServletRequest request,
			HttpServletResponse response) {

		switch (ex.getTypeError()) {
		case NotFound:
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
					new ResponeErrorMessage(HttpStatus.NOT_FOUND.value(), ex.getMessage(), request.getServletPath()));
		case InternalServerError:
			String message = messageSource.getMessage("InternalServerError", null, null);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponeErrorMessage(
					HttpStatus.INTERNAL_SERVER_ERROR.value(), message, request.getServletPath()));
		case BadRequest:
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
					new ResponeErrorMessage(HttpStatus.BAD_REQUEST.value(), ex.getMessage(), request.getServletPath()));
		case NoContent:
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(
					new ResponeErrorMessage(HttpStatus.NO_CONTENT.value(), ex.getMessage(), request.getServletPath()));
		default:
			return null;
		}
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = ErrorValidationException.class)
	public ResponseEntity<?> throwExceptionRegister(ErrorValidationException ex) {
		Map<String, String> maps = ex.getValids();
		return ResponseEntity.badRequest().body(maps);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public Map<String, String> handlerValidation(MethodArgumentNotValidException ex, HttpServletRequest request) {
		Map<String, String> maps = new HashMap<String, String>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String field = ((FieldError) error).getField();
			String msg = error.getDefaultMessage();
			maps.put(field, msg);
		});
		maps.put("path", request.getServletPath());
		return maps;
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(value = AuthenticationException.class)
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	public ResponseEntity<?> handleAuthenticationFaile() {
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(new ResponeErrorMessage(HttpStatus.UNAUTHORIZED.value(),
						messageSource.getMessage("AuthenticationException", null, null), "/api/login"));

	}
	@org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<?> handleExceptionGenerate(Exception ex, HttpServletRequest request) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(new ResponeErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
						ex.getMessage(), request.getServletPath()));

	}
	

}
