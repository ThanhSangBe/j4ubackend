package com.pts.j4u.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.pts.j4u.entity.User;
import com.pts.j4u.service.UserService;
@Component("CustomAuthenticationManager")
public class CustomAuthenticationManager implements AuthenticationManager{
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private UserService userService;
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		String password = authentication.getPrincipal().toString();
		User user = userService.getAndComparePassword(username, password);
		return new UsernamePasswordAuthenticationToken(user.getUsername(), null);
	}

}
