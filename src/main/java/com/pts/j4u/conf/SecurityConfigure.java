package com.pts.j4u.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder.BCryptVersion;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.pts.j4u.common.CloudinaryCommon;

@Configuration
@EnableWebMvc
public class SecurityConfigure extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder(BCryptVersion.$2Y,15);
	}
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		CustomAuthenticationFilter customAuthentication = new CustomAuthenticationFilter(authenticationManagerBean());
//		customAuthentication.setFilterProcessesUrl("/api/login");
//		http.csrf().disable();
//		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//		http.authorizeRequests().antMatchers("/**").permitAll();
//		http.addFilter(customAuthentication);
//		http.addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
		http.cors().and().authorizeRequests().anyRequest().permitAll().and().csrf().disable();
		http.addFilterBefore(new CustomAuthorizationFilter(), SecurityContextHolderAwareRequestFilter.class);

	}
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	@Bean
	public CorsFilter corsFilter()
	{
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.addAllowedOrigin("*");
		configuration.addAllowedHeader("*");
		configuration.addAllowedMethod("OPTIONS");
		configuration.addAllowedMethod("HEAD");
		configuration.addAllowedMethod("GET");
		configuration.addAllowedMethod("PUT");
		configuration.addAllowedMethod("POST");
		configuration.addAllowedMethod("DELETE");
		configuration.addAllowedMethod("PATCH");
		source.registerCorsConfiguration("/**", configuration);
		return new CorsFilter(source);
	}
	
	 @Bean
	    public Cloudinary cloudinary() {
	        Cloudinary cloudinary = new Cloudinary(
	                ObjectUtils.asMap(
	                       CloudinaryCommon.CLOUD_NAME,CloudinaryCommon.MY_CLOUD_NAME,
	                       CloudinaryCommon.API_KEY,CloudinaryCommon.MY_API_KEY,
	                       CloudinaryCommon.API_SECRET,CloudinaryCommon.MY_API_SECRET,
	                       CloudinaryCommon.SECURE, CloudinaryCommon.IS_SECURE
	                ));
	        return cloudinary;
	    }
}
