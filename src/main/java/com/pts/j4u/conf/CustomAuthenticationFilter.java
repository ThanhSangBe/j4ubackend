package com.pts.j4u.conf;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.pts.j4u.entity.Role;
import com.pts.j4u.entity.User;
import com.pts.j4u.repository.RoleRepository;
import com.pts.j4u.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class CustomAuthenticationFilter {
    //	@Resource(name = "CustomAuthenticationManager")
//	private AuthenticationManager authenticationManager;
//
//	public CustomAuthenticationFilter(AuthenticationManager authenticationManager) {
//		this.authenticationManager = authenticationManager;
//	}
//
//	@Override
//	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
//			throws AuthenticationException {
//		String username = request.getParameter("username");
//		String password = request.getParameter("password");
//		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username,
//				password);
//		return authenticationManager.authenticate(authenticationToken);
//	}
//
//	@Override
//	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
//			Authentication authResult) throws IOException, ServletException {
//		Map<String, String> maps = new HashMap<String, String>();
//		maps.put("message", "Login success");
//		maps.put("code", String.valueOf(HttpStatus.OK.value()));
//		maps.put("username", authResult.getPrincipal().toString());
//		maps.put("accessToken", JWTUtil.createToken());
//		response.setContentType("APPLICATION_JSON_VALUE");// SET BODY JSON, NEU KHONG SET MAC DINH SE LA TEXT
//		response.setStatus(HttpStatus.OK.value());
//		new ObjectMapper().writeValue(response.getOutputStream(), maps);// GHI RA BODY
//	}
//
//	@Override
//    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
//                                              AuthenticationException failed) throws IOException, ServletException {
//        Map<String, String> maps = new HashMap<String, String>();
//        String messageFailLogin = "Username or password is incorrect.";
//        maps.put("message", messageFailLogin);
//        maps.put("code", String.valueOf(HttpStatus.UNAUTHORIZED.value()));
//        response.setContentType("APPLICATION_JSON_VALUE");// SET BODY JSON, NEU
//        response.setStatus(HttpStatus.UNAUTHORIZED.value());
//        new ObjectMapper().writeValue(response.getOutputStream(), maps);// GHI RA BODY
//    }

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;

    public Map<String, Object> authentication(String username, String password) {
        //find user by username
        Optional<User> userOptional = userService.getOptionalByUserName(username);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            String passwordEntity = user.getPassword();
            boolean isCorrectPassword = passwordEncoder.matches(password, passwordEntity);
            if (isCorrectPassword) //oke
            {
                Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
                List<Role> roles = roleRepository.getByUserId(user.getId());
                roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(),
                        null, authorities);
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                return handleSuccessLogin(user);
            }
        }
        return handleUnsuccessfullyLogin();
    }

    private Map<String, Object> handleUnsuccessfullyLogin() {
        Map<String, Object> maps = new HashMap<>();
        String messageFailLogin = "Username or password is incorrect.";
        maps.put("message", messageFailLogin);
        maps.put("code", String.valueOf(HttpStatus.UNAUTHORIZED.value()));
        return maps;
//        response.setContentType("APPLICATION_JSON_VALUE");// SET BODY JSON, NEU
//        response.setStatus(HttpStatus.UNAUTHORIZED.value());
//       	return new ObjectMapper().writeValue(response.getOutputStream(), maps);// GHI RA BODY
    }

    public Map<String, Object> handleSuccessLogin(User user) {
        Map<String, Object> maps = new HashMap<>();

        maps.put("message", "Login success");
        maps.put("code", String.valueOf(HttpStatus.OK.value()));
        maps.put("username", user.getUsername());
        long timeAccessToken = System.currentTimeMillis() + 30 * 60 * 1000;//HSD 30p
        long timeRefreshToken = System.currentTimeMillis() + 10 * 24 * 60 * 60 * 1000;//HSD 10 ngay
        //create token
//        String userNameAuth = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userNameAuth = user.getUsername();
        String accessToken = JWT.create().withSubject(userNameAuth)
                .withClaim("roles ", SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .withExpiresAt(new Date(timeAccessToken))
                .sign(Algorithm.HMAC256("phamthanhsang".getBytes()));
        String refreshToken = JWT.create().withSubject(userNameAuth)
                .withClaim("roles ", SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .withExpiresAt(new Date(timeRefreshToken))
                .sign(Algorithm.HMAC256("phamthanhsang".getBytes()));
        maps.put("accessToken", accessToken);
        maps.put("refreshToken", refreshToken);
        maps.put("id", user.getId());
        maps.put("roles", roleRepository.getNameByUserId(user.getId()));
        return maps;
    }
}
