package com.pts.j4u.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name ="job")
public class Job {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false, columnDefinition = "TEXT(1000)")
	private String name;
	@Column(columnDefinition = "TEXT(10000)")
	private String description;
	@Column(name = "requirement",nullable = true, columnDefinition = "TEXT(5000)")
	private String requirement;
	@Column(name = "other_info",columnDefinition = "TEXT(5000)")
	private String ortherInfo;
	private Long amount;
	@Column(name = "start",columnDefinition = "DATE")
	private LocalDate start;
	@Column(name = "end",columnDefinition = "DATE")
	private LocalDate end;
	private Long salary;
	@Column(length = 500)
	private String files;
	@Column(columnDefinition = "DATETIME")
	private LocalDateTime createDate;
	@Column(columnDefinition = "DATETIME")
	private LocalDateTime modifiedDate;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private Company company;
	@OneToOne
	@JoinColumn(nullable = false)
	private Status status;
	@OneToMany(mappedBy = "job",orphanRemoval = true, cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JsonIgnore
	private List<ApplyJob> applyJobs = new ArrayList<ApplyJob>();
	@OneToMany(mappedBy = "job",orphanRemoval = true, cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JsonIgnore
	private List<JobTypeJob> jobTypeJobs = new ArrayList<>();
}
