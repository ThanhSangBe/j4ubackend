package com.pts.j4u.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class ApplyDemand {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(columnDefinition = "DATETIME")
	private LocalDateTime timeApply;
	@Column(columnDefinition = "TEXT(5000)")
	private String note;
	@ManyToOne
	@JoinColumn(nullable = false)
	private Company company;
	@ManyToOne
	@JoinColumn(nullable = false)
	private Demand demand;
}
