package com.pts.j4u.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "demand")
public class Demand {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(nullable = false,length = 1000)
	private String name;
	@Column(nullable = false, columnDefinition = "TEXT(5000)")
	private String desciption;
	private long amount;
	@Column(name = "requirement",nullable = true, columnDefinition = "TEXT(5000)")
	private String requirement;
	@Column(name = "other_info",columnDefinition = "TEXT(5000)")
	private String ortherInfo;
	@Column(name = "start",columnDefinition = "DATE")
	private LocalDate start;
	@Column(name = "end",columnDefinition = "DATE")
	private LocalDate end;
	@Column(length = 500)
	private String files;
	@Column(columnDefinition = "DATE")
	private LocalDate createDate;
	@Column(columnDefinition = "DATE")
	private LocalDate modifiedDate;
	@ManyToOne
	@JoinColumn(nullable = false)
	private University university;
	@OneToOne
	@JoinColumn(nullable = false)
	private Status status;
}
