package com.pts.j4u.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "like_job")
public class LikeJob {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(columnDefinition = "DATETIME")
    private LocalDateTime timeLike;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Candidate candidate;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Job job;
}
