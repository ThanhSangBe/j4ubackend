package com.pts.j4u.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "company")
public class Company {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "name", length = 500, nullable = false)
	private String name;
	@Column(name = "logo", length = 500)
	private String logo;
	@Column(name = "code", length = 150)
	private String code;
	@Column(name = "description", length = 10000, nullable = false)
	private String description;
	@Column(name = "website", length = 200, nullable = false)
	private String website;
	@Column(name = "email", length = 200, nullable = false, unique = true)
	private String email;
	@Column(name = "phone", length = 50)
	private String phone;
	@Column(name = "tax", length = 100, unique = false)
	private String tax;
	@Column(name = "crate_date", columnDefinition = "DATE")
	private LocalDate createDate;
	@Column(name = "modified_date", columnDefinition = "DATE")
	private LocalDate modifiedDate;
	@OneToOne
	@JoinColumn(nullable = false)
	private Status status;
	@OneToMany(mappedBy = "company",orphanRemoval = true,fetch = FetchType.LAZY, cascade = {CascadeType.MERGE,CascadeType.PERSIST})
	@JsonIgnore
	private List<User> users = new ArrayList<>();
	@OneToMany(mappedBy = "company",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE,orphanRemoval = true)
	@JsonIgnore
	private List<CompanyLocation> companyLocations = new ArrayList<CompanyLocation>();
	@OneToMany(mappedBy = "company",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE,orphanRemoval = true)
	@JsonIgnore
	private List<Job> jobs = new ArrayList<Job>();
	@OneToMany(mappedBy = "company",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE,orphanRemoval = true)
	@JsonIgnore
	private List<ApplyDemand> applyDemands = new ArrayList<ApplyDemand>();
}
