package com.pts.j4u.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "university")
public class University {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "name", nullable = false, length = 1000)
	private String name;
	@Column(name = "avatar", nullable = false, length = 500)
	private String avatar;
	@Column(name = "ShortName", nullable = false, length = 100, unique = true)
	private String shortName;
	@Column(name = "description", length = 10000)
	private String description;
	@Column(name = "website", length = 500, nullable = false)
	private String website;
	@Column(name = "email", nullable = false, length = 200)
	private String email;
	@Column(name = "phone", length = 50)
	private String phone;
	@Column(columnDefinition = "DATE")
	private LocalDate createDate;
	@Column(columnDefinition = "DATE")
	private LocalDate modifiedDate;
	
	@OneToOne
	@JoinColumn
	private Status status;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(nullable = true)
	private User user;

	@OneToMany(mappedBy = "university",fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true)
	@JsonIgnore
	private List<UniversityLocation> universityLocations = new ArrayList<UniversityLocation>();
	
	@OneToMany(mappedBy = "university",fetch = FetchType.LAZY,cascade = CascadeType.ALL,orphanRemoval = true)
	@JsonIgnore
	private List<Demand> demands = new ArrayList<Demand>();
	
	
}
