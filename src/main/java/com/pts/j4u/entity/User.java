package com.pts.j4u.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "username", unique = true, nullable = false, length = 500)
	private String username;
	@Column(name = "password", nullable = false, length = 500)
	private String password;
	@Column(name = "gender", columnDefinition = "TINYINT")
	private boolean gender;
	@Column(name = "avatar", length = 200)
	private String avatar;
	@Column(name = "phone", length = 50)
	private String phone;
	private String firstName;
	private String lastName;
	@Column(name = "email", length = 200)
	private String email;
	@Column(name = "create_Date", columnDefinition = "DATE")
	private LocalDate createDate;
	@Column(name = "modified_Date", columnDefinition = "DATE")
	private LocalDate modifiedDate;
	private boolean verifyEmail;
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status_id")
	private Status status;
	@OneToMany(mappedBy = "user", orphanRemoval = true, cascade = CascadeType.ALL)
	@JsonIgnore
	private List<UserRole> users = new ArrayList<UserRole>();
	@ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE,CascadeType.PERSIST})
	@JoinColumn(nullable = false)
	@JsonIgnore
	private Company company;
}
