package com.pts.j4u.service;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.PaginationDTO;
import com.pts.j4u.dto.UniversityCreateDTO;
import com.pts.j4u.dto.UniversityDTO;
import com.pts.j4u.dto.UserRegisterDTO;
import com.pts.j4u.entity.University;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UniversityService {

    UniversityDTO save(UniversityDTO dto);

    PaginationDTO findAll(int no, int limit);

    List<UniversityDTO> findAll();

    UniversityDTO changeStatus(Long id, StatusEnum status);

    UniversityDTO save(UniversityCreateDTO universityCreateDTO);

    UniversityDTO findById(Long id);

	UniversityDTO update(Long id, UniversityDTO dto);

    UniversityCreateDTO readJson(String university, MultipartFile file);

	void disableListId(List<Long> listId);

	void activeListId(List<Long> listId);

	void deleteListId(List<Long> listId);

	void deleteForce(List<Long> listId);

	PaginationDTO searchByNameAndStatus(int no, int limit, String name, int statusId);

	University getById(Long id);

    UniversityCreateDTO readJson(String university);

    UniversityDTO getByUserId(long userId);

    UniversityDTO register(UniversityCreateDTO universityCreateDTO, UserRegisterDTO userRegisterDTO);
}
