package com.pts.j4u.service;

import java.util.List;

import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.entity.Location;
import com.pts.j4u.entity.University;
import com.pts.j4u.entity.UniversityLocation;

public interface UniversityLocationService {

	List<LocationDTO> findByUniversity(Long id);

	List<UniversityLocation> save(List<LocationDTO> locations, University university);

	UniversityLocation save(LocationDTO location, University university);

	void deleteByUniversity(Long id);

}
