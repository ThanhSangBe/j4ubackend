package com.pts.j4u.service;

import java.util.List;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.DemandDTO;
import com.pts.j4u.dto.PaginationDTO;

public interface DemandService {

	DemandDTO save(DemandDTO demandDTO);

    DemandDTO findById(Long id);

    void changeStatusListId(List<Long> listId, StatusEnum statusEnum);

	List<DemandDTO> filter();

	DemandDTO update(DemandDTO demandDTO, long demandId);

    PaginationDTO findByUniversityId(Long id);
}
