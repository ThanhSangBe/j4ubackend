package com.pts.j4u.service;

import com.pts.j4u.entity.Role;
import com.pts.j4u.entity.User;
import com.pts.j4u.entity.UserRole;

public interface UserRoleService {

	UserRole save(User user, Role role);

	void deleteByUserId(Long id);


}
