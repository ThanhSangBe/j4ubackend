package com.pts.j4u.service;

import java.util.List;

import com.pts.j4u.dto.DistrictDTO;
import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.dto.ProvinceDTO;
import com.pts.j4u.entity.Location;

public interface LocationService {

	Location update(Location location, Long id);

	boolean compareList(List<LocationDTO> dtos, List<LocationDTO> entities);

	List<LocationDTO> update(List<LocationDTO> locationDTOs);

    List<ProvinceDTO> getProvinces();

    List<DistrictDTO> getDistrictsByProvincesId(long id);

    void setIdDTO(List<LocationDTO> dto, List<LocationDTO> entity);

}
