package com.pts.j4u.service;

import com.pts.j4u.dto.ApplyJobDTO;

public interface ApplyJobService {

	ApplyJobDTO save(ApplyJobDTO applyJobDTO);

}
