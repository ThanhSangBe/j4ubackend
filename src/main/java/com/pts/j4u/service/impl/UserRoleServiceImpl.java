package com.pts.j4u.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.entity.Role;
import com.pts.j4u.entity.User;
import com.pts.j4u.entity.UserRole;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.repository.UserRoleRepository;
import com.pts.j4u.service.UserRoleService;
@Service
public class UserRoleServiceImpl implements UserRoleService {
	@Autowired
	private UserRoleRepository userRoleRepository;
	@Autowired
	private MessageSource messageSource;
	
	@Override
	public UserRole save(User user, Role role)
	{
		UserRole userRole = new UserRole();
		userRole.setUser(user);
		userRole.setRole(role);
		return this.userRoleRepository.save(userRole);
	}
	@Override
	public void deleteByUserId(Long id)
	{
		this.userRoleRepository.deleteByIdUser(id);
	}

}
