package com.pts.j4u.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.*;
import com.pts.j4u.entity.*;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.mapper.CandidateMapper;
import com.pts.j4u.mapper.JobMapper;
import com.pts.j4u.mapper.UserMapper;
import com.pts.j4u.repository.ApplyJobRepository;
import com.pts.j4u.repository.CandidateRepository;
import com.pts.j4u.repository.specification.CandidateSpecification;
import com.pts.j4u.service.CandidateService;
import com.pts.j4u.service.MajorService;
import com.pts.j4u.service.StatusService;
import com.pts.j4u.service.UserService;
import com.pts.j4u.util.HandleFile;
import com.pts.j4u.util.MailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CandidateServiceImpl implements CandidateService {
    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private HandleFile handleFile;
    @Autowired
    private MajorService majorService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CandidateMapper candidateMapper;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ApplyJobRepository applyJobRepository;
    @Autowired
    private JobMapper jobMapper;
    @Autowired
    private MailUtil mailUtil;
    @Autowired
    private StatusService statusService;

    @Override
    public CandidateDTO create(CandidateRegisterDTO candidateRegisterDTO) {
        UserRegisterDTO userRegisterDTO = candidateRegisterDTO.getUser();
        UserDTO userDTO = this.userService.register(userRegisterDTO);
        User user = userMapper.mapToEntity(userDTO);
        uploadFile(candidateRegisterDTO);
        Candidate candidate = candidateMapper.mapToEntity(candidateRegisterDTO);
        // set user
        candidate.setUser(user);
        // set major
        setMajor(candidate, candidateRegisterDTO);
        Candidate candidateSaved = candidateRepository.save(candidate);
        //send mail verify

        return candidateMapper.mapToDTO(candidateSaved);
    }

    @Override
    public CandidateDTO update(CandidateRegisterDTO candidateRegisterDTO, Long candidateId) {
        UserRegisterDTO userRegisterDTO = candidateRegisterDTO.getUser();
        Candidate candidate = getById(candidateId);
        User user = candidate.getUser();
        userMapper.mapToUpdate(user, userRegisterDTO);
        setTimeNow(user);
        //upload file CV
        uploadFile(candidateRegisterDTO);
        candidate.setCV(candidateRegisterDTO.getCv());
        // set major
        setMajor(candidate, candidateRegisterDTO);
        Candidate candidateupdated = candidateRepository.save(candidate);
        return candidateMapper.mapToDTO(candidateupdated);

    }

    private void setTimeNow(User user) {
        LocalDate localDate = LocalDate.now();
        user.setModifiedDate(localDate);
    }

    private void setMajor(Candidate candidate, CandidateRegisterDTO dto) {
        Major major = this.majorService.findById(dto.getMajor().getId());
        candidate.setMajor(major);
    }

    @Override
    public CandidateDTO getCandidateByUserId(long userId) {
        Optional<Candidate> candidate = this.candidateRepository.getByUserId(userId);
        if (!candidate.isPresent()) {
            throw new ErrorMessageException(String.format(this.messageSource.getMessage("NotFound", null, null), "Candidate", "id", userId),
                    TypeError.NotFound);
        }
        return this.candidateMapper.mapToDTO(candidate.get());
    }

    @Override
    public PaginationDTO search(String firstName, String lastName, int pageNumber, int pagteSize) {
        Pageable pageable = PageRequest.of(pageNumber, pagteSize);
        Page<Candidate> pageCandidate = candidateRepository.findAll(CandidateSpecification.search(firstName, lastName), pageable);
        return new PaginationDTO(pageCandidate.map(candidate -> this.candidateMapper.mapToDTO(candidate)));

    }

    @Override
    public CandidateDTO findById(Long id) {
        Candidate candidate = this.candidateRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(this.messageSource.getMessage("NotFound", null, null), "Candidate", "id", id.toString()),
                TypeError.NotFound));
        return candidateMapper.mapToDTO(candidate);
    }

    @Override
    public Candidate getById(Long id) {
        return this.candidateRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(this.messageSource.getMessage("NotFound", null, null), "Candidate", "id", id.toString()),
                TypeError.NotFound));
    }

    @Override
    public CandidateRegisterDTO readJson(String candidate, MultipartFile fileCV, MultipartFile fileAvatar) {
        CandidateRegisterDTO candidateRegisterDTO = new CandidateRegisterDTO();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            candidateRegisterDTO = objectMapper.readValue(candidate, CandidateRegisterDTO.class);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        if (fileCV != null)
            candidateRegisterDTO.setFileCV(fileCV);
        if (fileAvatar != null)
            candidateRegisterDTO.getUser().setFile(fileAvatar);
        return candidateRegisterDTO;
    }

    @Override
    public void deleteById(Long candidateId) {
        if (candidateId != null && candidateId > 0) {
            Candidate candidate = getById(candidateId);
            this.candidateRepository.deleteById(candidate.getId());
        } else throw new ErrorMessageException("Candidate ID is null, cannot delete!", TypeError.BadRequest);
    }

    private void uploadFile(CandidateRegisterDTO candidateRegisterDTO) {
        if (candidateRegisterDTO.getFileCV() != null)
            candidateRegisterDTO.setCv(this.handleFile.upload(candidateRegisterDTO.getFileCV()));
    }

    @Override
    public boolean checkHaveCV(Candidate candidate) {
        boolean haveCV = false;
        if (candidate.getCV() != null && !candidate.equals(""))
            haveCV = true;
        return haveCV;
    }

    @Override
    public List<CandidateDTO> findByFirstNameOrLastName(String name) {
        List<Candidate> candidates = this.candidateRepository.findByFirstNameOrLastName(name);
        return candidates.stream().map(candidate -> this.candidateMapper.mapToDTO(candidate)).collect(Collectors.toList());
    }

    @Override
    public List<CandidateDTO> getListCandidateAppliedByIdCompanyAndIdJob(Long idJob) {
        List<ApplyJob> applyJobs = this.applyJobRepository.findByJobId(idJob);
        if (applyJobs != null && applyJobs.size() > 0) {
            List<CandidateDTO> candidateDTOs = new ArrayList<CandidateDTO>();
            for (ApplyJob applyJob : applyJobs) {
                candidateDTOs.add(this.candidateMapper.mapToDTO(applyJob.getCandidate()));
            }
            return candidateDTOs;
        }
        return null;
    }

    @Override
    public List<JobDTO> getListJobByIdCandidate(Long idCandidate) {
        List<ApplyJob> applyJobs = this.applyJobRepository.findByIdCandidate(idCandidate);
        if (applyJobs != null && applyJobs.size() > 0) {
            List<JobDTO> jobSimpleDTOs = new ArrayList<JobDTO>();
            for (ApplyJob applyJob : applyJobs) {
                jobSimpleDTOs.add(this.jobMapper.mapToSimple(applyJob.getJob()));
            }
            return jobSimpleDTOs;
        }
        return null;
    }

    @Override
    public void uploadFileCv(Long candidateId, MultipartFile fileCV) {
        Candidate candidate = getById(candidateId);
        candidate.setCV(handleFile.uploadFilePDF(fileCV, candidateId));
        this.candidateRepository.save(candidate);
    }

    @Override
    public CandidateDTO updateRole(Long candidateId, Integer statusId) {
        Candidate candidate = getById(candidateId);
        Status status = statusService.findById(statusId);
        candidate.setStatus(status);
        return candidateMapper.mapToDTO(this.candidateRepository.save(candidate));
    }

}
