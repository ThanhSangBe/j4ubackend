package com.pts.j4u.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.entity.Location;
import com.pts.j4u.entity.University;
import com.pts.j4u.entity.UniversityLocation;
import com.pts.j4u.mapper.LocationMapper;
import com.pts.j4u.repository.UniversityLocationRepository;
import com.pts.j4u.service.UniversityLocationService;

@Service
public class UniversityLocationServiceImpl implements UniversityLocationService {
	@Autowired
	private UniversityLocationRepository universityLocationRepository;
	@Autowired
	private LocationMapper locationMapper;

	@Override
	public List<LocationDTO> findByUniversity(Long id) {
		List<UniversityLocation> universityLocations = this.universityLocationRepository.findByUniversityId(id);
		List<LocationDTO> locationDTOs = new ArrayList<LocationDTO>();
		universityLocations.forEach(item -> {
			Location location = item.getLocation();
			LocationDTO locationDTO = locationMapper.mapToDTO(location);
			locationDTOs.add(locationDTO);
		});
		return locationDTOs;
	}

	@Override
	public UniversityLocation save(LocationDTO locationDTO, University university) {
		UniversityLocation universityLocation = new UniversityLocation();
		Location location = this.locationMapper.mapToEntity(locationDTO);
		universityLocation.setLocation(location);
		universityLocation.setUniversity(university);
		return this.universityLocationRepository.save(universityLocation);
	}

	@Override
	public List<UniversityLocation> save(List<LocationDTO> locations, University university) {
		List<UniversityLocation> universityLocations = new ArrayList<UniversityLocation>();
		locations.forEach(item -> {
			UniversityLocation universityLocation = save(item, university);
			universityLocations.add(universityLocation);
		});
		return universityLocations;
	}
	
	@Override
	public void deleteByUniversity(Long id)
	{
		this.universityLocationRepository.deleteByUniversityId(id);
	}

}
