package com.pts.j4u.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pts.j4u.common.Constant;
import com.pts.j4u.common.RoleEnum;
import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.common.TypeError;
import com.pts.j4u.conf.CustomAuthenticationFilter;
import com.pts.j4u.dto.*;
import com.pts.j4u.dto.request.VerifyCodePasswordDTO;
import com.pts.j4u.entity.Role;
import com.pts.j4u.entity.User;
import com.pts.j4u.entity.UserRole;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.exception.ErrorValidationException;
import com.pts.j4u.mapper.UserMapper;
import com.pts.j4u.repository.RoleRepository;
import com.pts.j4u.repository.UserRepository;
import com.pts.j4u.service.RoleService;
import com.pts.j4u.service.StatusService;
import com.pts.j4u.service.UserRoleService;
import com.pts.j4u.service.UserService;
import com.pts.j4u.util.HandleFile;
import com.pts.j4u.util.HandleGeneral;
import com.pts.j4u.util.MailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;

@Service
@Transactional
public class UserServiceImpl implements UserService, UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private StatusService statusService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private HandleFile handleFile;
    @Autowired
    private HandleGeneral handleGeneral;
    @Autowired
    private MailUtil mailUtil;
    @Autowired
    private CustomAuthenticationFilter customAuthenticationFilter;
    @Value("${password_default}")
    private String PASSWORD_DEFAULT;

    @Override
    public PaginationDTO findAll(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        Page<User> pageUsers = userRepository.findAll(pageable);
        Page<UserDTO> pageUserDto = pageUsers.map(user -> userMapper.mapToDTO(user));
        return new PaginationDTO(pageUserDto);
    }

    @Override
    public UserDTO findById(Long id) {
        User entity = this.userRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "User", "Id", String.valueOf(id)),
                TypeError.NotFound));
        return userMapper.mapToDTO(entity);
    }

    @Override
    public User save(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public User saveNewUser(String email, RoleEnum roleEnum) {
        User user = new User();
        user.setUsername(email);
        user.setEmail(email);
        user.setPassword(passwordEncoder.encode(PASSWORD_DEFAULT));
        user.setCreateDate(LocalDate.now());
        user.setModifiedDate(LocalDate.now());
        user.setStatus(this.statusService.findByEnum(StatusEnum.Pending));
        Role role = roleService.findByEnum(roleEnum);
        UserRole userRole = this.userRoleService.save(user, role);
        return userRole.getUser();
    }

    @Override
    public UserDTO update(Long userId, UserRegisterDTO userRegisterDTO) {
        User user = getById(userId);
        this.userMapper.mapToUpdate(user, userRegisterDTO);
        User userUpdated = this.userRepository.save(user);
        return userMapper.mapToDTO(userUpdated);
    }

    @Override
    public UserDTO register(UserRegisterDTO userRegisterDTO) {
        // valid
        validUser(userRegisterDTO);
        // set role
        Role role = getRole(userRegisterDTO);
        // upload file
        uploadFile(userRegisterDTO);
        // setPassword
        String password = userRegisterDTO.getPassword();
        userRegisterDTO.setPassword(passwordEncoder.encode(password));
        User user = this.userMapper.mapToEntity(userRegisterDTO);
        // set Status
        setStatus(user, role);
        // set Time
        setDateTime(user);
        UserRole userRoleSaved = this.userRoleService.save(user, role);
        //send mail
        String verifyCode;
        try {
            verifyCode = JWT.create().withSubject(user.getEmail())
                    .sign(algorithm);
            // send mail
            mailUtil.sendMailVerify(user.getEmail(), verifyCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.userMapper.mapToDTO(userRoleSaved.getUser());
    }

    private Role getRole(UserRegisterDTO userRegisterDTO) {
        List<Role> roles = userRegisterDTO.getRole();
        int firstIndex = 0;
        if (roles == null || roles.size() == 0) {
            return this.roleService.findByEnum(RoleEnum.Candidate);
        } else {
            return this.roleService.findById(roles.get(firstIndex).getId());
        }
    }

    private void setStatus(User user, Role role) {
        user.setStatus(this.statusService.findByEnum(StatusEnum.Pending));
    }

    private void setDateTime(User user) {
        user.setCreateDate(LocalDate.now());
        user.setModifiedDate(LocalDate.now());
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new ErrorMessageException(messageSource.getMessage("UsernameIncorrect", null, null),
                        TypeError.BadRequest));
    }

    @Override
    public Optional<User> getOptionalByUserName(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByUsername(username);
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
        this.roleRepository.getByUserId(user.getId())
                .forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                authorities);
    }

    @Override
    public UserRegisterDTO readJson(String user) {
        UserRegisterDTO userRegisterDTO = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            userRegisterDTO = new UserRegisterDTO();
            userRegisterDTO = objectMapper.readValue(user, UserRegisterDTO.class);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        return userRegisterDTO;
    }

    @Override
    public UserRegisterDTO readJson(String user, MultipartFile file) {
        UserRegisterDTO userRegisterDTO = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            userRegisterDTO = new UserRegisterDTO();
            userRegisterDTO = objectMapper.readValue(user, UserRegisterDTO.class);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        if (file != null)
            userRegisterDTO.setFile(file);
        return userRegisterDTO;
    }

    public void validUser(UserRegisterDTO userRegisterDTO) {
        String username = userRegisterDTO.getUsername();
        String email = userRegisterDTO.getEmail();
        Map<String, String> valids = new HashMap<String, String>();
        checkExistsUserName(username, valids);
        checkExistsEmail(email, valids);
        comparePassword(userRegisterDTO.getPassword(), userRegisterDTO.getConfirmPassword(), valids);
        if (valids.size() > 0)
            throw new ErrorValidationException(valids);
        validPassword(userRegisterDTO);
    }

    private void validPassword(UserRegisterDTO dto) {
        String password = dto.getPassword();
        this.handleGeneral.validationPassword(password);
    }

    private void checkExistsUserName(String username, Map<String, String> valids) {
        boolean isExists = this.userRepository.existsByUsername(username);
        if (isExists)
            valids.put("username", this.messageSource.getMessage("valid.username.exists", null, null));
    }

    private void checkExistsEmail(String email, Map<String, String> valids) {
        boolean isExists = this.userRepository.existsByEmail(email);
        if (isExists)
            valids.put("email", this.messageSource.getMessage("valid.email.exists", null, null));
    }

    private void comparePassword(String pass, String confirmPass, Map<String, String> valids) {
        if (pass.compareTo(confirmPass) != 0)
            valids.put("password", this.messageSource.getMessage("valid.password.notmatch", null, null));
    }

    private void uploadFile(UserRegisterDTO userRegisterDTO) {
        MultipartFile file = userRegisterDTO.getFile();
        if (file != null)
            userRegisterDTO.setAvatar(this.handleFile.upload(file));
    }

    @Override
    public void uploadAvatar(UserRegisterDTO userRegisterDTO, User user) {
        MultipartFile file = userRegisterDTO.getFile();
        if (file != null) {
            String url = this.handleFile.upload(file);
            user.setAvatar(url);
        }
    }

    @Override
    public UserDTO changePassword(ChangePasswordDTO dto, long id) {
        User user = getById(id);
        String passwordEntity = user.getPassword();
        String passwordDto = dto.getOldPassword();
        // compare password
        comparePassword(passwordEntity, passwordDto);
        // set new password
        String newPassword = dto.getNewPassword();
        this.handleGeneral.validationPassword(newPassword);
        user.setPassword(passwordEncoder.encode(newPassword));
        this.userRepository.save(user);
        return userMapper.mapToDTO(user);

    }

    @Override
    public User getById(long id) {
        User entity = this.userRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "User", "Id", String.valueOf(id)),
                TypeError.NotFound));
        return entity;
    }

    private void comparePassword(String passEntity, String passDto) {
        boolean isEquals = passwordEncoder.matches(passDto, passEntity);

        if (!isEquals)
            throw new ErrorMessageException(this.messageSource.getMessage("valid.password.notmatch", null, null),
                    TypeError.BadRequest);
    }

    @Override
    public User getAndComparePassword(String username, String passDto) {
        User user = findByUsername(username);
        String passwordEntity = user.getPassword();
        comparePassword(passwordEntity, passDto);
        return user;
    }

    @Override
    public void delete(User user) {
        this.userRoleService.deleteByUserId(user.getId());
        this.userRepository.deleteByIdUser(user.getId());
    }

    private Algorithm algorithm = Algorithm.HMAC256("phamthanhsang".getBytes());
    //list to add queue
    private List<MimeMessage> queue = new ArrayList<MimeMessage>();

    @Override
    public String fortgotPassword(String email) {
        if (email == null)
            throw new ErrorMessageException("Email must be not null", TypeError.BadRequest);
        boolean isExistsEmail = userRepository.existsByEmail(email);
        if (isExistsEmail) {
            // create verify Code
            long timeAccessToken = 30 * 60 * 1000;
            String verifyCode;
            try {
                verifyCode = JWT.create().withSubject(email)
                        .withExpiresAt(new Date(System.currentTimeMillis() + timeAccessToken)).withClaim("email", email)
                        .sign(algorithm);
                // send mail
                mailUtil.sendMailForgotPassword(email, verifyCode);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        } else {
            throw new ErrorMessageException("Email not exists!", TypeError.BadRequest);
        }
    }


    @Override
    public UserDTO resetPassword(VerifyCodePasswordDTO verifyCodePasswordDTO) {
        verifyCodeAndEmail(verifyCodePasswordDTO.getEmail(), verifyCodePasswordDTO.getVerifyCode());
        // valid password
        String password = verifyCodePasswordDTO.getPassword();
        handleGeneral.validationPassword(password);
        User user = this.userRepository.findByEmail(verifyCodePasswordDTO.getEmail())
                .orElseThrow(() -> new ErrorMessageException("Email not found", TypeError.NotFound));
        user.setPassword(passwordEncoder.encode(password));
        User userUpdated = this.userRepository.save(user);
        return this.userMapper.mapToDTO(userUpdated);
    }

    @Override
    public boolean verifyCodeAndEmail(String email, String verifyCode) {
        try {
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT decodedJWT = verifier.verify(verifyCode);
            String emailVerify = decodedJWT.getSubject();
            return email.equalsIgnoreCase(emailVerify);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void updateListRoleId(List<Integer> roleIds, Long userId) {
        userRoleService.deleteByUserId(userId);
        User user = getById(userId);
        List<UserRole> userRoles = new ArrayList<>();
        if (roleIds != null && roleIds.size() > 0) {
            roleIds.forEach(id -> {
                //create user role
                Role role = this.roleService.findById(id);
                UserRole userRoleSaved = this.userRoleService.save(user, role);
                userRoles.add(userRoleSaved);
            });
        }
//        user.setUsers(usx
    }

    @Override
    public PaginationDTO findUserByRoleId(Integer roleId, Integer pageNumber, Integer pageSize) {
        Page<User> pageUser = this.userRepository.getUsersByRoleId(roleId, PageRequest.of(pageNumber, pageSize));
        pageUser.map(user -> this.userMapper.mapToDTO(user));
        return new PaginationDTO(pageUser);
    }

    @Override
    public Map handleUserGoogle(GoogleAccountDTO account) {
        String email = account.getEmail();
        //check email is exists
        boolean isExistsEmail = this.userRepository.existsByEmail(email);
        User user = null;
        if (isExistsEmail) {
            user = this.userRepository.findByEmail(email).get();
            //get role and set role to create Token
        } else {
            //register new user
            user = new User();
            user.setUsername(email);
            user.setVerifyEmail(account.isVerified_email());
            user.setStatus(this.statusService.findByEnum(StatusEnum.Active));
            user.setPassword(passwordEncoder.encode(PASSWORD_DEFAULT));
            user.setAvatar(account.getPicture());
            user.setModifiedDate(LocalDate.now());
            user.setCreateDate(LocalDate.now());
            user.setEmail(email);
            //Save role
            user = this.userRepository.save(user);
        }
        return customAuthenticationFilter.handleSuccessLogin(user);
    }

}
