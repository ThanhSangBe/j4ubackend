package com.pts.j4u.service.impl;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.entity.Job;
import com.pts.j4u.entity.JobType;
import com.pts.j4u.entity.JobTypeJob;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.repository.JobTypeJobRepository;
import com.pts.j4u.service.JobTypeJobService;
import com.pts.j4u.service.JobTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class JobTypeJobServiceImpl implements JobTypeJobService {
    @Autowired
    private JobTypeJobRepository jobTypeJobRepository;
    @Autowired
    private JobTypeService jobTypeService;
    @Autowired
    private MessageSource messageSource;

    @Override
    public List<JobTypeJob> save(List<JobType> jobTypes, Job job) {
        if (jobTypes != null && jobTypes.size() > 0) {
            List<JobTypeJob> jobTypeJobs = new ArrayList<>();
            jobTypes.forEach(jobType -> {
                JobTypeJob jobTypeJob = new JobTypeJob();
                JobType jobTypeEntity = this.jobTypeService.findById(jobType.getId());
                jobTypeJob.setJobType(jobTypeEntity);
                jobTypeJob.setJob(job);
                JobTypeJob saved = this.jobTypeJobRepository.save(jobTypeJob);
                jobTypeJobs.add(saved);
            });
            return jobTypeJobs;
        }
        return null;
    }

    @Override
    public List<JobType> findByJobId(Long jobId) {
        List<JobType> jobTypes = new ArrayList<>();
        List<JobTypeJob> jobTypeJobs = this.jobTypeJobRepository.findByJobId(jobId);
        jobTypeJobs.forEach(item -> {
            jobTypes.add(item.getJobType());
        });
        return jobTypes;
    }

    @Override
    public void update(Job job, List<JobType> jobTypesDTO) {
        List<JobTypeJob> jobTypeJobs = this.jobTypeJobRepository.findByJobId(job.getId());
        if (jobTypeJobs != null && jobTypeJobs.size() > 0) {
            int sizeJobTypeDtos = jobTypesDTO.size();
            //handle get Job Type
            List<JobType> jobTypes = new ArrayList<>();
            jobTypeJobs.forEach(item -> {
                jobTypes.add(item.getJobType());
            });
            int sizeJobTypeJob = jobTypeJobs.size();
            if (sizeJobTypeDtos == sizeJobTypeJob) {
                for (int i = 0; i < sizeJobTypeJob; i++) {
                    JobType jobType = jobTypes.get(i);
                    JobType jobTypeDTO = jobTypesDTO.get(i);
                    if (jobType.getId() != jobTypeDTO.getId()) {
                        update(jobTypeJobs.get(i), this.jobTypeService.findById(jobTypeDTO.getId()));
                    }
                }
            } else {
                //delete
                jobTypeJobs.forEach(item ->{
                   deleteById(item.getId());
                });
                //create again
                jobTypesDTO.forEach(item ->{
                    JobType jobType = this.jobTypeService.findById(item.getId());
                    save(jobType,job);
                });
            }

        }
    }

    private void update(JobTypeJob jobTypeJob, JobType jobType) {
        jobTypeJob.setJobType(jobType);
        this.jobTypeJobRepository.save(jobTypeJob);
    }
    @Override
    public void deleteById(Long id)
    {
        JobTypeJob jobTypeJob = findById(id);
        jobTypeJob.setJobType(null);
        jobTypeJob.setJob(null);
        this.jobTypeJobRepository.save(jobTypeJob);
        this.jobTypeJobRepository.deleteById(id);
    }
    @Override
    public JobTypeJob save(JobType jobType, Job job)
    {
        JobTypeJob jobTypeJob = new JobTypeJob();
        jobTypeJob.setJobType(jobType);
        jobTypeJob.setJob(job);
        return this.jobTypeJobRepository.save(jobTypeJob);
    }
    @Override
    public JobTypeJob findById(Long id)
    {
        return this.jobTypeJobRepository.findById(id).orElseThrow(()-> new ErrorMessageException(
                String.format(this.messageSource.getMessage("NotFound",null,null),"JobTypeJob","id",String.valueOf(id))
                , TypeError.NotFound));
    }
}
