package com.pts.j4u.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.*;
import com.pts.j4u.entity.Company;
import com.pts.j4u.entity.Role;
import com.pts.j4u.entity.Status;
import com.pts.j4u.entity.User;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.exception.ErrorValidationException;
import com.pts.j4u.mapper.CompanyMapper;
import com.pts.j4u.mapper.UserMapper;
import com.pts.j4u.repository.CompanyLocationRepository;
import com.pts.j4u.repository.CompanyRepository;
import com.pts.j4u.repository.UserRepository;
import com.pts.j4u.service.*;
import com.pts.j4u.service.strategy.CompanyStrategy;
import com.pts.j4u.util.HandleFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private CompanyLocationRepository companyLocationRepository;
    @Autowired
    private StatusService statusService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CompanyMapper companyMapper;
    @Resource(name = "CompanyLocation")
    private CompanyStrategy companyLocationStrategy;
    @Resource(name = "Company")
    private CompanyStrategy companyStrategy;
    @Autowired
    private HandleFile handleFile;
    @Autowired
    private UserService userService;
    @Autowired
    private JobService jobService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CandidateService candidateService;
    @Autowired
    private UserMapper userMapper;

    @Override
    public Company save(CompanyDTO entity) {
        if (entity.getLocations() != null)
            return companyLocationStrategy.save(entity);
        else
            return companyStrategy.save(entity);
    }

    private Company register(CompanyDTO companyDTO, UserDTO userDTO) {
        if (companyDTO.getLocations() != null)
            return companyLocationStrategy.register(companyDTO, userDTO);
        return null;
    }

    @Override
    public CompanyDTO register(CompanyRegisterDTO companyRegisterDTO) {
        UserRegisterDTO userRegisterDTO = companyRegisterDTO.getUser();
        CompanyCreateDTO companyCreateDTO = companyRegisterDTO.getCompany();
        //set user for company
        userRegisterDTO.setEmail(companyRegisterDTO.getCompany().getEmail());
        userRegisterDTO.setRole(Arrays.asList(new Role(2,"Role_Company")));
        // save user
        UserDTO userDTO = this.userService.register(userRegisterDTO);
        // upload file
        MultipartFile file = companyCreateDTO.getFile();
        if (file != null)
            companyCreateDTO.setLogo(this.handleFile.upload(file));
        // save company
        CompanyDTO companyDTO = this.companyMapper.mapToDTO(companyCreateDTO);
        Company company = register(companyDTO, userDTO);
        //update user with company
        User user = userService.findByUsername(userRegisterDTO.getUsername());
        user.setCompany(company);
        this.userRepository.save(user);
        return this.companyMapper.mapToDTO(company);
    }

    @Override
    public Page<Company> findAll(Pageable pageable) {
        return companyRepository.findAll(pageable);
    }

    @Override
    public CompanyDTO findById(Long id) {
        Company company =companyRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "Company", "id", String.valueOf(id)),
                TypeError.NotFound));
        return this.companyMapper.mapToDTO(company);
    }

    @Override
    public boolean existsById(Long id) {
        return companyRepository.existsById(id);
    }

    @Override
    public long count() {
        return companyRepository.count();
    }

    @Override
    public List<Company> findAll() {
        return this.companyRepository.findAll();
    }

    @Override
    public Company changeStatus(Long id, StatusEnum status) {
        Company company = getById(id);
        Status statusChange = statusService.findByEnum(status);
        company.setStatus(statusChange);
        List<User> users = company.getUsers();
        users.forEach(user -> user.setStatus(statusChange));
        company.setUsers(users);
        return this.companyRepository.save(company);
    }

    @Override
    public CompanyDTO update(Long id, CompanyDTO companyDTO) {
        Company entity = getById(id);
        companyMapper.mapUpdate(entity, companyDTO);
        entity.setModifiedDate(LocalDate.now());
        Company updated = this.companyRepository.save(entity);
        CompanyDTO dto = companyMapper.mapToDTO(updated);
        return dto;
    }

    @Override
    public CompanyDTO update(CompanyCreateDTO companyCreateDto, Long id) {
        Company company = getById(id);
        // upload file
        updateLogo(companyCreateDto);
        CompanyDTO companyDTO = companyMapper.mapFromCreateToDTO(companyCreateDto);
        companyMapper.mapUpdate(company, companyDTO);
        company.setModifiedDate(LocalDate.now());
        Company updated = this.companyRepository.save(company);
        CompanyDTO dto = companyMapper.mapToDTO(updated);
        return dto;
    }

    @Override
    public Company getById(long id){
        return companyRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "Company", "id", String.valueOf(id)),
                TypeError.NotFound));
    }

    @Override
    public List<CompanyDTO> findByStatus(Integer id) {
        List<Company> companies = this.companyRepository.getByStatusId(id);
        return companies.stream().map(item -> this.companyMapper.mapToDTO(item)).collect(Collectors.toList());
    }

    @Override
    public PaginationDTO findAll(int no, int limit) {
        Pageable pageable = PageRequest.of(no, limit);
        Page<Company> companyPages = this.companyRepository.findAll(pageable);
        return setPropertyPagination(companyPages);
    }

    @Override
    public CompanyCreateDTO readJson(String company, MultipartFile file) {

        if (company == null || file == null) {
            throwCompanyOrFileNull(company, file);
        }
        CompanyCreateDTO companyCreateDTO = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            companyCreateDTO = new CompanyCreateDTO();
            companyCreateDTO = objectMapper.readValue(company, CompanyCreateDTO.class);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        companyCreateDTO.setFile(file);
        return companyCreateDTO;
    }

    @Override
    public CompanyRegisterDTO readJsonRegister(String company, MultipartFile file) {
        CompanyRegisterDTO companyRegisterDTO = new CompanyRegisterDTO();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            companyRegisterDTO = objectMapper.readValue(company, CompanyRegisterDTO.class);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        if (file != null)
            companyRegisterDTO.getCompany().setFile(file);
        // valid company
        valid(companyRegisterDTO.getCompany());
        return companyRegisterDTO;
    }

    @Override
    public CompanyDTO save(CompanyCreateDTO dto) {
        CompanyDTO companyDTO = this.companyMapper.mapToDTO(dto);
        MultipartFile file = dto.getFile();
        String url = this.handleFile.upload(file);
        companyDTO.setLogo(url);
        Company companySaved = save(companyDTO);

        return this.companyMapper.mapToDTO(companySaved);
    }

    private void throwCompanyOrFileNull(String company, MultipartFile file) {
        if (company == null || company.equals("") && file == null) {
            throw new ErrorMessageException(this.messageSource.getMessage("FailCreate", null, null),
                    TypeError.BadRequest);
        } else if (company == null || company.equals("")) {
            throw new ErrorMessageException(this.messageSource.getMessage("valid.company.notempty", null, null),
                    TypeError.BadRequest);
        }
//		else if (file == null) {
//			throw new ErrorMessageException(this.messageSource.getMessage("valid.file.notempty", null, null),
//					TypeError.BadRequest);
//		}
    }

    private void valid(CompanyCreateDTO companyCreateDTO) {
        Map<String, String> valids = new HashMap<>();
        String name = companyCreateDTO.getName();
        String email = companyCreateDTO.getEmail();
        String website = companyCreateDTO.getWebsite();
        String code = companyCreateDTO.getCode();
        // check null
        if (name == null || email == null || website == null || code == null) {
            if (name == null)
                valids.put("name", this.messageSource.getMessage("valid.name.notempty", null, null));
            if (email == null)
                valids.put("email", this.messageSource.getMessage("valid.email.notempty", null, null));
            if (website == null)
                valids.put("website", this.messageSource.getMessage("valid.website.notempty", null, null));
            if (code == null)
                valids.put("code", this.messageSource.getMessage("valid.code.notempty", null, null));
        } else// check exists
        {
            boolean isExistsEmail = this.companyRepository.existsByEmail(email);
            boolean isExistsWebsite = this.companyRepository.existsByWebsite(website);
            boolean isExistsCode = this.companyRepository.existsByCode(code);
            if (isExistsEmail)
                valids.put("email", this.messageSource.getMessage("EmailExists", null, null));
            if (isExistsWebsite)
                valids.put("website", this.messageSource.getMessage("WebsiteExists", null, null));
            if (isExistsCode)
                valids.put("code", this.messageSource.getMessage("CodeExists", null, null));
        }
        if (valids.size() > 0)
            throw new ErrorValidationException(valids);

    }

    @Override
    public CompanySimpleDTO findByIdAndJobs(Long id) {
        Company company = getById(id);
        CompanySimpleDTO companyDTO = this.companyMapper.mapToSimple(company);
//		List<JobSimpleDTO> jobs = this.jobService.findByCompanyId(id);
//		companyDTO.setJobs(jobs);
        return companyDTO;
    }

    @Override
    public PaginationDTO findAll(String name, int status, int no, int limit) {
        Pageable pageable = PageRequest.of(no, limit);
        Page<Company> companyPage = null;
        if (!name.equals("") && status != 0)// have both
        {
            companyPage = this.companyRepository.findByNameAndStatusId(pageable, name, status);
        } else if (!name.equals("") && status == 0)// have name
        {
            companyPage = this.companyRepository.findByName(pageable, name);
        } else if (name.equals("") && status != 0)// have status
        {
            companyPage = this.companyRepository.findByStatusId(pageable, status);
        } else
            return findAll(no, limit);
        return setPropertyPagination(companyPage);
    }

    @Override
    public void disableListId(List<Long> listId) {
        if (listId != null && listId.size() > 0) {
            listId.forEach(id -> {
                changeStatus(id, StatusEnum.Disable);
            });
        }
    }

    @Override
    public void activeListId(List<Long> listId) {
        if (listId != null && listId.size() > 0) {
            listId.forEach(id -> {
                changeStatus(id, StatusEnum.Active);
            });
        }
    }

    @Override
    public void deleteListId(List<Long> listId) {
        if (listId != null && listId.size() > 0) {
            listId.forEach(id -> {
                changeStatus(id, StatusEnum.Deleted);
            });
        }
    }

    @Override
    public void deleteForceByList(List<Long> listId) {
        if (listId != null && listId.size() > 0) {
            for (Long id : listId) {
                deleteForce(id);
            }
        }
    }

    @Override
    public void deleteForce(Long id) {
        try {
            this.companyRepository.deleteById(id);
        } catch (Exception e) {
            throw new ErrorMessageException(e.getMessage(), TypeError.InternalServerError);
        }

    }

    @Override
    public List<CandidateDTO> getCandidatesByIdJob(Long id) {
        if (id != null)
            return this.candidateService.getListCandidateAppliedByIdCompanyAndIdJob(id);
        return null;
    }

    @Override
	public CompanyDTO getByUserId(long userId) {
        Optional<Company> companyOptional = this.companyRepository.getByUserId(userId);
        if (companyOptional.isPresent())
            return this.companyMapper.mapToDTO(companyOptional.get());
        throw new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "Company", "id", userId),
                TypeError.NotFound);
    }

    private PaginationDTO setPropertyPagination(Page<Company> companyPage) {
        List<CompanyDTO> companyDTOList = companyPage.getContent().stream()
                .map(item -> this.companyMapper.mapToDTO(item)).collect(Collectors.toList());

        PaginationDTO paginationDTO = new PaginationDTO();
        paginationDTO.setData(companyDTOList);
        paginationDTO.setFirst(companyPage.isFirst());
        paginationDTO.setLast(companyPage.isLast());
        paginationDTO.setTotalPages(companyPage.getTotalPages());
        paginationDTO.setNumberOfCurrentPage(companyPage.getNumberOfElements());
        paginationDTO.setSizeCurrentItems(companyPage.getSize());
        paginationDTO.setTotalItems(companyPage.getTotalElements());
        paginationDTO.setPageCurrent(companyPage.getNumber());
        return paginationDTO;
    }

    public void updateLogo(CompanyCreateDTO companyCreateDTO) {
        MultipartFile logo = companyCreateDTO.getFile();
        if (logo != null) {
            String url = this.handleFile.upload(logo);
            companyCreateDTO.setLogo(url);
        }
    }
    @Override
    public List<UserDTO> getUsersByCompanyId(Long companyId)
    {
        List<User> users = this.userRepository.getByCompanyId(companyId);
        return users.stream().map(user-> this.userMapper.mapToDTO(user)).collect(Collectors.toList());
    }
}
