package com.pts.j4u.service.impl;

import com.pts.j4u.dto.JobDTO;
import com.pts.j4u.dto.LikeJobDTO;
import com.pts.j4u.dto.PaginationDTO;
import com.pts.j4u.entity.Job;
import com.pts.j4u.entity.LikeJob;
import com.pts.j4u.mapper.JobMapper;
import com.pts.j4u.mapper.LikeJobMapper;
import com.pts.j4u.repository.LikeJobRepository;
import com.pts.j4u.service.CandidateService;
import com.pts.j4u.service.JobService;
import com.pts.j4u.service.LikeJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@Transactional
public class LikeJobServiceImpl implements LikeJobService {
    @Autowired
    private LikeJobRepository likeJobRepository;
    @Autowired
    private LikeJobMapper likeJobMapper;
    @Autowired
    private JobService jobService;
    @Autowired
    private CandidateService candidateService;
    @Autowired
    private JobMapper jobMapper;

    @Override
    public LikeJobDTO save(Long jobId, Long candidateId) {
        LikeJob likeJob = new LikeJob();
        likeJob.setTimeLike(LocalDateTime.now());
        likeJob.setCandidate(candidateService.getById(candidateId));
        likeJob.setJob(jobService.findById(jobId));
        LikeJob likeJobSaved = this.likeJobRepository.save(likeJob);
        return likeJobMapper.mapToDto(likeJobSaved);
    }

    @Override
    public void delete(Long jobId, Long candidateId) {
        this.likeJobRepository.deleteByJobIdAndCandidateId(jobId, candidateId);
    }

    @Override
    public PaginationDTO getJobdLikedByCandidateId(Long candidateId, Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        Page<Job> pageJob = this.likeJobRepository.getJobLikedByCandidateId(candidateId, pageable);
        return new PaginationDTO(pageJob.map(page -> this.jobMapper.mapToDTO(page)));
    }
}
