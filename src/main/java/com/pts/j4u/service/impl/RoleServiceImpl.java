package com.pts.j4u.service.impl;

import java.util.List;
import java.util.Optional;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.PaginationDTO;
import com.pts.j4u.exception.ErrorMessageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.pts.j4u.common.RoleEnum;
import com.pts.j4u.entity.Role;
import com.pts.j4u.repository.RoleRepository;
import com.pts.j4u.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private MessageSource messageSource;
	@Override
	public Role save(Role entity) {
		return roleRepository.save(entity);
	}

	@Override
	public List<Role> findAll() {
		return roleRepository.findAll();
	}

	@Override
	public Role findById(Integer id) {
		return roleRepository.findById(id).orElseThrow(()-> new ErrorMessageException(
				String.format(this.messageSource.getMessage("NotFound",null,null),"Role","ID",String.valueOf(id))
				, TypeError.NotFound));
	}

	@Override
	public boolean existsById(Integer id) {
		return roleRepository.existsById(id);
	}
	
	@Override
	public Role update(Integer id, Role role)
	{
		Role entity = findById(id);
		entity.setName(role.getName());
		return this.roleRepository.save(entity);
	}
	
	@Override
	public Role findByEnum(RoleEnum role)
	{
		switch (role) {
		case Admin:
			return findById(5);	
		case Candidate:
			return findById(4);
		case Company:
			return findById(2);
		default:
			return findById(1);
		}
	}

	
}
