package com.pts.j4u.service.impl;

import com.pts.j4u.entity.CompanyLocation;
import com.pts.j4u.repository.CompanyLocationRepository;
import com.pts.j4u.service.CompanyLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyLocationServiceImpl implements CompanyLocationService {
    @Autowired
    private CompanyLocationRepository companyLocationRepository;

    @Override
    public List<CompanyLocation> getByCompanyId(Long id)
    {
        return this.companyLocationRepository.getByCompanyId(id);
    }
}
