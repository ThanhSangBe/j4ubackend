package com.pts.j4u.service.impl;

import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.ApplyJobDTO;
import com.pts.j4u.entity.ApplyJob;
import com.pts.j4u.entity.Candidate;
import com.pts.j4u.entity.Job;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.mapper.ApplyJobMapper;
import com.pts.j4u.repository.ApplyJobRepository;
import com.pts.j4u.service.ApplyJobService;
import com.pts.j4u.service.CandidateService;
import com.pts.j4u.service.JobService;

@Service
@Transactional
public class ApplyJobServiceImpl implements ApplyJobService {
	@Autowired
	private ApplyJobRepository applyJobRepository;
	@Autowired
	private CandidateService candidateService;
	@Autowired
	private JobService jobService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ApplyJobMapper applyJobMapper;

	@Override
	public ApplyJobDTO save(ApplyJobDTO applyJobDTO) {
		ApplyJob applyJob = map(applyJobDTO);
		applyJob.setTimeApply(LocalDateTime.now());
		handleCV(applyJob.getCandidate());
		ApplyJob saved = this.applyJobRepository.save(applyJob);
		return this.applyJobMapper.mapToDTO(saved);
	}

	private ApplyJob map(ApplyJobDTO dto) {
		Candidate candidate = this.candidateService.getById(dto.getCandidate().getId());
		Job job = this.jobService.findById(dto.getJob().getId());
		ApplyJob applyJob = new ApplyJob();
		applyJob.setCandidate(candidate);
		applyJob.setJob(job);
		return applyJob;
	}

	private void handleCV(Candidate candidate) {
		boolean isHaveCV = this.candidateService.checkHaveCV(candidate);
		if (!isHaveCV)
			throw new ErrorMessageException(this.messageSource.getMessage("valid.cv.notempty", null, null),
					TypeError.BadRequest);
	}

}
