package com.pts.j4u.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pts.j4u.common.RoleEnum;
import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.*;
import com.pts.j4u.entity.Status;
import com.pts.j4u.entity.University;
import com.pts.j4u.entity.User;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.mapper.LocationMapper;
import com.pts.j4u.mapper.UniversityMapper;
import com.pts.j4u.repository.UniversityRepository;
import com.pts.j4u.service.*;
import com.pts.j4u.service.strategy.UniversityStrategy;
import com.pts.j4u.util.HandleFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UniversityServiceImpl implements UniversityService {
    @Autowired
    private UniversityRepository universityRepository;
    @Autowired
    private UniversityMapper universityMapper;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private StatusService statusService;
    @Autowired
    private UniversityLocationService universityLocationService;
    @Autowired
    private LocationMapper locationMapper;
    @Autowired
    private LocationService locationService;
    @Resource(name = "UniversityStrategy")
    private UniversityStrategy universityStrategy;

    @Resource(name = "UniversityLocationStrategy")
    private UniversityStrategy universityLocationStrategy;
    @Autowired
    private HandleFile handleFile;
    @Autowired
    private UserService userService;

    @Override
    public UniversityDTO save(UniversityDTO dto) {

        List<LocationDTO> locationDTO = dto.getLocations();
        if (locationDTO == null) {
            return universityStrategy.save(dto);
        } else {
            return universityLocationStrategy.save(dto);
        }
    }

    @Override
    public PaginationDTO findAll(int no, int limit) {
        Pageable pageable = PageRequest.of(no, limit);
        Page<University> universityDTOPage = this.universityRepository.findAll(pageable);
        List<UniversityDTO> universityDTOS = universityDTOPage.getContent().stream()
                .map(item -> this.universityMapper.mapToDTO(item)).collect(Collectors.toList());
        PaginationDTO paginationDTO = new PaginationDTO();
        paginationDTO.setData(universityDTOS);
        paginationDTO.setFirst(universityDTOPage.isFirst());
        paginationDTO.setLast(universityDTOPage.isLast());
        paginationDTO.setTotalPages(universityDTOPage.getTotalPages());
        paginationDTO.setNumberOfCurrentPage(universityDTOPage.getNumberOfElements());
        paginationDTO.setSizeCurrentItems(universityDTOPage.getSize());
        paginationDTO.setTotalItems(universityDTOPage.getTotalElements());
        paginationDTO.setPageCurrent(universityDTOPage.getNumber());
        return paginationDTO;
    }

    @Override
    public List<UniversityDTO> findAll() {
        List<University> universities = this.universityRepository.findAll();
        List<UniversityDTO> universityDTOs = universities.stream()
                .map(university -> this.universityMapper.mapToDTO(university)).collect(Collectors.toList());
        return universityDTOs;
    }

    @Override
    public University getById(Long id) {
        University university = this.universityRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "University", "Id", String.valueOf(id)),
                TypeError.NotFound));
        return university;
    }

    @Override
    public UniversityDTO changeStatus(Long id, StatusEnum status) {
        University university = getById(id);
        Status statusEntity = this.statusService.findByEnum(status);
        university.setStatus(statusEntity);
        // user
        User user = university.getUser();
        user.setStatus(statusEntity);
        university.setUser(user);
        University universityUpdate = this.universityRepository.save(university);
        UniversityDTO dto = this.universityMapper.mapToDTO(universityUpdate);
        return dto;

    }

    @Override
    public void deleteListId(List<Long> listId) {
        if (listId != null && listId.size() > 0) {
            listId.forEach(id -> {
                UniversityDTO dto = findById(id);
                changeStatus(dto.getId(), StatusEnum.Deleted);
            });
        }
    }

    @Override
    public void activeListId(List<Long> listId) {
        if (listId != null && listId.size() > 0) {
            listId.forEach(id -> {
                UniversityDTO dto = findById(id);
                changeStatus(dto.getId(), StatusEnum.Active);
            });
        }
    }

    @Override
    public void disableListId(List<Long> listId) {
        if (listId != null && listId.size() > 0) {
            listId.forEach(id -> {
                UniversityDTO dto = findById(id);
                changeStatus(dto.getId(), StatusEnum.Disable);
            });
        }
    }

    @Override
    public void deleteForce(List<Long> listId) {
        if (listId != null && listId.size() > 0) {
            for (Long id : listId) {
                UniversityDTO dto = findById(id);
                this.universityRepository.deleteById(dto.getId());
            }
        }
    }

    @Override
    public UniversityDTO save(UniversityCreateDTO universityCreateDTO) {
        UniversityDTO universityDTO = this.universityMapper.mapToDTO(universityCreateDTO);
        MultipartFile file = universityCreateDTO.getFile();
        String url = this.handleFile.upload(file);
        universityDTO.setAvatar(url);
        UniversityDTO universityDTOSaved = save(universityDTO);
        User user = this.userService.saveNewUser(universityDTOSaved.getEmail(), RoleEnum.University);
        University university = getById(universityDTOSaved.getId());
        university.setUser(user);
        this.universityRepository.save(university);
        return universityDTOSaved;
    }

    @Override
    public UniversityDTO findById(Long id) {
        University university = this.universityRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "University", "Id", String.valueOf(id)),
                TypeError.NotFound));
        UniversityDTO dto = this.universityMapper.mapToDTO(university);
        return dto;
    }

    @Override
    public UniversityDTO update(Long id, UniversityDTO dto) {
        University entity = getById(id);
        // Map To Update
        universityMapper.mapToUpdate(dto, entity);
        // Check location
        University updated = this.universityRepository.save(entity);
        Long idUniversity = updated.getId();
        List<LocationDTO> locationDTOs = dto.getLocations();
        handlerLocation(idUniversity, locationDTOs, updated);
        return findById(idUniversity);
    }

    private void handlerLocation(Long idUniversity, List<LocationDTO> locationDTO, University university) {
        List<LocationDTO> locationDTOs = this.universityLocationService.findByUniversity(idUniversity);
        boolean isEquals = this.locationService.compareList(locationDTOs, locationDTO);
        if (isEquals == false) // update location
        {
            int sizeLocationDTO = locationDTOs.size();

            if (sizeLocationDTO != 0) {
                int sizeLocationEntity = locationDTO.size();
                if (sizeLocationDTO == sizeLocationEntity) {// update
                    locationDTOs.forEach(location -> {
                        this.locationService.setIdDTO(locationDTO, locationDTOs);
                        this.locationService.update(locationDTO);
                    });
                } else {// delete and update
                    this.universityLocationService.deleteByUniversity(idUniversity);
                    this.universityLocationService.save(locationDTO, university);

                }

            } else
                this.universityLocationService.save(locationDTO, university);
        }
    }

    @Override
    public PaginationDTO searchByNameAndStatus(int no, int limit, String name, int statusId) {
        Pageable pageable = PageRequest.of(no, limit);
        Page<University> universityPage = null;
        if (!name.equals("") && statusId != 0)
            universityPage = this.universityRepository.findByStatusIdAndName(pageable, statusId, name);
        if (!name.equals("") && statusId == 0)
            universityPage = this.universityRepository.findByName(pageable, name);
        if (name.equals("") && statusId != 0)
            universityPage = this.universityRepository.findByStatusId(pageable, statusId);
        if (name.equals("") && statusId == 0)
            universityPage = this.universityRepository.findAll(pageable);
        return setPropertyPagination(universityPage);
    }

    @Override
    public UniversityCreateDTO readJson(String university, MultipartFile file) {
        if (university == null || file == null) {
            throwCompanyOrFileNull(university, file);
        }
        UniversityCreateDTO universityCreateDTO = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            universityCreateDTO = new UniversityCreateDTO();
            universityCreateDTO = objectMapper.readValue(university, UniversityCreateDTO.class);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        universityCreateDTO.setFile(file);
        return universityCreateDTO;
    }

    @Override
    public UniversityCreateDTO readJson(String university) {
        UniversityCreateDTO universityCreateDTO = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            universityCreateDTO = new UniversityCreateDTO();
            universityCreateDTO = objectMapper.readValue(university, UniversityCreateDTO.class);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }
        return universityCreateDTO;
    }


    @Override
    public UniversityDTO getByUserId(long userId) {
        Optional<University> universityOptional = this.universityRepository.findByUserId(userId);
        if (universityOptional.isPresent())
            return this.universityMapper.mapToDTO(universityOptional.get());
        throw new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "University", "Id", userId),
                TypeError.NotFound);
    }

    private void throwCompanyOrFileNull(String university, MultipartFile file) {
        if (university == null || university.equals("") && file == null) {
            throw new ErrorMessageException(this.messageSource.getMessage("FailCreate", null, null),
                    TypeError.BadRequest);
        } else if (university == null || university.equals("")) {
            throw new ErrorMessageException(this.messageSource.getMessage("valid.company.notempty", null, null),
                    TypeError.BadRequest);
        } else if (file == null) {
            throw new ErrorMessageException(this.messageSource.getMessage("valid.file.notempty", null, null),
                    TypeError.BadRequest);
        }
    }

    private PaginationDTO setPropertyPagination(Page<University> universityPage) {
        List<UniversityDTO> universityDTOs = universityPage.getContent().stream()
                .map(university -> this.universityMapper.mapToDTO(university)).collect(Collectors.toList());

        PaginationDTO paginationDTO = new PaginationDTO();
        paginationDTO.setData(universityDTOs);
        paginationDTO.setFirst(universityPage.isFirst());
        paginationDTO.setLast(universityPage.isLast());
        paginationDTO.setTotalPages(universityPage.getTotalPages());
        paginationDTO.setNumberOfCurrentPage(universityPage.getNumberOfElements());
        paginationDTO.setSizeCurrentItems(universityPage.getSize());
        paginationDTO.setTotalItems(universityPage.getTotalElements());
        paginationDTO.setPageCurrent(universityPage.getNumber());
        return paginationDTO;
    }

    @Override
    public UniversityDTO register(UniversityCreateDTO universityCreateDTO, UserRegisterDTO userRegisterDTO) {
        UniversityDTO universityDTO = this.universityMapper.mapToDTO(universityCreateDTO);
        UniversityDTO universitySaved = save(universityDTO);
        UserDTO userDTO = userService.register(userRegisterDTO);
        University university = getById(universitySaved.getId());
        User user = userService.getById(userDTO.getId());
        university.setUser(user);
        return this.universityMapper.mapToDTO(university);
    }
}
