package com.pts.j4u.service.impl;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.DemandDTO;
import com.pts.j4u.dto.PaginationDTO;
import com.pts.j4u.dto.UniversityDTO;
import com.pts.j4u.entity.Demand;
import com.pts.j4u.entity.Status;
import com.pts.j4u.entity.University;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.mapper.DemandMapper;
import com.pts.j4u.repository.DemandRepository;
import com.pts.j4u.service.DemandService;
import com.pts.j4u.service.StatusService;
import com.pts.j4u.service.UniversityService;
import com.pts.j4u.util.HandleDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DemandServiceImpl implements DemandService {
    @Autowired
    private DemandRepository demandRepository;
    @Autowired
    private DemandMapper demandMapper;
    @Autowired
    private HandleDateTime handleDateTime;
    @Autowired
    private UniversityService universityService;
    @Autowired
    private StatusService statusService;
    @Autowired
    private MessageSource messageSource;

    @Override
    public DemandDTO save(DemandDTO demandDTO) {
        Demand demand = this.demandMapper.mapToEntity(demandDTO);
        // set time start and time end
        String startDateStr = demandDTO.getStartDate();
        String endDateStr = demandDTO.getEndDate();
        // valid
        this.handleDateTime.validStartEndTime(startDateStr, endDateStr);
        LocalDate startDate = this.handleDateTime.parse(startDateStr);
        LocalDate endDate = this.handleDateTime.parse(endDateStr);
        demand.setStart(startDate);
        demand.setEnd(endDate);
        demand.setCreateDate(LocalDate.now());
        demand.setModifiedDate(LocalDate.now());
        // set Univetsity
        UniversityDTO universityDTO = demandDTO.getUniversityDTO();
        Long universityId = universityDTO.getId();
        University university = this.universityService.getById(universityId);
        demand.setUniversity(university);
        // set status
        Status status = statusService.findByEnum(StatusEnum.Pending);
        demand.setStatus(status);
        Demand demandSaved = this.demandRepository.save(demand);
        return demandMapper.mapToDTO(demandSaved);
    }

    @Override
    public DemandDTO update(DemandDTO demandDTO, long demandId) {
        Demand demand = getById(demandId);
        this.demandMapper.mapToUpdate(demand, demandDTO);
        // set time start and time end
        String startDateStr = demandDTO.getStartDate();
        String endDateStr = demandDTO.getEndDate();
        // valid
        this.handleDateTime.validStartEndTime(startDateStr, endDateStr);
        LocalDate startDate = this.handleDateTime.parse(startDateStr);
        LocalDate endDate = this.handleDateTime.parse(endDateStr);
        demand.setStart(startDate);
        demand.setEnd(endDate);
        Demand demandSaved = this.demandRepository.save(demand);
        return demandMapper.mapToDTO(demandSaved);
    }

    @Override
    public DemandDTO findById(Long id) {
        return this.demandMapper.mapToDTO(getById(id));
    }

    private void changeStatus(Long id, StatusEnum statusEnum) {
        Demand demand = getById(id);
        Status status = this.statusService.findByEnum(statusEnum);
        demand.setStatus(status);
        this.demandRepository.save(demand);
    }

    private Demand getById(Long id) {
        return this.demandRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "Demand", "id", String.valueOf(id)),
                TypeError.NotFound));
    }

    @Override
    public void changeStatusListId(List<Long> listId, StatusEnum statusEnum) {
        if (listId != null && listId.size() > 0) {
            listId.forEach(id -> {
                changeStatus(id, statusEnum);
            });
        }
    }

    @Override
    public List<DemandDTO> filter() {
        return this.demandRepository.findAll().stream().map(demand -> this.demandMapper.mapToDTO(demand))
                .collect(Collectors.toList());
    }

    @Override
    public PaginationDTO findByUniversityId(Long id) {
        Pageable pageable = PageRequest.of(0, 10);
        Page<Demand> demandPage = this.demandRepository.findByUniversityId(id, pageable);
        return new PaginationDTO(demandPage.map(demand -> demandMapper.mapToDTO(demand)));
    }

}
