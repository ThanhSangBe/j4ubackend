package com.pts.j4u.service.impl;

import java.util.List;

import com.pts.j4u.common.StatusCommon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.entity.Status;
import com.pts.j4u.repository.StatusRepository;
import com.pts.j4u.service.StatusService;
@Service
public class StatusServiceImpl implements StatusService {
	@Autowired
	private StatusRepository statusRepository;
	
	@Override
	public Status save(Status entity) {
		return statusRepository.save(entity);
	}

	@Override
	public Page<Status> findAll(Pageable pageable) {
		return statusRepository.findAll(pageable);
	}

	@Override
	public List<Status> findAll() {
		return statusRepository.findAll();
	}

	@Override
	public Status findById(Integer id) {
		return statusRepository.findById(id).get();
	}

	@Override
	public boolean existsById(Integer id) {
		return statusRepository.existsById(id);
	}

	@Override
	public long count() {
		return statusRepository.count();
	}
	@Override
	public Status update(int id, Status status)
	{
		Status entity = findById(id);
		entity.setName(status.getName());
		return this.statusRepository.save(entity);
	}
	
	@Override
	public Status findByEnum(StatusEnum status)
	{
		switch (status) {
		case Active: 
			return findById(StatusCommon.STATUS_ACTIVE_ID);
		case Disable:
			return findById(StatusCommon.STATUS_DISABLE_ID);
		case Deleted:
			return findById(StatusCommon.STATUS_DELETED_ID);
		default:
			return findById(StatusCommon.STATUS_PENDING_ID);
		}
	}
	
}
