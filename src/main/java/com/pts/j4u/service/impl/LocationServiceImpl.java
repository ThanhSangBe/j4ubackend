package com.pts.j4u.service.impl;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.DistrictDTO;
import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.dto.ProvinceDTO;
import com.pts.j4u.entity.District;
import com.pts.j4u.entity.Location;
import com.pts.j4u.entity.Province;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.mapper.DistrictMapper;
import com.pts.j4u.mapper.LocationMapper;
import com.pts.j4u.mapper.ProvinceMapper;
import com.pts.j4u.repository.DistrictRepository;
import com.pts.j4u.repository.LocationRepository;
import com.pts.j4u.repository.ProvinceRepository;
import com.pts.j4u.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private LocationMapper locationMapper;
    @Autowired
    private ProvinceRepository provinceRepository;
    @Autowired
    private ProvinceMapper provinceMapper;
    @Autowired
    private DistrictRepository districtRepository;
    @Autowired
    private DistrictMapper districtMapper;

    @Override
    public Location update(Location location, Long id) {
        Location entity = findById(id);
        entity.setAddress(location.getAddress());
        entity.setNote(location.getNote());
        entity.setDistrict(location.getDistrict());
        Location saved = this.locationRepository.save(entity);
        return saved;
    }

    @Override
    public List<LocationDTO> update(List<LocationDTO> locationDTOs) {
        List<LocationDTO> locations = new ArrayList<LocationDTO>();
        locationDTOs.forEach(item -> {
            Location location = this.locationMapper.mapToEntity(item);
            Location updated = update(location, location.getId());
            LocationDTO dto = this.locationMapper.mapToDTO(updated);
            locations.add(dto);
        });
        return locations;
    }

    public Location findById(Long id) {
        Location entity = this.locationRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(messageSource.getMessage("NotFound", null, null), "Location", "Id", String.valueOf(id)),
                TypeError.NotFound));
        return entity;
    }

    @Override
    public boolean compareList(List<LocationDTO> entities, List<LocationDTO> dtos) {
        if (dtos == null || entities == null)
            return false;
        int sizeDTO = dtos.size();
        int sizeEntity = entities.size();
        if (sizeDTO != sizeEntity)
            return false;
        else {
            for (int i = 0; i < sizeDTO; i++) {
                String addressDTO = dtos.get(i).getAddress();
                String addressEntity = entities.get(i).getAddress();
                String noteDTO = dtos.get(i).getNote();
                String noteEntity = entities.get(i).getNote();
                District districtDTO = dtos.get(i).getDistrict();
                District districtEntity = entities.get(i).getDistrict();
                if (!addressDTO.equals(addressEntity) || !noteDTO.equals(noteEntity)
                        || !districtDTO.equals(districtEntity))
                    return false;
            }
        }
        return true;
    }

    @Override
    public List<ProvinceDTO> getProvinces() {
        List<Province> provinces = provinceRepository.findAll();
        return provinces.stream().map(province -> this.provinceMapper.mapProvinceDTO(province)).collect(Collectors.toList());
    }

    @Override
    public List<DistrictDTO> getDistrictsByProvincesId(long id) {
        List<District> districts = this.districtRepository.findByProvinceId(id);
        if (districts != null && districts.size() > 0)
            return districts.stream().map(district -> this.districtMapper.mapDistrictDTO(district)).collect(Collectors.toList());
        else return null;
    }


    @Override
    public void setIdDTO(List<LocationDTO> dto, List<LocationDTO> entity) {
        int size = entity.size();
        for (int i = 0; i < size; i++) {
            dto.get(i).setId(entity.get(i).getId());
        }
    }


}
