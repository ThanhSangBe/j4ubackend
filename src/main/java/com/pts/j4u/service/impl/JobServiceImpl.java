package com.pts.j4u.service.impl;

import com.pts.j4u.common.Constant;
import com.pts.j4u.common.StatusCommon;
import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.JobCreateDTO;
import com.pts.j4u.dto.JobDTO;
import com.pts.j4u.dto.PaginationDTO;
import com.pts.j4u.entity.*;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.exception.ErrorValidationException;
import com.pts.j4u.mapper.JobMapper;
import com.pts.j4u.repository.JobRepository;
import com.pts.j4u.repository.specification.JobSpecification;
import com.pts.j4u.service.CompanyService;
import com.pts.j4u.service.JobService;
import com.pts.j4u.service.JobTypeJobService;
import com.pts.j4u.service.StatusService;
import com.pts.j4u.util.HandleDateTime;
import com.pts.j4u.util.impl.HandleDateTimeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class JobServiceImpl implements JobService {
    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private HandleDateTime handleDateTime;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private JobTypeJobService jobTypeJobService;
    @Autowired
    private JobMapper jobMapper;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private StatusService statusService;
    @Autowired
    EntityManager entityManager;
    private List<Long> listIdJobToPendingDisableAferTimeExpired = new ArrayList<>();

    @Override
    public JobCreateDTO create(JobCreateDTO jobCreateDTO) {
        // valids
        valid(jobCreateDTO);
        Job job = this.jobMapper.mapToEntity(jobCreateDTO);
        // set Date Time
        setTime(job);
        // set Status pending
        setStatus(job);
        // set Company
        Company company = this.companyService.getById(jobCreateDTO.getCompany().getId());
        job.setCompany(company);
        List<JobType> jobTypes = jobCreateDTO.getJobTypes();
        // save jobtype and job
        List<JobTypeJob> saved = this.jobTypeJobService.save(jobTypes, job);
        Job jobSaved = saved.get(0).getJob();
        listIdJobToPendingDisableAferTimeExpired.add(jobSaved.getId());
        return this.jobMapper.mapToDTO(jobSaved);
    }

    @Override
    public JobDTO update(JobCreateDTO jobDTO, Long id) {
        // check exists by Id
        Job job = findById(id);
        // valid
        valid(jobDTO);
        // map to update
        this.jobMapper.mapUpdate(job, jobDTO);
        // update job type
        this.jobTypeJobService.update(job, jobDTO.getJobTypes());

        Job jobUpdated = this.jobRepository.save(job);
        return this.jobMapper.mapToSimple(job);
    }

    private void setTime(Job job) {
        LocalDateTime dateTime = LocalDateTime.now();
        job.setCreateDate(dateTime);
        job.setModifiedDate(dateTime);
    }

    private void valid(JobCreateDTO jobCreateDTO) {
        String end = jobCreateDTO.getEnd();
        Map<String, String> valids = new HashMap<>();
        // not null
        checkNull(jobCreateDTO, valids);
        if (valids.size() > 0)
            throw new ErrorValidationException(valids);
        // valid time start and end
        String start = jobCreateDTO.getStart();
        if (start == null || start.equals("")) {
            jobCreateDTO.setStart(LocalDate.now().format(HandleDateTimeImpl.DATE_FORMAT));
            start = jobCreateDTO.getStart();
        }
        handleDateTime.validStartEndTime(start, end);
    }

    private void checkNull(JobCreateDTO jobCreateDTO, Map<String, String> valids) {
        String name = jobCreateDTO.getName();
        String description = jobCreateDTO.getDescription();
        String requirement = jobCreateDTO.getRequirement();
        String end = jobCreateDTO.getEnd();
        List<JobType> jobTypes = jobCreateDTO.getJobTypes();
        if (name == null || description == null || requirement == null || end == null || jobTypes == null) {
            if (name == null)
                valids.put("name", this.messageSource.getMessage("valid.name.notempty", null, null));
            if (description == null)
                valids.put("description", this.messageSource.getMessage("valid.description.notempty", null, null));
            if (requirement == null)
                valids.put("requirement", this.messageSource.getMessage("valid.requirement.notempty", null, null));
            if (jobTypes == null || jobTypes.size() == 0)
                valids.put("job-type", this.messageSource.getMessage("valid.jobtype.notempty", null, null));
            if (end == null)
                valids.put("date-end", this.messageSource.getMessage("valid.dateend.notempty", null, null));
        }
    }

    private void setStatus(Job job) {
        Status pending = statusService.findByEnum(StatusEnum.Pending);
        job.setStatus(pending);
    }

    @Override
    public void deleteById(long id) {
        Job job = findById(id);
        Status deleted = this.statusService.findByEnum(StatusEnum.Deleted);
        job.setStatus(deleted);
        this.jobRepository.save(job);
    }

    @Override
    public Job findById(Long id) {
        Job job = this.jobRepository.findById(id)
                .orElseThrow(() -> new ErrorMessageException(String
                        .format(this.messageSource.getMessage("NotFound", null, null), "Job", "Id", String.valueOf(id)),
                        TypeError.NotFound));
        return job;
    }

    @Override
    public JobCreateDTO getDetailById(long id) {
        Job job = findById(id);
        return this.jobMapper.mapToDTO(job);
    }

    @Override
    public PaginationDTO findByCompanyId(Long id, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Job> jobPage = this.jobRepository.findByCompanyId(id, pageable);
        List<JobDTO> jobSimpleDTOS = new ArrayList<>();
        jobPage.getContent().forEach(item -> {
            jobSimpleDTOS.add(this.jobMapper.mapToSimple(item));
        });
        return new PaginationDTO(jobSimpleDTOS, jobPage.isFirst(), jobPage.isLast(), jobPage.getTotalPages(), jobPage.getSize(),
                jobPage.getTotalElements(), jobPage.getNumber(), jobPage.getNumber());
    }


    public PaginationDTO findAll(int no, int limit, String status) {
        Pageable pageable = PageRequest.of(no, limit);
        Page<Job> jobPage = checkCodeStatus(pageable, status);
        // check code statu
        List<JobCreateDTO> jobs = new ArrayList<>();
        jobPage.getContent().forEach(job -> {
            JobCreateDTO jobCreateDTO = this.jobMapper.mapToDTO(job);
            jobs.add(jobCreateDTO);
        });
        return new PaginationDTO(jobs, jobPage.isFirst(), jobPage.isLast(), jobPage.getTotalPages(), jobPage.getSize(),
                jobPage.getTotalElements(), jobPage.getNumber(), jobPage.getNumber());
    }

    @Override
    public void changeStatusListId(List<Long> idJobs, StatusEnum status) {
        if (idJobs != null && idJobs.size() > 0) {
            for (Long id : idJobs) {
                changeStatusById(id, status);
            }
        }
    }


    private void changeStatusById(Long id, StatusEnum statusEnum) {
        Job job = findById(id);
        Status status = this.statusService.findByEnum(statusEnum);
        job.setStatus(status);
        job.setModifiedDate(LocalDateTime.now());
        this.jobRepository.save(job);
    }

    private Page<Job> checkCodeStatus(Pageable pageable, String status) {
        if (status.equalsIgnoreCase(StatusCommon.STATUS_ACTIVE_NAME) || status.equalsIgnoreCase(StatusCommon.STATUS_DISABLE_NAME)
                || status.equalsIgnoreCase(StatusCommon.STATUS_PENDING_NAME) || status.equalsIgnoreCase(StatusCommon.STATUS_DELETED_NAME)
                || status.equalsIgnoreCase("")
        ) {
            if (status.equalsIgnoreCase(StatusCommon.STATUS_ACTIVE_NAME)) {
                return this.jobRepository.findAllByStatusId(pageable, StatusCommon.STATUS_ACTIVE_ID);
            } else if (status.equalsIgnoreCase(StatusCommon.STATUS_DISABLE_NAME)) {
                return this.jobRepository.findAllByStatusId(pageable, StatusCommon.STATUS_DISABLE_ID);
            } else if (status.equalsIgnoreCase(StatusCommon.STATUS_PENDING_NAME)) {
                return this.jobRepository.findAllByStatusId(pageable, StatusCommon.STATUS_PENDING_ID);
            } else if (status.equalsIgnoreCase(StatusCommon.STATUS_DELETED_NAME)) {
                return this.jobRepository.findAllByStatusId(pageable, StatusCommon.STATUS_DELETED_ID);
            } else {
                return this.jobRepository.findAll(pageable);
            }
        } else {
            throw new ErrorMessageException(this.messageSource.getMessage("input.value", null, null), TypeError.BadRequest);
        }
    }

    //test
    @Override
    public PaginationDTO filter(Map<String, String> params) {
        try {
            String name = params.get("name");
            String company = params.get("company");
            String jobTypeId = params.get("type");
            String provinceIdStr = params.get("province");
            String districtStr = params.get("district");
            //page
            String pageNumberStr = params.get("no");
            String pageSizeStr = params.get("limit");
            Integer pageNumber = pageNumberStr != null ? Integer.valueOf(pageNumberStr) : Constant.PAGE_NUMBER_DEFAULT;
            Integer pageSize = pageSizeStr != null ? Integer.valueOf(pageSizeStr) : Constant.PAGE_SIZE_DEFAULT;
            Pageable pageable = PageRequest.of(pageNumber, pageSize);
            Integer idJobType = jobTypeId != null ? Integer.valueOf(jobTypeId) : null;
            Long provinceId = provinceIdStr != null ? Long.valueOf(provinceIdStr) : null;
            Long districtId = districtStr != null ? Long.valueOf(districtStr) : null;
            Page<Job> pageJob = this.jobRepository.findAll(JobSpecification.findAll(name, company, idJobType, provinceId, districtId), pageable);
            PaginationDTO paginationDTO = new PaginationDTO();
            paginationDTO.setFirst(pageJob.isFirst());
            paginationDTO.setLast(pageJob.isLast());
            paginationDTO.setData(pageJob.getContent().stream().map(item -> this.jobMapper.mapToDTO(item)).collect(Collectors.toList()));
            paginationDTO.setTotalItems(pageJob.getTotalElements());
            paginationDTO.setNumberOfCurrentPage(pageJob.getNumberOfElements());
            paginationDTO.setPageCurrent(pageNumber);
            return paginationDTO;
        } catch (NumberFormatException e) {
            throw new ErrorMessageException("Input Invalid", TypeError.BadRequest);
        }

    }

    @Override
    public PaginationDTO filterVersion1(String name, String company, Integer type, Long province,
                                        Long district, Integer no, Integer limit) {
        try {
            Page<Job> pageJob = this.jobRepository.findAll(JobSpecification.findAll(name, company,
                    type, province, district), PageRequest.of(no, limit));
            PaginationDTO paginationDTO = new PaginationDTO();
            paginationDTO.setFirst(pageJob.isFirst());
            paginationDTO.setLast(pageJob.isLast());
            paginationDTO.setData(pageJob.getContent().stream().map(item -> this.jobMapper.mapToDTO(item)).collect(Collectors.toList()));
            paginationDTO.setTotalItems(pageJob.getTotalElements());
            paginationDTO.setNumberOfCurrentPage(pageJob.getNumberOfElements());
            paginationDTO.setPageCurrent(limit);
            return paginationDTO;
        } catch (NumberFormatException e) {
            throw new ErrorMessageException("Input Invalid", TypeError.BadRequest);
        }

    }

    @Scheduled(fixedDelay = Constant.ONE_HOUR)
    public void DisableJobAfterTimeExpireSchedule() {
        if (listIdJobToPendingDisableAferTimeExpired != null && listIdJobToPendingDisableAferTimeExpired.size() > 0) {
            LocalDateTime timeNow = LocalDateTime.now();
            for (Long idJob : listIdJobToPendingDisableAferTimeExpired) {
                Job jobCurrent = findById(idJob);
                LocalDateTime createDateJob = jobCurrent.getCreateDate();
                //get distance between time now and create job
                int dateDistance = (int) ChronoUnit.DAYS.between(jobCurrent.getCreateDate().toLocalDate(),timeNow.toLocalDate());
                if (Math.abs(dateDistance) >= Constant.DAY_EXPIRE_CHANGE_STATUS_PENDING_JOB) {
                    jobCurrent.setStatus(statusService.findByEnum(StatusEnum.Pending));
                    this.jobRepository.save(jobCurrent);
                }
            }

        }
    }
}
