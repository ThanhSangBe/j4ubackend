package com.pts.j4u.service.impl;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.entity.JobType;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.repository.JobTypeRepository;
import com.pts.j4u.service.JobTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobTypeServiceImpl implements JobTypeService {
    @Autowired
    private JobTypeRepository jobTypeRepository;
    @Autowired
    private MessageSource messageSource;

    @Override
    public List<JobType> findAll() {
        return this.jobTypeRepository.findAll();
    }

    @Override
    public JobType findById(Long id) {
        return this.jobTypeRepository.findById(id).orElseThrow(() -> new ErrorMessageException(
                String.format(this.messageSource.getMessage("NotFound", null, null), "JobType", "id", String.valueOf(id)),
                TypeError.NotFound
        ));
    }

    @Override
    public JobType save(JobType jobType) {
        return this.jobTypeRepository.save(jobType);
    }

    @Override
    public JobType findByName(String name) {
        return this.jobTypeRepository.findByName(name).orElseThrow(() -> new ErrorMessageException(
                String.format(this.messageSource.getMessage("NotFound", null, null), "JobType", "name", name)
                , TypeError.NotFound)
        );
    }
}
