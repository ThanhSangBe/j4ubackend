package com.pts.j4u.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.entity.Major;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.repository.MajorRepository;
import com.pts.j4u.service.MajorService;
@Service
public class MajorServiceImpl implements MajorService {
	@Autowired
	private MajorRepository majorRepository;
	@Autowired
	private MessageSource messageSource;

	@Override
	public List<Major> findAll() {
		return majorRepository.findAll();
	}

	@Override
	public Major findById(Long id) {
		return majorRepository.findById(id).orElseThrow(()-> new ErrorMessageException(
				String.format(this.messageSource.getMessage("NotFound", null,null), "Major","id",String.valueOf(id))
				, TypeError.NotFound));
	}

	@Override
	public boolean existsById(Long id) {
		return majorRepository.existsById(id);
	}
	@Override
	public Major save(Major major)
	{
		LocalDate dateTime = LocalDate.now();
		major.setCreateDate(dateTime);
		major.setModifiedDate(dateTime);
		return this.majorRepository.save(major);
	}
	
}
