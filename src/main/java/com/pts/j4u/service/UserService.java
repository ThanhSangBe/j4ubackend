package com.pts.j4u.service;

import com.pts.j4u.dto.*;
import com.pts.j4u.dto.request.VerifyCodePasswordDTO;
import org.springframework.web.multipart.MultipartFile;

import com.pts.j4u.common.RoleEnum;
import com.pts.j4u.entity.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface UserService {

	PaginationDTO findAll(int pageNumber, int pageSize);

	UserDTO findById(Long id);

	User save(User user);

	User saveNewUser(String email, RoleEnum role);

    UserDTO update(Long userId, UserRegisterDTO userRegisterDTO);

    UserDTO register(UserRegisterDTO userRegisterDTO);

    User findByUsername(String username);

    Optional<User> getOptionalByUserName(String username);

    UserRegisterDTO readJson(String user);

    UserRegisterDTO readJson(String user, MultipartFile file);

    void uploadAvatar(UserRegisterDTO userRegisterDTO, User user);

	UserDTO changePassword(ChangePasswordDTO dto, long id);

	User getAndComparePassword(String username, String passDto);

	void delete(User user);

	User getById(long id);

	String fortgotPassword(String email);

    UserDTO resetPassword(VerifyCodePasswordDTO verifyCodePasswordDTO);

	boolean verifyCodeAndEmail(String email, String verifyCode);

	void updateListRoleId(List<Integer> roleIds, Long userId);


	PaginationDTO findUserByRoleId(Integer roleId, Integer pageNumber, Integer pageSize);

	Map handleUserGoogle(GoogleAccountDTO account);
}
