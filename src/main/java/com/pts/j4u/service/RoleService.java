package com.pts.j4u.service;

import java.util.List;
import java.util.Optional;

import com.pts.j4u.common.RoleEnum;
import com.pts.j4u.entity.Role;

public interface RoleService {

	boolean existsById(Integer id);

	Role findById(Integer id);

	List<Role> findAll();

	Role save(Role entity);

	Role update(Integer id, Role role);

	Role findByEnum(RoleEnum role);

}
