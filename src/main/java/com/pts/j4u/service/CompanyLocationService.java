package com.pts.j4u.service;

import com.pts.j4u.entity.CompanyLocation;

import java.util.List;

public interface CompanyLocationService {
    List<CompanyLocation> getByCompanyId(Long id);
}
