package com.pts.j4u.service;

import com.pts.j4u.entity.Job;
import com.pts.j4u.entity.JobType;
import com.pts.j4u.entity.JobTypeJob;

import java.util.List;

public interface JobTypeJobService {
    List<JobTypeJob> save(List<JobType> jobTypes, Job job);

    List<JobType> findByJobId(Long jobId);

    void update(Job job, List<JobType> jobTypesDTO);

    void deleteById(Long id);

    JobTypeJob save(JobType jobType, Job job);

    JobTypeJob findById(Long id);
}
