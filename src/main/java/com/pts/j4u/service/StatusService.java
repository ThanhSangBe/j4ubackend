package com.pts.j4u.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.entity.Status;

public interface StatusService  {

	Status update(int id, Status status);

	long count();

	boolean existsById(Integer id);

	Status findById(Integer id);

	Page<Status> findAll(Pageable pageable);

	Status save(Status entity);

	List<Status> findAll();

	Status findByEnum(StatusEnum status);

}
