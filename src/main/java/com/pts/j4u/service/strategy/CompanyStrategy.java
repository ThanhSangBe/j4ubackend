package com.pts.j4u.service.strategy;

import com.pts.j4u.dto.CompanyDTO;
import com.pts.j4u.dto.UserDTO;
import com.pts.j4u.entity.Company;

public interface CompanyStrategy {
	public com.pts.j4u.entity.Company save(CompanyDTO companyDTO);
	public Company register(CompanyDTO companyDTO, UserDTO userDTO);
}
