package com.pts.j4u.service.strategy.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.pts.j4u.dto.UserDTO;
import com.pts.j4u.entity.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pts.j4u.common.RoleEnum;
import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.CompanyDTO;
import com.pts.j4u.entity.Status;
import com.pts.j4u.entity.User;
import com.pts.j4u.mapper.CompanyMapper;
import com.pts.j4u.repository.CompanyRepository;
import com.pts.j4u.service.StatusService;
import com.pts.j4u.service.UserService;
import com.pts.j4u.service.strategy.CompanyStrategy;

@Component("Company")
public class CompanyStrategyImpl implements CompanyStrategy {
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private StatusService statusService;
	@Autowired
	private UserService userService;
	@Autowired
	private CompanyMapper companyMapper;
	@Override
	public com.pts.j4u.entity.Company save(CompanyDTO companyDTO) {
		com.pts.j4u.entity.Company entity = companyMapper.mapToEntity(companyDTO);
		//save company
		Status pending = statusService.findByEnum(StatusEnum.Pending);
		entity.setStatus(pending);
		entity.setCreateDate(LocalDate.now());
		entity.setModifiedDate(LocalDate.now());
		//handler user
		User userSaved = this.userService.saveNewUser(entity.getEmail(), RoleEnum.Company);
		List<User> users = new ArrayList<>(); users.add(userSaved);
		entity.setUsers(users);
		return this.companyRepository.save(entity);
	}

	@Override
	public Company register(CompanyDTO companyDTO, UserDTO userDTO) {
		return null;
	}

	public CompanyStrategyImpl()
	{	
	}

}
