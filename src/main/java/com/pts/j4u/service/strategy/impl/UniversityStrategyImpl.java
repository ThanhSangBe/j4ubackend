package com.pts.j4u.service.strategy.impl;

import com.pts.j4u.common.RoleEnum;
import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.dto.UniversityDTO;
import com.pts.j4u.entity.*;
import com.pts.j4u.mapper.UniversityMapper;
import com.pts.j4u.repository.DistrictRepository;
import com.pts.j4u.repository.UniversityRepository;
import com.pts.j4u.service.StatusService;
import com.pts.j4u.service.UserService;
import com.pts.j4u.service.strategy.UniversityStrategy;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("UniversityStrategy")
public class UniversityStrategyImpl implements UniversityStrategy {
    @Autowired
    private StatusService statusService;
    @Autowired
    private UniversityMapper universityMapper;
    @Autowired
    private UniversityRepository universityRepository;
    @Autowired
    private UserService userService;

    @Override
    public UniversityDTO save(UniversityDTO dto) {
        University university = universityMapper.mapToEntity(dto);
        university.setCreateDate(LocalDate.now());
        university.setModifiedDate(LocalDate.now());
        setPendingStatus(university);
//        createUser(university);
        University universitySaved = this.universityRepository.save(university);
        UniversityDTO dtoSaved = this.universityMapper.mapToDTO(universitySaved);
        return dtoSaved;
    }
    private void setPendingStatus(University university)
    {
        Status pending = statusService.findByEnum(StatusEnum.Pending);
        university.setStatus(pending);
    }
    private void createUser(University entity)
    {
        String email = entity.getEmail();
        User userSaved = this.userService.saveNewUser(email, RoleEnum.University);
        entity.setUser(userSaved);
    }

}
