package com.pts.j4u.service.strategy.impl;

import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.dto.UniversityDTO;
import com.pts.j4u.entity.District;
import com.pts.j4u.entity.Location;
import com.pts.j4u.entity.University;
import com.pts.j4u.entity.UniversityLocation;
import com.pts.j4u.mapper.LocationMapper;
import com.pts.j4u.mapper.UniversityMapper;
import com.pts.j4u.repository.DistrictRepository;
import com.pts.j4u.repository.UniversityLocationRepository;
import com.pts.j4u.repository.UniversityRepository;
import com.pts.j4u.service.strategy.UniversityStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

@Component("UniversityLocationStrategy")
public class UniversityLocationStrategyImpl implements UniversityStrategy {
	@Resource(name = "UniversityStrategy")
	private UniversityStrategy universityStrategy;
	@Autowired
	private DistrictRepository districtRepository;
	@Autowired
	private UniversityMapper universityMapper;
	@Autowired
	private LocationMapper locationMapper;
	@Autowired
	private UniversityLocationRepository universityLocationRepository;
	@Autowired
	private UniversityRepository universityRepository;

	@Override
	public UniversityDTO save(UniversityDTO dto) {
		UniversityDTO universitySaved = universityStrategy.save(dto);
		University entity = this.universityRepository.findById(universitySaved.getId()).get();
		List<LocationDTO> locationDTOs = dto.getLocations();
		List<Location> locations = setLocation(locationDTOs);
		locations.forEach(location -> {
			UniversityLocation universityLocation = new UniversityLocation();
			universityLocation.setLocation(location);
			universityLocation.setUniversity(entity);
			this.universityLocationRepository.save(universityLocation);
		});

		return universitySaved;
	}

	private List<Location> setLocation(List<LocationDTO> dtos) {
		List<Location> locations = new ArrayList<Location>();
		dtos.forEach(locationDTO -> {
			District district = districtRepository.findById(locationDTO.getDistrict().getId()).get();
			locationDTO.setDistrict(district);
			Location location = locationMapper.mapToEntity(locationDTO);
			locations.add(location);
		});
		return locations;
	}
}
