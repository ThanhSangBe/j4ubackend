package com.pts.j4u.service.strategy;

import com.pts.j4u.dto.UniversityDTO;
import com.pts.j4u.dto.UserRegisterDTO;

public interface UniversityStrategy {
    UniversityDTO save(UniversityDTO dto);
   
}
