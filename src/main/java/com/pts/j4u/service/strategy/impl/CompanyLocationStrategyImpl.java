package com.pts.j4u.service.strategy.impl;

import com.pts.j4u.common.RoleEnum;
import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.common.TypeError;
import com.pts.j4u.dto.CompanyDTO;
import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.dto.UserDTO;
import com.pts.j4u.entity.*;
import com.pts.j4u.exception.ErrorMessageException;
import com.pts.j4u.mapper.CompanyMapper;
import com.pts.j4u.mapper.LocationMapper;
import com.pts.j4u.mapper.UserMapper;
import com.pts.j4u.repository.CompanyLocationRepository;
import com.pts.j4u.repository.CompanyRepository;
import com.pts.j4u.repository.DistrictRepository;
import com.pts.j4u.repository.UserRepository;
import com.pts.j4u.service.StatusService;
import com.pts.j4u.service.UserService;
import com.pts.j4u.service.strategy.CompanyStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component("CompanyLocation")
@Transactional
public class CompanyLocationStrategyImpl implements CompanyStrategy {
    @Autowired
    private StatusService statusService;
    @Autowired
    private UserService userService;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private DistrictRepository districtRepository;
    @Autowired
    private LocationMapper locationMapper;
    @Autowired
    private CompanyLocationRepository companyLocationRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CompanyRepository companyRepository;

    public CompanyLocationStrategyImpl() {
    }

    @Override
    public com.pts.j4u.entity.Company save(CompanyDTO companyDTO) {
        Company company = setCompany(companyDTO);
        // handler user
        User userSaved = this.userService.saveNewUser(company.getEmail(), RoleEnum.Company);
        List<User> users = new ArrayList<>();
        users.add(userSaved);
        company.setUsers(users);
        return saveCompanyLocation(companyDTO, company);
    }

    public Company register(CompanyDTO companyDTO, UserDTO userDTO) {
        Company company = setCompany(companyDTO);
        //Check exists Email Company
        String emailCompany = company.getEmail();
        boolean isExistsEmail = this.companyRepository.existsByEmail(emailCompany);
        if (isExistsEmail) {
            throw new ErrorMessageException("Email is exists!", TypeError.BadRequest);
        }
        //set user saved
        User user = this.userRepository.findById(userDTO.getId()).get();
        List<User> users = new ArrayList<>();
        users.add(user);
        company.setUsers(users);
        return saveCompanyLocation(companyDTO, company);
    }

    private Company saveCompanyLocation(CompanyDTO companyDTO, Company company) {
        List<LocationDTO> locationDTOs = companyDTO.getLocations();
        CompanyLocation companyLocationSaved = new CompanyLocation();
        for (LocationDTO locationDTO : locationDTOs) {
            District district = districtRepository.findById(locationDTO.getDistrict().getId()).get();
            locationDTO.setDistrict(district);
            Location location = locationMapper.mapToEntity(locationDTO);
            CompanyLocation companyLocation = new CompanyLocation();
            companyLocation.setCompany(company);
            companyLocation.setLocation(location);
            companyLocationSaved = companyLocationRepository.save(companyLocation);
        }
        return companyLocationSaved.getCompany();
    }

    private Company setCompany(CompanyDTO companyDTO) {
        Company company = this.companyMapper.mapToEntity(companyDTO);
        // save company
        Status pending = statusService.findByEnum(StatusEnum.Pending);
        company.setStatus(pending);
        company.setCreateDate(LocalDate.now());
        company.setModifiedDate(LocalDate.now());
        return company;
    }

}
