package com.pts.j4u.service;

import java.util.List;

import com.pts.j4u.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.entity.Company;
import org.springframework.web.multipart.MultipartFile;

public interface CompanyService {

	long count();

	boolean existsById(Long id);

	CompanyDTO findById(Long id);

	CompanyDTO register(CompanyRegisterDTO companyRegisterDTO);

	Page<Company> findAll(Pageable pageable);

	Company save(CompanyDTO entity);

	List<Company> findAll();

	Company changeStatus(Long id, StatusEnum status);

	CompanyDTO update(Long id, CompanyDTO companyDTO);

	Company getById(long id);

	List<CompanyDTO> findByStatus(Integer id);

	PaginationDTO findAll(int no, int limit);

	CompanyCreateDTO readJson(String company, MultipartFile file);

	CompanyRegisterDTO readJsonRegister(String company, MultipartFile file);

	CompanyDTO save(CompanyCreateDTO dto);

	CompanySimpleDTO findByIdAndJobs(Long id);

	PaginationDTO findAll(String name, int status, int no, int limit);

	void deleteListId(List<Long> listId);

	void activeListId(List<Long> listId);

	void disableListId(List<Long> listId);

	void deleteForce(Long id);

	void deleteForceByList(List<Long> listId);

	CompanyDTO update(CompanyCreateDTO companyCreateDto, Long id);

	List<CandidateDTO> getCandidatesByIdJob(Long id);

    CompanyDTO getByUserId(long userId);

	List<UserDTO> getUsersByCompanyId(Long companyId);
}
