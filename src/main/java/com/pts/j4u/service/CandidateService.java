package com.pts.j4u.service;

import com.pts.j4u.dto.CandidateDTO;
import com.pts.j4u.dto.CandidateRegisterDTO;
import com.pts.j4u.dto.JobDTO;
import com.pts.j4u.dto.PaginationDTO;
import com.pts.j4u.entity.Candidate;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface CandidateService {

    CandidateDTO create(CandidateRegisterDTO candidateRegisterDTO);

    CandidateRegisterDTO readJson(String candidate, MultipartFile fileCV, MultipartFile fileAvatar);

	CandidateDTO update(CandidateRegisterDTO candidateRegisterDTO, Long candidateId);

    CandidateDTO getCandidateByUserId(long userId);

    PaginationDTO search(String firstName, String lastName, int pageNumber, int pagteSize);

    CandidateDTO findById(Long id);

	Candidate getById(Long id);

    void deleteById(Long candidateId);

    boolean checkHaveCV(Candidate candidate);

    List<CandidateDTO> findByFirstNameOrLastName(String name);

    List<CandidateDTO> getListCandidateAppliedByIdCompanyAndIdJob(Long idJob);

	List<JobDTO> getListJobByIdCandidate(Long idCandidate);

    void uploadFileCv(Long candidateId, MultipartFile fileCV);

    CandidateDTO updateRole(Long candidateId, Integer statusId);
}
