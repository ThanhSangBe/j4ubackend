package com.pts.j4u.service;

import java.util.List;

import com.pts.j4u.entity.Major;

public interface MajorService {

	boolean existsById(Long id);

	Major findById(Long id);

	List<Major> findAll();

    Major save(Major major);
}
