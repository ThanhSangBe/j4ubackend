package com.pts.j4u.service;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.JobCreateDTO;
import com.pts.j4u.dto.JobDTO;
import com.pts.j4u.dto.PaginationDTO;
import com.pts.j4u.entity.Job;

import java.util.List;
import java.util.Map;

public interface JobService {

    JobCreateDTO create(JobCreateDTO jobCreateDTO);

    JobDTO update(JobCreateDTO jobDTO, Long id);

    void deleteById(long id);

    PaginationDTO findByCompanyId(Long id, int page, int size);

    PaginationDTO findAll(int no, int limit,String status);

	Job findById(Long id);

	JobCreateDTO getDetailById(long id);

	void changeStatusListId(List<Long> idJobs, StatusEnum status);

    PaginationDTO filter(Map<String, String> params);

    PaginationDTO filterVersion1(String name, String company, Integer type, Long province,
                                 Long district, Integer no, Integer limit);
}
