package com.pts.j4u.service;

import com.pts.j4u.dto.JobDTO;
import com.pts.j4u.dto.LikeJobDTO;
import com.pts.j4u.dto.PaginationDTO;
import org.springframework.data.domain.Page;

public interface LikeJobService {
    LikeJobDTO save(Long jobId, Long candidateId);

    void delete(Long jobId, Long candidateId);

    PaginationDTO getJobdLikedByCandidateId(Long candidateId, Integer pageNumber, Integer pageSize);
}
