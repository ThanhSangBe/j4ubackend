package com.pts.j4u.service;

import com.pts.j4u.entity.JobType;

import java.util.List;

public interface JobTypeService {

    List<JobType> findAll();

    JobType findById(Long id);

    JobType save(JobType jobType);

    JobType findByName(String name);
}
