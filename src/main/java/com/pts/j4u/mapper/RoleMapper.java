package com.pts.j4u.mapper;

import com.pts.j4u.dto.RoleDTO;
import com.pts.j4u.entity.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper {
    Role toRole(RoleDTO roleDTO);

    RoleDTO toRoleDto(Role role);
}
