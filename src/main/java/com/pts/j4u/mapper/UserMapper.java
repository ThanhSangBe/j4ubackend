package com.pts.j4u.mapper;

import com.pts.j4u.dto.UserDTO;
import com.pts.j4u.dto.UserRegisterDTO;
import com.pts.j4u.entity.User;

import java.util.List;

//@Mapper(componentModel = "spring")
public interface UserMapper {

	UserDTO mapToDTO(User user);

    List<UserDTO> mapToDTO(List<User> users);

    User mapToEntity(UserDTO user);

    User mapToEntity(UserRegisterDTO userRegisterDTO);

    void mapToUpdate(User user, UserRegisterDTO userRegisterDTO);
}
