package com.pts.j4u.mapper;

import com.pts.j4u.dto.StatusDTO;
import com.pts.j4u.entity.Status;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface StatusMapper {
    Status toStatus(StatusDTO statusDTO);
    StatusDTO toStatusDTO(Status status);
    @Mapping(target = "name",source = "nameStatus")
    List<Status> toStatusList(List<StatusDTO> statusDTOs);
}
