package com.pts.j4u.mapper;

import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.entity.Location;
//@Mapper(componentModel = "spring")
public interface LocationMapper {

	Location mapToEntity(LocationDTO dto);

	LocationDTO mapToDTO(Location entity);

}
