package com.pts.j4u.mapper;

import com.pts.j4u.dto.JobCreateDTO;
import com.pts.j4u.dto.JobDTO;
import com.pts.j4u.entity.Job;

public interface JobMapper {
    Job mapToEntity(JobCreateDTO dto);

    void mapUpdate(Job job, JobCreateDTO dto);

    JobCreateDTO mapToDTO(Job entity);

    JobDTO mapToSimple(Job job);
}
