package com.pts.j4u.mapper;

import com.pts.j4u.dto.LikeJobDTO;
import com.pts.j4u.entity.LikeJob;

public interface LikeJobMapper {
    LikeJobDTO mapToDto(LikeJob likeJobEntity);
}
