package com.pts.j4u.mapper;

import com.pts.j4u.dto.DemandDTO;
import com.pts.j4u.entity.Demand;

public interface DemandMapper {

	DemandDTO mapToDTO(Demand demand);

	Demand mapToEntity(DemandDTO demandDTO);

	void mapToUpdate(Demand demand, DemandDTO demandDTO);

}
