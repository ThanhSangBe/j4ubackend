package com.pts.j4u.mapper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pts.j4u.dto.DemandDTO;
import com.pts.j4u.entity.Demand;
import com.pts.j4u.mapper.DemandMapper;
import com.pts.j4u.mapper.UniversityMapper;
import com.pts.j4u.util.HandleDateTime;

@Component
public class DemandMapperImpl implements DemandMapper {
	@Autowired
	private UniversityMapper universityMapper;
	@Autowired
	private HandleDateTime handleDateTime;

	@Override
	public Demand mapToEntity(DemandDTO demandDTO) {
		Demand demand = new Demand();
		demand.setId(demandDTO.getId());
		demand.setAmount(demandDTO.getAmount());
		demand.setName(demandDTO.getName());
		demand.setDesciption(demandDTO.getDesciption());
		demand.setRequirement(demandDTO.getRequirement());
		demand.setOrtherInfo(demandDTO.getOrtherInfo());
		demand.setFiles(demandDTO.getFiles());
		demand.setStatus(demandDTO.getStatus());
		demand.setUniversity(universityMapper.mapToEntity(demandDTO.getUniversityDTO()));
		return demand;
	}

	@Override
	public DemandDTO mapToDTO(Demand demand) {
		DemandDTO demandDTO = new DemandDTO();
		demandDTO.setId(demand.getId());
		demandDTO.setAmount(demand.getAmount());
		demandDTO.setName(demand.getName());
		demandDTO.setDesciption(demand.getDesciption());
		demandDTO.setRequirement(demand.getRequirement());
		demandDTO.setOrtherInfo(demand.getOrtherInfo());
		demandDTO.setFiles(demand.getFiles());
		demandDTO.setStatus(demand.getStatus());
		demandDTO.setStartDate(handleDateTime.parse(demand.getStart()));
		demandDTO.setEndDate(handleDateTime.parse(demand.getEnd()));
		demandDTO.setUniversityDTO(universityMapper.mapToDTO(demand.getUniversity()));
		return demandDTO;
	}

	@Override
	public void mapToUpdate(Demand demand, DemandDTO demandDTO) {
		demand.setName(demandDTO.getName());
		demand.setAmount(demandDTO.getAmount());
		demand.setDesciption(demandDTO.getDesciption());
		demand.setOrtherInfo(demandDTO.getOrtherInfo());
		demand.setFiles(demandDTO.getFiles());
		demand.setRequirement(demandDTO.getRequirement());
	}
}
