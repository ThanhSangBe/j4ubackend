package com.pts.j4u.mapper.impl;

import com.pts.j4u.dto.CandidateDTO;
import com.pts.j4u.dto.CandidateRegisterDTO;
import com.pts.j4u.entity.Candidate;
import com.pts.j4u.mapper.CandidateMapper;
import com.pts.j4u.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CandidateMapperImpl implements CandidateMapper {
    @Autowired
    private UserMapper userMapper;
    @Override
    public Candidate mapToEntity(CandidateRegisterDTO candidateRegisterDTO)
    {
        Candidate candidate = new Candidate();
        candidate.setCV(candidateRegisterDTO.getCv());
        candidate.setMajor(candidateRegisterDTO.getMajor());
        candidate.setUser(userMapper.mapToEntity(candidateRegisterDTO.getUser()));
        return candidate;
    }
    @Override
    public CandidateDTO mapToDTO(Candidate entity)
    {
        CandidateDTO dto = new CandidateDTO();
        dto.setCv(entity.getCV());
        dto.setMajor(entity.getMajor());
        dto.setUserDTO(userMapper.mapToDTO(entity.getUser()));
        dto.setId(entity.getId());
        return dto;
    }
}
