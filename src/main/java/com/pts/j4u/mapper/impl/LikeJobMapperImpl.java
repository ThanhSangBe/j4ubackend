package com.pts.j4u.mapper.impl;

import com.pts.j4u.dto.LikeJobDTO;
import com.pts.j4u.entity.LikeJob;
import com.pts.j4u.mapper.CandidateMapper;
import com.pts.j4u.mapper.JobMapper;
import com.pts.j4u.mapper.LikeJobMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LikeJobMapperImpl implements LikeJobMapper {
    @Autowired
    private CandidateMapper candidateMapper;
    @Autowired
    private JobMapper jobMapper;

    @Override
    public LikeJobDTO mapToDto(LikeJob likeJobEntity) {
        if (likeJobEntity == null)
            return null;
        LikeJobDTO likeJobDTO = new LikeJobDTO();
        likeJobDTO.setTimeLike(likeJobDTO.getTimeLike());
        likeJobDTO.setCandidate(candidateMapper.mapToDTO(likeJobEntity.getCandidate()));
        likeJobDTO.setJob(jobMapper.mapToDTO(likeJobEntity.getJob()));
        return likeJobDTO;
    }
}
