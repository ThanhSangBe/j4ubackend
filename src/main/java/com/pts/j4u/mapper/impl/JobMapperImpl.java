package com.pts.j4u.mapper.impl;

import com.pts.j4u.dto.JobCreateDTO;
import com.pts.j4u.dto.JobDTO;
import com.pts.j4u.entity.Job;
import com.pts.j4u.mapper.CompanyMapper;
import com.pts.j4u.mapper.JobMapper;
import com.pts.j4u.mapper.StatusMapper;
import com.pts.j4u.service.JobTypeJobService;
import com.pts.j4u.util.HandleDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class JobMapperImpl implements JobMapper {
    @Autowired
    private JobTypeJobService jobTypeJobService;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private HandleDateTime handleDateTime;
    @Autowired
    private StatusMapper statusMapper;

    @Override
    public Job mapToEntity(JobCreateDTO dto) {
        Job entity = new Job();
        entity.setAmount(dto.getAmount());
        entity.setDescription(dto.getDescription());
        entity.setFiles(dto.getFiles());
        entity.setRequirement(dto.getRequirement());
        entity.setOrtherInfo(dto.getOrtherInfo());
        entity.setName(dto.getName());
        entity.setStart(handleDateTime.parse(dto.getStart()));
        entity.setEnd(handleDateTime.parse(dto.getEnd()));
        entity.setStatus(statusMapper.toStatus(dto.getStatus()));
        entity.setSalary(dto.getSalary());
        return entity;
    }

    @Override
    public void mapUpdate(Job job, JobCreateDTO dto) {
        job.setName(dto.getName());
        job.setDescription(dto.getDescription());
        job.setStart(handleDateTime.parse(dto.getStart()));
        job.setEnd(handleDateTime.parse(dto.getEnd()));
        job.setFiles(dto.getFiles());
        job.setOrtherInfo(dto.getOrtherInfo());
        job.setModifiedDate(LocalDateTime.now());
        job.setRequirement(dto.getRequirement());
        job.setAmount(dto.getAmount());
        job.setSalary(dto.getSalary());

    }

    @Override
    public JobCreateDTO mapToDTO(Job entity) {
        JobCreateDTO dto = new JobCreateDTO();
        dto.setId(entity.getId());
        dto.setAmount(entity.getAmount());
        dto.setDescription(entity.getDescription());
        dto.setFiles(entity.getFiles());
        dto.setRequirement(entity.getRequirement());
        dto.setOrtherInfo(entity.getOrtherInfo());
        dto.setName(entity.getName());
        dto.setStart(handleDateTime.parse(entity.getStart()));
        dto.setEnd(handleDateTime.parse(entity.getEnd()));
        dto.setJobTypes(jobTypeJobService.findByJobId(dto.getId()));
        dto.setCompany(companyMapper.mapToSimple(entity.getCompany()));
        dto.setStatus(statusMapper.toStatusDTO(entity.getStatus()));
        dto.setSalary(entity.getSalary());
        return dto;
    }

    @Override
    public JobDTO mapToSimple(Job job) {
        JobDTO jobSimpleDTO = new JobDTO();
        jobSimpleDTO.setId(job.getId());
        jobSimpleDTO.setName(job.getName());
        jobSimpleDTO.setStart(handleDateTime.parse(job.getStart()));
        jobSimpleDTO.setEnd(handleDateTime.parse(job.getEnd()));
        jobSimpleDTO.setJobTypes(jobTypeJobService.findByJobId(job.getId()));
        jobSimpleDTO.setStatus(statusMapper.toStatusDTO(job.getStatus()));
        jobSimpleDTO.setSalary(job.getSalary());
        return jobSimpleDTO;
    }

}
