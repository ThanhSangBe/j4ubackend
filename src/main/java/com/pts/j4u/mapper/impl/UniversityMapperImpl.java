package com.pts.j4u.mapper.impl;

import java.time.LocalDate;
import java.util.List;

import com.pts.j4u.dto.UniversityCreateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.dto.UniversityDTO;
import com.pts.j4u.entity.University;
import com.pts.j4u.mapper.UniversityMapper;
import com.pts.j4u.service.UniversityLocationService;
@Component
public class UniversityMapperImpl implements UniversityMapper {
	@Autowired
	private UniversityLocationService universityLocationService;

	@Override
	public University mapToEntity(UniversityDTO dto)
	{
		if(dto == null)
			return null;
		University university = new University();
		if(dto.getId() != null)
			university.setId(dto.getId());
		university.setName(dto.getName());
		university.setEmail(dto.getEmail());
		university.setShortName(dto.getShortName());
		university.setPhone(dto.getPhone());
		university.setDescription(dto.getDescription());
		university.setWebsite(dto.getWebsite());
		university.setAvatar(dto.getAvatar());
		return university;
	}
	@Override
	public UniversityDTO mapToDTO(University entity)
	{
		if(entity == null)
			return null;
		UniversityDTO dto = new UniversityDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setShortName(entity.getShortName());
		dto.setEmail(entity.getEmail());
		dto.setAvatar(entity.getAvatar());
		dto.setPhone(entity.getPhone());
		dto.setWebsite(entity.getWebsite());
		dto.setDescription(entity.getDescription());
		dto.setAvatar(entity.getAvatar());
		setLocationDTO(dto.getId(), dto);
		dto.setStatus(entity.getStatus());
		return dto;
	}
	@Override
	public UniversityDTO mapToDTO(UniversityCreateDTO entity)
	{
		UniversityDTO dto = new UniversityDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setShortName(entity.getShortName());
		dto.setEmail(entity.getEmail());
		dto.setAvatar(entity.getAvatar());
		dto.setPhone(entity.getPhone());
		dto.setWebsite(entity.getWebsite());
		dto.setDescription(entity.getDescription());
		dto.setAvatar(entity.getAvatar());
		dto.setLocations(entity.getLocations());
		return dto;
	}
	private void setLocationDTO(Long idUniversity, UniversityDTO dto)
	{
		List<LocationDTO> locationDTOs = this.universityLocationService.findByUniversity(idUniversity);
		dto.setLocations(locationDTOs);
	}
	
	@Override
	public void mapToUpdate(UniversityDTO dto, University entity)
	{
		entity.setModifiedDate(LocalDate.now());
		entity.setName(dto.getName());
		entity.setDescription(dto.getDescription());
		entity.setEmail(dto.getEmail());
		entity.setPhone(dto.getPhone());
		entity.setWebsite(dto.getWebsite());
	}

}
