package com.pts.j4u.mapper.impl;

import com.pts.j4u.dto.DistrictDTO;
import com.pts.j4u.entity.District;
import com.pts.j4u.mapper.DistrictMapper;
import org.springframework.stereotype.Component;

@Component
public class DistrictMapperImpl implements DistrictMapper {
    @Override
    public District mapToDistrict(DistrictDTO districtDTO) {
        District district = new District();
        district.setId(districtDTO.getId());
        district.setName(districtDTO.getName());
        return district;
    }

    @Override
    public DistrictDTO mapDistrictDTO(District district) {
        DistrictDTO districtDTO = new DistrictDTO();
        districtDTO.setId(district.getId());
        districtDTO.setName(district.getName());
        return districtDTO;
    }
}
