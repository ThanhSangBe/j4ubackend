package com.pts.j4u.mapper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pts.j4u.dto.ApplyJobDTO;
import com.pts.j4u.entity.ApplyJob;
import com.pts.j4u.mapper.ApplyJobMapper;
import com.pts.j4u.mapper.CandidateMapper;
import com.pts.j4u.mapper.JobMapper;
@Component
public class ApplyJobMapperImpl implements ApplyJobMapper {
	@Autowired
	private JobMapper jobMapper;
	@Autowired
	private CandidateMapper candidateMapper;
	@Override
	public ApplyJobDTO mapToDTO(ApplyJob entity)
	{
		ApplyJobDTO dto = new ApplyJobDTO();
		dto.setId(entity.getId());
		dto.setTimeApply(entity.getTimeApply());
		dto.setCandidate(candidateMapper.mapToDTO(entity.getCandidate()));
		dto.setJob(jobMapper.mapToSimple(entity.getJob()));
		return dto;
	}
}
