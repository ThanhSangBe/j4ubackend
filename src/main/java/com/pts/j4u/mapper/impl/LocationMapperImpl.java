package com.pts.j4u.mapper.impl;

import org.springframework.stereotype.Component;

import com.pts.j4u.dto.LocationDTO;
import com.pts.j4u.entity.Location;
import com.pts.j4u.mapper.LocationMapper;

@Component
public class LocationMapperImpl implements LocationMapper {
	
	@Override
	public Location mapToEntity(LocationDTO dto)
	{
		if(dto == null)
			return null;
		Location location = new Location();
		if(dto.getId() != null) location.setId(dto.getId());
		location.setDistrict(dto.getDistrict());
		location.setAddress(dto.getAddress());
		location.setNote(dto.getNote());
		return location;
	}
	
	@Override
	public LocationDTO mapToDTO(Location entity)
	{
		if(entity == null)
			return null;
		LocationDTO locationDTO = new LocationDTO();
		locationDTO.setId(entity.getId());
		locationDTO.setAddress(entity.getAddress());
		locationDTO.setNote(entity.getNote());
		locationDTO.setDistrict(entity.getDistrict());
		return locationDTO;
	}
}
