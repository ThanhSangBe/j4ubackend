package com.pts.j4u.mapper.impl;

import com.pts.j4u.dto.ProvinceDTO;
import com.pts.j4u.entity.Province;
import com.pts.j4u.mapper.ProvinceMapper;
import org.springframework.stereotype.Component;

@Component
public class ProvinceMapperImpl implements ProvinceMapper {
    @Override
    public Province mapToProvince(ProvinceDTO provinceDTO) {
        Province province = new Province();
        province.setId(provinceDTO.getId());
        province.setName(provinceDTO.getName());
        province.setCode(provinceDTO.getCode());
        return province;
    }

    @Override
    public ProvinceDTO mapProvinceDTO(Province province) {
        ProvinceDTO provinceDTO = new ProvinceDTO();
        provinceDTO.setId(province.getId());
        provinceDTO.setName(province.getName());
        provinceDTO.setCode(province.getCode());
        return provinceDTO;
    }
}
