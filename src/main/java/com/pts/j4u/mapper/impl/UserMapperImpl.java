package com.pts.j4u.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import com.pts.j4u.dto.request.UserRequestUpdateDTO;
import com.pts.j4u.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pts.j4u.dto.UserDTO;
import com.pts.j4u.dto.UserRegisterDTO;
import com.pts.j4u.entity.Role;
import com.pts.j4u.entity.User;
import com.pts.j4u.mapper.UserMapper;
import com.pts.j4u.repository.RoleRepository;
@Component
public class UserMapperImpl implements UserMapper {
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserService userService;
	@Override
	public UserDTO mapToDTO(User user)
	{
		if(user == null) 
			return null;
		UserDTO dto = new UserDTO();
		dto.setId(user.getId());
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		dto.setEmail(user.getEmail());
		dto.setGender(user.isGender());
		dto.setAvatar(user.getAvatar());
		dto.setPhone(user.getPhone());
		dto.setUsername(user.getUsername());
		dto.setStatus(user.getStatus());
		List<Role> roles = roleRepository.getByUserId(user.getId());
		dto.setRole(roles);
		return dto;
	}

	@Override
	public List<UserDTO> mapToDTO(List<User> users)
	{
		List<UserDTO> userDTOS = new ArrayList<>();
		if(users != null && users.size() >0)
		{
			users.forEach(user-> userDTOS.add(mapToDTO(user)));
		}
		return userDTOS;
	}
	@Override
	public User mapToEntity(UserDTO user)
	{
		if(user == null)
			return null;
		User dto = new User();
		dto.setId(user.getId());
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		dto.setEmail(user.getEmail());
		dto.setGender(user.isGender());
		dto.setAvatar(user.getAvatar());
		dto.setPhone(user.getLastName());
		dto.setUsername(user.getUsername());
		dto.setStatus(user.getStatus());
		return dto;
	}
	@Override
	public User mapToEntity(UserRegisterDTO userRegisterDTO)
	{
		User user = new User();
		user.setId(userRegisterDTO.getId());
		user.setUsername(userRegisterDTO.getUsername());
		user.setEmail(userRegisterDTO.getEmail());
		user.setAvatar(userRegisterDTO.getAvatar());
		user.setFirstName(userRegisterDTO.getFirstName());
		user.setLastName(userRegisterDTO.getLastName());
		user.setPassword(userRegisterDTO.getPassword());
		user.setGender(userRegisterDTO.isGender());
		return user;
	}
	@Override
	public void mapToUpdate(User user, UserRegisterDTO userRegisterDTO)
	{
		user.setFirstName(userRegisterDTO.getFirstName());
		user.setLastName(userRegisterDTO.getLastName());
		user.setEmail(userRegisterDTO.getEmail());
		user.setPhone(userRegisterDTO.getPhone());
		user.setGender(userRegisterDTO.isGender());
		userService.uploadAvatar(userRegisterDTO,user);
	}
}
