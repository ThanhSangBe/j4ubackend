package com.pts.j4u.mapper.impl;

import com.pts.j4u.dto.*;
import com.pts.j4u.entity.CompanyLocation;
import com.pts.j4u.entity.Location;
import com.pts.j4u.mapper.LocationMapper;
import com.pts.j4u.mapper.UserMapper;
import com.pts.j4u.service.CompanyLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pts.j4u.entity.Company;
import com.pts.j4u.mapper.CompanyMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CompanyMapperImpl implements CompanyMapper {
	@Autowired
	private CompanyLocationService companyLocationService;
	@Autowired
	private LocationMapper locationMapper;
	@Autowired
	private UserMapper userMapper;

	@Override
	public void mapUpdate(Company company, CompanyDTO dto) {
		company.setName(dto.getName());
		company.setDescription(dto.getDescription());
		company.setEmail(dto.getEmail());
		company.setLogo(dto.getLogo());
		company.setPhone(dto.getPhone());
		company.setTax(dto.getTax());
		company.setWebsite(dto.getTax());
		company.setCode(dto.getCode());
	}

	@Override
	public CompanyDTO mapToDTO(Company company) {
		CompanyDTO dto = new CompanyDTO();
		dto.setId(company.getId());
		dto.setName(company.getName());
		dto.setEmail(company.getEmail());
		dto.setPhone(company.getPhone());
		dto.setTax(company.getTax());
		dto.setWebsite(company.getWebsite());
		dto.setDescription(company.getDescription());
		dto.setLogo(company.getLogo());
		dto.setCode(company.getCode());
		dto.setLocations(setLocation(company.getId()));
		dto.setUsers(userMapper.mapToDTO(company.getUsers()));
		StatusDTO statusDto = new StatusDTO();
		statusDto.setId(company.getStatus().getId());
		statusDto.setName(company.getStatus().getName());
		dto.setStatus(statusDto);
		return dto;
	}

	@Override
	public CompanyDTO mapFromCreateToDTO(CompanyCreateDTO dto) {
		CompanyDTO companyDTO = new CompanyDTO();
		companyDTO.setCode(dto.getCode());
		companyDTO.setDescription(dto.getDescription());
		companyDTO.setEmail(dto.getEmail());
		companyDTO.setId(dto.getId());
		companyDTO.setName(dto.getName());
		companyDTO.setPhone(dto.getPhone());
		companyDTO.setTax(dto.getTax());
		companyDTO.setWebsite(dto.getWebsite());
		companyDTO.setLogo(dto.getLogo());
		return companyDTO;
	}

	@Override
	public Company mapToEntity(CompanyDTO company) {
		Company dto = new Company();
		dto.setId(company.getId());
		dto.setName(company.getName());
		dto.setEmail(company.getEmail());
		dto.setPhone(company.getPhone());
		dto.setTax(company.getTax());
		dto.setWebsite(company.getWebsite());
		dto.setDescription(company.getDescription());
		dto.setLogo(company.getLogo());
		dto.setCode(company.getCode());
		return dto;
	}

	@Override
	public CompanyDTO mapToDTO(CompanyCreateDTO company) {
		CompanyDTO dto = new CompanyDTO();
		dto.setId(company.getId());
		dto.setName(company.getName());
		dto.setEmail(company.getEmail());
		dto.setPhone(company.getPhone());
		dto.setTax(company.getTax());
		dto.setWebsite(company.getWebsite());
		dto.setDescription(company.getDescription());
		dto.setLogo(company.getLogo());
		dto.setCode(company.getCode());
		dto.setLocations(company.getLocations());
		return dto;
	}

	@Override
	public CompanySimpleDTO mapToSimple(Company company) {
		CompanySimpleDTO companySimpleDTO = new CompanySimpleDTO();
		companySimpleDTO.setCode(company.getCode());
		companySimpleDTO.setEmail(company.getEmail());
		companySimpleDTO.setId(company.getId());
		companySimpleDTO.setName(company.getName());
		companySimpleDTO.setLogo(company.getLogo());
		companySimpleDTO.setWebsite(company.getWebsite());
		return companySimpleDTO;
	}

	List<LocationDTO> setLocation(Long companyId) {
		List<LocationDTO> locationDTOS = new ArrayList<>();
		List<CompanyLocation> companyLocations = this.companyLocationService.getByCompanyId(companyId);
		if (companyLocations != null && companyLocations.size() > 0) {
			List<Location> locations = new ArrayList<>();
			for (CompanyLocation item : companyLocations) {
				locations.add(item.getLocation());
			}
			return locations.stream().map(location -> this.locationMapper.mapToDTO(location))
					.collect(Collectors.toList());
		}
		return null;
	}

}
