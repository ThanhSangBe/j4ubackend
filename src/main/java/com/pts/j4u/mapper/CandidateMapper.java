package com.pts.j4u.mapper;

import com.pts.j4u.dto.CandidateDTO;
import com.pts.j4u.dto.CandidateRegisterDTO;
import com.pts.j4u.entity.Candidate;

public interface CandidateMapper {
    Candidate mapToEntity(CandidateRegisterDTO candidateRegisterDTO);

    CandidateDTO mapToDTO(Candidate entity);
}
