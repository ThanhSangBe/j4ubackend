package com.pts.j4u.mapper;

import com.pts.j4u.dto.ApplyJobDTO;
import com.pts.j4u.entity.ApplyJob;

public interface ApplyJobMapper {

	ApplyJobDTO mapToDTO(ApplyJob entity);

}
