package com.pts.j4u.mapper;

import com.pts.j4u.dto.UniversityCreateDTO;
import com.pts.j4u.dto.UniversityDTO;
import com.pts.j4u.entity.University;

public interface UniversityMapper {

	University mapToEntity(UniversityDTO dto);

    UniversityDTO mapToDTO(University entity);

    UniversityDTO mapToDTO(UniversityCreateDTO entity);

    void mapToUpdate(UniversityDTO dto, University entity);
}
