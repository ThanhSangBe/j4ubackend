package com.pts.j4u.mapper;

import com.pts.j4u.dto.ProvinceDTO;
import com.pts.j4u.entity.Province;

public interface ProvinceMapper {
    Province mapToProvince(ProvinceDTO provinceDTO);
    ProvinceDTO mapProvinceDTO(Province province);
}
