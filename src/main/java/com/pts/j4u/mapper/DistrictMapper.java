package com.pts.j4u.mapper;

import com.pts.j4u.dto.DistrictDTO;
import com.pts.j4u.entity.District;

public interface DistrictMapper {
    District mapToDistrict(DistrictDTO districtDTO);
    DistrictDTO mapDistrictDTO(District district);
}
