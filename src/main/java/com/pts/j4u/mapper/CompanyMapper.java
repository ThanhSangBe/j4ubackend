package com.pts.j4u.mapper;

import com.pts.j4u.dto.CompanyCreateDTO;
import com.pts.j4u.dto.CompanyDTO;
import com.pts.j4u.dto.CompanySimpleDTO;
import com.pts.j4u.entity.Company;
//@Mapper(componentModel = "spring")
public interface CompanyMapper {

	void mapUpdate(Company entity, CompanyDTO dto);

	CompanyDTO mapToDTO(Company company);

	Company mapToEntity(CompanyDTO company);

    CompanyDTO mapToDTO(CompanyCreateDTO company);

    CompanySimpleDTO mapToSimple(Company company);

	CompanyDTO mapFromCreateToDTO(CompanyCreateDTO dto);
}
