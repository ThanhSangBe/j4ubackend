package com.pts.j4u.common;

public class StatusCommon {
    public static final int STATUS_ACTIVE_ID = 1;
    public static final int STATUS_DISABLE_ID = 2;
    public static final int STATUS_DELETED_ID = 3;
    public static final int STATUS_PENDING_ID = 4;
    public static final String STATUS_ACTIVE_NAME = "active";
    public static final String STATUS_DISABLE_NAME = "disable";
    public static final String STATUS_DELETED_NAME = "deleted";
    public static final String STATUS_PENDING_NAME = "pending";
}
