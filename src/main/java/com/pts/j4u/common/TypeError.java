package com.pts.j4u.common;

public enum TypeError {
	NotFound,InternalServerError,BadRequest, NoContent
}
