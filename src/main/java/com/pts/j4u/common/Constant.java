package com.pts.j4u.common;

public class Constant {
    public static final Integer PAGE_NUMBER_DEFAULT = 0;
    public static final Integer PAGE_SIZE_DEFAULT = 10;
    public static final long HALF_DAY = 12 * 60 * 60 * 1000;
    public static final Long TIME_EXPIRES = System.currentTimeMillis() + 30 * 60 * 1000;//30p
    public static String BASE_URL;
    public static String ORIGIN_REQUEST_URL;
    public static final int DAY_EXPIRE_CHANGE_STATUS_PENDING_JOB = 7;
    public static final long ONE_SECOND = 1000;
    public static final long ONE_MINUTE = 60 * ONE_SECOND;
    public static final long ONE_HOUR = 60 * ONE_MINUTE;
    public static final long ONE_DAY = 24 * ONE_HOUR;

    public void setBaseUrl(String url) {
        BASE_URL = url;
    }

    public static void setOriginRequestUrl(String originRequestUrl) {
        ORIGIN_REQUEST_URL = originRequestUrl;
    }

    public static String getOriginRequestUrl() {
        return ORIGIN_REQUEST_URL;
    }
}
