package com.pts.j4u.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pts.j4u.service.CompanyService;

import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("api/company/v1")
@Tag(name = "Company v1")
public class CompanyV1Controller {
	@Autowired
	private CompanyService companyService;

	@PutMapping("active")
	public ResponseEntity<?> activeList(@RequestParam(value = "id", required = false) List<Long> listId) {
		this.companyService.activeListId(listId);
		return ResponseEntity.ok("Active successfully");
	}

	@PutMapping("disable")
	public ResponseEntity<?> disableList(@RequestParam(value = "id", required = false) List<Long> listId) {
		this.companyService.disableListId(listId);
		return ResponseEntity.ok("Disable successfully");
	}

	@DeleteMapping("delete")
	public ResponseEntity<?> deleteList(@RequestParam(value = "id", required = false) List<Long> listId) {
		this.companyService.deleteListId(listId);
		return ResponseEntity.ok("Delete successfully");
	}
}
