package com.pts.j4u.controller;

import com.pts.j4u.entity.Major;
import com.pts.j4u.service.MajorService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/majors")
@Tag(name = "Major")
public class MajorController {
    @Autowired
    private MajorService majorService;
    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody Major major)
    {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.majorService.save(major));
    }
    @GetMapping("")
    public ResponseEntity<?> findAll()
    {
        return ResponseEntity.ok(this.majorService.findAll());
    }
}
