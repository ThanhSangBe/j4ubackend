package com.pts.j4u.controller;

import com.pts.j4u.service.LikeJobService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/like")
@Tag(name = "Like job")
public class LikeJobController extends BaseController {
    @Autowired
    private LikeJobService likeJobService;

    @PostMapping("{candidateId}/{jobId}")
    public ResponseEntity<?> create(@PathVariable(required = false) long candidateId,
                                    @PathVariable(required = false) long jobId) {
        return setCreateReponse(likeJobService.save(jobId, candidateId));
    }

    @DeleteMapping("{candidateId}/{jobId}")
    public ResponseEntity<?> delete(@PathVariable(required = false) long candidateId,
                                    @PathVariable(required = false) long jobId) {
        likeJobService.delete(jobId, candidateId);
        return setResponse("Delete success");
    }

    @GetMapping("{candidateId}")
    public ResponseEntity<?> getJobLikedByCandidateId(@PathVariable(required = false) long candidateId,
                                                      @RequestParam(required = false, defaultValue = "0") Integer pageNumber,
                                                      @RequestParam(required = false, defaultValue = "10") Integer pageSize) {
        return setResponse(this.likeJobService.getJobdLikedByCandidateId(candidateId, pageNumber, pageSize));
    }

}
