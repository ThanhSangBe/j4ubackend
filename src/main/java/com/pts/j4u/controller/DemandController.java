package com.pts.j4u.controller;

import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.DemandDTO;
import com.pts.j4u.service.DemandService;

import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("api/demand")
@Tag(name = "Demand")
public class DemandController {
	@Autowired
	private DemandService demandService;

	@PostMapping("")
	public ResponseEntity<?> create(@RequestBody DemandDTO demandDTO) {
		return ResponseEntity.status(HttpStatus.CREATED).body(this.demandService.save(demandDTO));
	}

	@PutMapping("{id}")
	public ResponseEntity<?> update(@RequestBody DemandDTO demandDTO) {
		return null;
	}

	@PutMapping("active")
	public ResponseEntity<?> activeListId(@RequestParam("id") List<Long> listId) {
		this.demandService.changeStatusListId(listId, StatusEnum.Active);
		return ResponseEntity.ok("Active successfully");
	}

	@PutMapping("disable")
	public ResponseEntity<?> disableListId(@RequestParam("id") List<Long> listId) {
		this.demandService.changeStatusListId(listId, StatusEnum.Disable);
		return ResponseEntity.ok("Disable successfully");
	}
	@Operation(summary = "Delete by list ID")
	@PutMapping("delete")
	public ResponseEntity<?> deleteListId(@RequestParam("id") List<Long> listId) {
		this.demandService.changeStatusListId(listId, StatusEnum.Deleted);
		return ResponseEntity.ok("Delete successfully");
	}
	@Operation(summary = "Filter ")
	@GetMapping("")
	public ResponseEntity<?> filter() {
		return ResponseEntity.ok(this.demandService.filter());
	}
	@Operation(summary = "Get detail by id")
	@GetMapping("{id}")
	public ResponseEntity<?> findById(@PathVariable Long id){
		return ResponseEntity.ok(this.demandService.findById(id));
	}
}
