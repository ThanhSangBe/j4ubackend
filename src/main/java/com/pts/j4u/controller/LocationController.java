package com.pts.j4u.controller;

import com.pts.j4u.dto.result.ResultObject;
import com.pts.j4u.service.LocationService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/location")
@Tag(name = "Location")
public class LocationController {
    @Autowired
    LocationService locationService;
    @GetMapping("provinces")
    public ResponseEntity<?> getProvinces()
    {
        return ResponseEntity.ok(new ResultObject(HttpStatus.OK.value(), "Get provinces success",locationService.getProvinces()));
    }

    @GetMapping("provinces/{id}/districts")
    public ResponseEntity<?> getDistricts(@PathVariable long id)
    {
        return ResponseEntity.ok(new ResultObject(HttpStatus.OK.value(),"Get districts success",locationService.getDistrictsByProvincesId(id)));
    }

}
