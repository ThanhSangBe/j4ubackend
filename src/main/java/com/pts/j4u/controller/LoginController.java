package com.pts.j4u.controller;

import com.pts.j4u.conf.CustomAuthenticationFilter;
import com.pts.j4u.dto.GoogleAccountDTO;
import com.pts.j4u.service.UserService;
import com.pts.j4u.util.social.GoogleUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Map;

@RestController
@RequestMapping("")
@Tag(name = "Auth")
public class LoginController extends BaseController {
    @Autowired
    GoogleUtil googleUtil;
    @Autowired
    CustomAuthenticationFilter customAuthenticationFilter;
    @Autowired
    private UserService userService;
    @Value("${base.url.fe}")
    private String baseURL;

    @Operation(summary = "Auth with google")
    @GetMapping("login")
    public ResponseEntity<?> loginGoogle(HttpServletRequest request, ModelMap model) {
        try {
            String code = request.getParameter("code");
            String accessToken = googleUtil.getToken(code);
            GoogleAccountDTO googleAccountDTO = googleUtil.getUserInfo(accessToken);
            Map maps = userService.handleUserGoogle(googleAccountDTO);
            return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(baseURL +
                    "/auth/success/accessToken=" + maps.get("accessToken") +
                    "&&userId=" + maps.get("id"))).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(baseURL + "/auth/failed")).build();
    }

    @Operation(summary = "Login")
    @PostMapping(value = "api/login", produces = {"application/json"})
    public ResponseEntity<?> login(@RequestParam(required = false) String username, @RequestParam(required = false) String password) {
        return ResponseEntity.ok(customAuthenticationFilter.authentication(username, password));
    }

}
