package com.pts.j4u.controller;

import com.pts.j4u.common.TypeError;
import com.pts.j4u.exception.ErrorMessageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class FileController {
    @Autowired
    ServletContext application;

    @GetMapping("/upload/{fileName:.+}")
    public ResponseEntity<?> readFile(@PathVariable String fileName, HttpServletRequest httpServletRequest) {
        Resource resource = getResource(fileName);
        if (resource != null && resource.exists()) {
            String contentType = setContenType(resource, httpServletRequest);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
            // inline: display file browser
        }
        throw new ErrorMessageException("File not found",TypeError.NotFound);
    }
    @GetMapping("download/{fileName:.+}")
    public ResponseEntity<?> downloadFile(@PathVariable String fileName, HttpServletRequest httpServletRequest){
        Resource resource = getResource(fileName);
        if (resource != null && resource.exists()) {
            String contentType = setContenType(resource, httpServletRequest);
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
            // attachment: download file
        }
        throw new ErrorMessageException("File not found",TypeError.NotFound);
    }


    private Resource getResource(String fileName) {
        Path pathFile = Paths.get(application.getRealPath("/") + "/upload/" + fileName);
        Resource resource = null;
        try {
            resource = new UrlResource(pathFile.toUri());
        } catch (MalformedURLException e) {
            throw new ErrorMessageException("Error to get resource!", TypeError.BadRequest);
        }
        return resource;
    }

    private String setContenType(Resource resource, HttpServletRequest servletRequest) {
        String contentType = null;
        try {
            contentType = servletRequest.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            return contentType;
        } catch (IOException e) {
            throw new ErrorMessageException("Error to set content Type", TypeError.BadRequest);
        }
    }
}
