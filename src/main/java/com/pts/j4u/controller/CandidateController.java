package com.pts.j4u.controller;

import com.pts.j4u.dto.CandidateDTO;
import com.pts.j4u.dto.CandidateRegisterDTO;
import com.pts.j4u.service.CandidateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/candidates")
@Tag(name = "Candidate")
public class CandidateController extends BaseController {
    @Autowired
    private CandidateService candidateService;

    @Operation(summary = "Register candidate")
    @PostMapping(value = "", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseStatus(code = HttpStatus.CREATED)
    public CandidateDTO register(@RequestPart(name = "candidate", required = true) String candidate,
                                 @RequestPart(name = "cv", required = false) MultipartFile fileCV,
                                 @RequestPart(name = "avatar", required = false) MultipartFile fileAvatar) {
        CandidateRegisterDTO candidateRegisterDTO = this.candidateService.readJson(candidate, fileCV, fileAvatar);
        CandidateDTO candidateDTOSaved = this.candidateService.create(candidateRegisterDTO);
        return candidateDTOSaved;
    }

    @PutMapping(value = "{id}", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> update(@RequestParam(name = "candidate") String candidate,
                                    @RequestPart(name = "cv", required = false) MultipartFile fileCV,
                                    @RequestPart(name = "avatar", required = false) MultipartFile fileAvatar, @PathVariable long id) {
        CandidateRegisterDTO candidateRegisterDTO = this.candidateService.readJson(candidate, fileCV, fileAvatar);
        CandidateDTO candidateUpdated = this.candidateService.update(candidateRegisterDTO, id);
        return ResponseEntity.ok(candidateUpdated);
    }

    @Operation(summary = "Upload cv by id Candidate")
    @PutMapping(value = "upload/cv", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> updateCV(@RequestPart(name = "file") MultipartFile fileCV,
                                      @RequestPart(name = "candidateId") String candidateId) {
        candidateService.uploadFileCv(Long.valueOf(candidateId), fileCV);
        return ResponseEntity.ok("Upload cv success");
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(this.candidateService.findById(id));
    }

    @GetMapping("{id}/jobs")
    public ResponseEntity<?> getJobsByIdCandidate(@PathVariable Long id) {
        return ResponseEntity.ok(this.candidateService.getListJobByIdCandidate(id));
    }

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestParam(name = "no", required = false, defaultValue = "0") Integer no,
                                    @RequestParam(name = "limit", required = false, defaultValue = "10") Integer limit,
                                    @RequestParam(name = "firstName", required = false) String firstName,
                                    @RequestParam(name = "lastName", required = false) String lastName
    ) {
        return setResponse(candidateService.search(firstName, lastName, no, limit));
    }

    @Operation(summary = "Get candidate by user id")
    @GetMapping("user")
    public ResponseEntity<?> getByUserId(@RequestParam(required = true) long userId) {
        return setResponse(candidateService.getCandidateByUserId(userId));
    }

    @Operation(summary = "Delete by candidate id")
    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long candidateId) {
        candidateService.deleteById(candidateId);
        return setResponse("Delete candidate success");

    }

    @Operation(summary = "Update status candidate by id")
    @PutMapping("status/{candidateId}/{statusId}")
    public ResponseEntity<?> updateStatusById(@PathVariable Long candidateId, @PathVariable Integer statusId) {
        return setResponse(candidateService.updateRole(candidateId, statusId));
    }
}
