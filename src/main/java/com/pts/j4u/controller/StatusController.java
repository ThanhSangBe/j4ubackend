package com.pts.j4u.controller;

import com.pts.j4u.mapper.StatusMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pts.j4u.entity.Status;
import com.pts.j4u.service.StatusService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("api/status")
@Tag(name = "Status")
public class StatusController {
	@Autowired
	private StatusService statusService;

	@Operation(summary = "Find All Status")
	@GetMapping("")
	public ResponseEntity<?> findAll() {
		return ResponseEntity.ok(this.statusService.findAll());
	}

	@Operation(summary = "Find status by ID")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = Status.class))),
			@ApiResponse(responseCode = "404", content = @Content(schema = @Schema(example = "ID Status không tìm thấy."))) })
	@GetMapping("{id}")
	public ResponseEntity<?> findById(@PathVariable int id) {
		return ResponseEntity.ok(this.statusService.findById(id));
	}

	@Operation(summary = "Create Status")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(example = "Tên status thêm thành công."))) })
	@PostMapping("")
	public ResponseEntity<?> save(@RequestBody Status status) {
		Status saved = this.statusService.save(status);
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(String.format("Status %s saved successfully", saved.getName()));
	}

	@Operation(summary = "Update Status")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(example = "Tên status cập nhật thành công."))),
			@ApiResponse(responseCode = "404", content = @Content(schema = @Schema(example = "Id Status không tìm thấy."))) })
	@PutMapping("{id}")
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Status status) {
		Status updated = this.statusService.update(id, status);
		return ResponseEntity.ok(String.format("Status %s updated successfully", updated.getName()));
	}
}
