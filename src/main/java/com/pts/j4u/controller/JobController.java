package com.pts.j4u.controller;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.JobCreateDTO;
import com.pts.j4u.service.JobService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/job")
@Tag(name = "Job")
public class JobController extends BaseController {
    @Autowired
    private JobService jobService;

    @Operation(summary = "Create new job")
    @PostMapping("")
    public ResponseEntity<?> create(@RequestBody JobCreateDTO dto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(this.jobService.create(dto));
    }

    @Operation(summary = "Get all")
    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestParam Map<String, String> params) {
        String no = params.getOrDefault("no", "0");
        String limit = params.getOrDefault("limit", "5");
        String status = params.getOrDefault("status", "");
        return ResponseEntity.ok(this.jobService.findAll(Integer.valueOf(no), Integer.valueOf(limit), status));
    }

    @Operation(summary = "Update by id")
    @PutMapping("{id}")
    public ResponseEntity<?> update(@RequestBody JobCreateDTO dto, @PathVariable Long id) {
        return ResponseEntity.ok(this.jobService.update(dto, id));
    }

    @Operation(summary = "Delete by id")
    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteById(@PathVariable long id) {
        this.jobService.deleteById(id);
        return ResponseEntity.ok("Deleted successfully");
    }

    @Operation(summary = "Get by id")
    @GetMapping("{id}")
    public ResponseEntity<?> getDetail(@PathVariable long id) {
        return ResponseEntity.ok(this.jobService.getDetailById(id));
    }

    @Operation(summary = "Active list id")
    @PutMapping("active")
    public ResponseEntity<?> activeListId(@RequestParam(value = "id", required = false) List<Long> listId) {
        this.jobService.changeStatusListId(listId, StatusEnum.Active);
        return ResponseEntity.ok("Active Successfull");
    }

    @Operation(summary = "Disable list Id")
    @PutMapping("disable")
    public ResponseEntity<?> disableListId(@RequestParam(value = "id", required = false) List<Long> listId) {
        this.jobService.changeStatusListId(listId, StatusEnum.Disable);
        return ResponseEntity.ok("Disable Successfull");
    }

    @Operation(summary = "Search")
    @GetMapping("q")
    public ResponseEntity<?> nameLike(@RequestParam Map<String, String> params) {
        return setResponse(jobService.filter(params));
    }

    @Operation(summary = "Search")
    @GetMapping("query")
    public ResponseEntity<?> search(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String nameCompany,
            @RequestParam(required = false) Integer type,
            @RequestParam(required = false) Long province,
            @RequestParam(required = false) Long district,
            @RequestParam(value = "no", defaultValue = "0",required = false) Integer no,
            @RequestParam(value = "limit", defaultValue = "10",required = false) Integer limit
    ) {
        return setResponse(jobService.filterVersion1(name, nameCompany, type, province, district, no, limit));
    }
}
