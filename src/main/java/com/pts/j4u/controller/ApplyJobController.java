package com.pts.j4u.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pts.j4u.dto.ApplyJobDTO;
import com.pts.j4u.service.ApplyJobService;

import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("api/apply-job")
@Tag(name = "Apply Job")
public class ApplyJobController {
	@Autowired
	private ApplyJobService applyJobService;

	@PostMapping("")
	public ResponseEntity<?> create(@RequestBody ApplyJobDTO applyJobDTO) {
		return ResponseEntity.ok(this.applyJobService.save(applyJobDTO));
	}
}
