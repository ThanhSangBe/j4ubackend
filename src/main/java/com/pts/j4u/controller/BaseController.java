package com.pts.j4u.controller;

import com.pts.j4u.dto.result.ResultObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BaseController {
    public ResponseEntity<ResultObject> setResponse(Object data) {
        return ResponseEntity.ok(new ResultObject(HttpStatus.OK.value(), "success", data));
    }

    public ResponseEntity<ResultObject> setCreateReponse(Object data) {
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResultObject(HttpStatus.CREATED.value(), "Create item success", data));
    }
}
