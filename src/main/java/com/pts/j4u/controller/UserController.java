package com.pts.j4u.controller;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.ChangePasswordDTO;
import com.pts.j4u.dto.UserDTO;
import com.pts.j4u.dto.UserRegisterDTO;
import com.pts.j4u.dto.request.VerifyCodePasswordDTO;
import com.pts.j4u.dto.result.ResultObject;
import com.pts.j4u.entity.User;
import com.pts.j4u.repository.UserRepository;
import com.pts.j4u.service.StatusService;
import com.pts.j4u.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/user")
@Tag(name = "User")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Value("${base.url.fe}")
    private String baseURL;
    @Autowired
    private StatusService statusService;
    @Operation(summary = "Find by id")
    @GetMapping("{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return ResponseEntity.ok(userService.findById(id));
    }
    @Operation(summary = "Get status by username")
    @GetMapping("username")
    public ResponseEntity<?> getStatusByUsername(@RequestParam String username) {
        return ResponseEntity.ok(userService.findByUsername(username).getStatus());
    }


    @PostMapping(value = "", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    @ResponseStatus(code = HttpStatus.CREATED)
    public UserDTO register(@RequestPart(name = "user", required = false) String user,
                            @RequestPart(name = "file", required = false) MultipartFile file) {
        UserRegisterDTO userRegisterDTO = this.userService.readJson(user, file);
        UserDTO userDTO = this.userService.register(userRegisterDTO);
        return userDTO;
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<ResultObject> updateUser(@RequestBody UserRegisterDTO user, @PathVariable Long id) {
        return setResponse(this.userService.update(id, user));

    }

    @PostMapping("change-password/{id}")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDTO dto, @PathVariable long id) {
        return ResponseEntity.ok(this.userService.changePassword(dto, id));
    }

    @GetMapping("forgot-password")
    public ResponseEntity<?> forgotPassword(HttpServletRequest httpServletRequest, @RequestParam(name = "email", required = false) String email) {
        return setResponse(this.userService.fortgotPassword(email));
    }

    @GetMapping("verify-code")
    public ResponseEntity<?> verifyEmailAndVerifyCode(
            @RequestParam Map<String, String> params) {
        String email = params.get("email");
        String verifyCode = params.get("verifyCode");
        boolean isverifySuccess = userService.verifyCodeAndEmail(email, verifyCode);
        if (isverifySuccess) {
            String uriResponse = String.format("/reset-password/email=%s&verifyCode=%s", email, verifyCode);
            return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(baseURL + uriResponse)).build();
        } else {
            String uriResponse = String.format("/failed-reset-password");
            return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(baseURL + uriResponse)).build();
        }

    }

    @GetMapping("verify-email")
    public ResponseEntity<?> verifyEmail(
            @RequestParam String email,
            @RequestParam String verifyCode
    ) {
        boolean isVerifySuccess = userService.verifyCodeAndEmail(email, verifyCode);
        if (isVerifySuccess) {
            User user = this.userRepository.findByEmail(email).get();
            user.setVerifyEmail(true);
            user.setStatus(statusService.findByEnum(StatusEnum.Active));
            userRepository.save(user);
            return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(baseURL)).build();
        }
        return setResponse("Verify failed");
    }

    @PostMapping("reset-password")
    public ResponseEntity<?> resetPassword(@RequestParam(name = "email", required = true) String email,
                                           @RequestParam(name = "verifyCode", required = true) String verifyCode,
                                           @RequestBody VerifyCodePasswordDTO verifyCodePassword) {
        verifyCodePassword.setEmail(email);
        verifyCodePassword.setVerifyCode(verifyCode);
        return setResponse(userService.resetPassword(verifyCodePassword));
    }

    @Operation(summary = "Update role id")
    @PutMapping("{userId}/roles")
    public ResponseEntity<?> updateRole(@RequestParam List<Integer> roleIds, @PathVariable long userId) {
        this.userService.updateListRoleId(roleIds, userId);
        return setResponse("Update role success");

    }

    @Operation(summary = "Get all")
    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestParam(required = false, defaultValue = "0") int pageNumber,
                                    @RequestParam(required = false, defaultValue = "10") int pageSize
    ) {
        return setResponse(this.userService.findAll(pageNumber, pageSize));
    }
}
