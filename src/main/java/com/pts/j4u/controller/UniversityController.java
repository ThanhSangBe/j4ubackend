package com.pts.j4u.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.PaginationDTO;
import com.pts.j4u.dto.UniversityCreateDTO;
import com.pts.j4u.dto.UniversityDTO;
import com.pts.j4u.dto.UserRegisterDTO;
import com.pts.j4u.dto.result.ResultObject;
import com.pts.j4u.entity.University;
import com.pts.j4u.repository.UniversityRepository;
import com.pts.j4u.service.DemandService;
import com.pts.j4u.service.UniversityService;
import com.pts.j4u.service.UserService;
import com.pts.j4u.util.HandleFile;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/university")
@Tag(name = "University")
public class UniversityController extends BaseController {
    @Autowired
    private UniversityService universityService;
    @Autowired
    private DemandService demandService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private UserService userService;
    @Autowired
    private HandleFile handleFile;
    @Autowired
    private UniversityRepository universityRepository;

    private String messageResponse = "";

    @Operation(summary = "Get all")
    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam Map<String, String> params) {
        String no = params.getOrDefault("no", "0");
        String limit = params.getOrDefault("limit", "5");
        PaginationDTO paginationDTO = this.universityService.findAll(Integer.valueOf(no), Integer.valueOf(limit));
        return ResponseEntity.ok(paginationDTO);
    }

    @Operation(summary = "Register university")
    @PostMapping(value = "register", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.ALL_VALUE})
    public ResponseEntity<?> register(@RequestPart(name = "university", required = false) String university,
                                      @RequestPart(name = "user", required = false) String user) {
        UniversityCreateDTO universityCreateDTO = this.universityService.readJson(university);
        UserRegisterDTO userRegisterDTO = this.userService.readJson(user);
        return setCreateReponse(this.universityService.register(universityCreateDTO, userRegisterDTO));
    }

    @Operation(summary = "Get by id")
    @GetMapping("{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        UniversityDTO dto = this.universityService.findById(id);
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Create new University by admin")
    @PostMapping(value = "", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.ALL_VALUE})
    public ResponseEntity<?> create(@RequestPart(name = "university", required = false) String university,
                                    @RequestPart(name = "file", required = false) MultipartFile file) {
        UniversityCreateDTO universityCreateDTO = universityService.readJson(university, file);
        UniversityDTO universityDTOSaved = this.universityService.save(universityCreateDTO);
        messageResponse = String.format(messageSource.getMessage("SaveSuccessfully", null, null),
                universityDTOSaved.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResultObject(HttpStatus.CREATED.value(), messageResponse, universityDTOSaved));
    }

    @Operation(summary = "Update by id")
    @PutMapping(value = "{id}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> update(@PathVariable Long id,
                                    @RequestPart(name = "university", required = false) String university,
                                    @RequestPart(name = "file", required = false) MultipartFile file) {
        ObjectMapper objectMapper = new ObjectMapper();
        UniversityDTO universityUpdated = null;
        try {
            UniversityDTO universityDTO = objectMapper.readValue(university, UniversityDTO.class);
            universityUpdated = this.universityService.update(id, universityDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (file != null) {
            //upload file
            String url = handleFile.upload(file);
            University universityEntity = this.universityService.getById(id);
            universityEntity.setAvatar(url);
            University universitySaved = this.universityRepository.save(universityEntity);
        }
        messageResponse = String.format(messageSource.getMessage("UpdateSuccessfully", null, null),
                universityUpdated.getName());
        return ResponseEntity.ok(new ResultObject(HttpStatus.OK.value(), messageResponse, universityUpdated));
    }

    @Operation(summary = "Delete by id")
    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        UniversityDTO dto = this.universityService.changeStatus(id, StatusEnum.Deleted);
        messageResponse = String.format(messageSource.getMessage("DisableSuccessfully", null, null), dto.getName());
        return ResponseEntity.ok(messageResponse);
    }

    @Operation(summary = "Active")
    @PutMapping(value = "{id}/active")
    public ResponseEntity<?> active(@PathVariable Long id) {
        UniversityDTO dto = this.universityService.changeStatus(id, StatusEnum.Active);
        messageResponse = String.format(messageSource.getMessage("ActiveSuccessfully", null, null), dto.getName());
        return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    // version 1 CRUD
    @Operation(summary = "Change status list University to Active")
    @PutMapping(value = "v1/active")
    public ResponseEntity<?> activeListId(@RequestParam(value = "id", required = false) List<Long> listId) {
        this.universityService.activeListId(listId);
        return ResponseEntity.ok("Active successfully");
    }

    @Operation(summary = "Change status list University to Disabled")
    @PutMapping(value = "v1/disable")
    public ResponseEntity<?> disableListId(@RequestParam(value = "id", required = false) List<Long> listId) {
        this.universityService.disableListId(listId);
        return ResponseEntity.ok("Disable successfully");
    }

    @Operation(summary = "Change status list University to deleted")
    @PutMapping(value = "v1/delete")
    public ResponseEntity<?> deleteListId(@RequestParam(value = "id", required = false) List<Long> listId) {
        this.universityService.deleteListId(listId);
        return ResponseEntity.ok("Delete successfully");
    }

    @Operation(summary = "Delete force list university")
    @DeleteMapping(value = "delete-force")
    public ResponseEntity<?> deleteForceListId(@RequestParam(value = "id", required = false) List<Long> listId) {
        this.universityService.deleteForce(listId);
        return ResponseEntity.ok("Delete successfully");
    }

    @Operation(summary = "Search by name, status")
    @GetMapping("search")
    public ResponseEntity<?> findByName(@RequestParam Map<String, String> params) {
        String name = params.getOrDefault("name", "");
        String status = params.getOrDefault("status", "0");
        String no = params.getOrDefault("no", "0");
        String limit = params.getOrDefault("limit", "5");
        return ResponseEntity.ok(this.universityService.searchByNameAndStatus(Integer.valueOf(no),
                Integer.valueOf(limit), name, Integer.valueOf(status)));
    }

    @Operation(summary = "Get demand by university id")
    @GetMapping("{id}/demands")
    public ResponseEntity<?> findDemandsByIdUniversity(@PathVariable long id) {
        return setResponse(demandService.findByUniversityId(id));
    }

    @Operation(summary = "Get university by user id")
    @GetMapping("user")
    public ResponseEntity<?> getByUserId(@RequestParam(required = true) long userId) {
        return setResponse(this.universityService.getByUserId(userId));
    }

}
