package com.pts.j4u.controller;

import com.pts.j4u.repository.DemandRepository;
import com.pts.j4u.repository.JobRepository;
import com.pts.j4u.repository.UserRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/statistics")
@Tag(name = "Statistics")
public class StatisticsController extends BaseController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private DemandRepository demandRepository;
    @Operation(summary = "Statistics user register for the last number month")
    @GetMapping("user")
    public ResponseEntity<?> countUserByNumberMonth(@RequestParam(defaultValue = "6",required = true) Integer month) {
        return setResponse(userRepository.statisticsByUserRegister6Month(month));
    }

    @Operation(summary = "Statistics job create date for the last number month")
    @GetMapping("job")
    public ResponseEntity<?> countJobByNumberMonth(@RequestParam(defaultValue = "6",required = true) Integer month) {
        return setResponse(jobRepository.statisticByNumberLastMonth(month));
    }

    @Operation(summary = "Statistics demand create date for the last number month")
    @GetMapping("demand")
    public ResponseEntity<?> countDemandByNumberMonth(@RequestParam(defaultValue = "6",required = true) Integer month) {
        return setResponse(demandRepository.statisticByNumberLastMonth(month));
    }
    @Operation(summary = "Statistics user verify email or not verify email")
    @GetMapping("user/verify-email")
    public ResponseEntity<?> statisticsUserVerifyEmail() {
        return setResponse(userRepository.statisticsByUserRegisterVerifyEmail());
    }
    @Operation(summary = "Statistics user by every role")
    @GetMapping("user-role")
    public ResponseEntity<?> statisticsRole(){
        return setResponse(userRepository.statisticsByRole());
    }

}
