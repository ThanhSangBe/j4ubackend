package com.pts.j4u.controller;

import com.pts.j4u.common.StatusEnum;
import com.pts.j4u.dto.CompanyCreateDTO;
import com.pts.j4u.dto.CompanyDTO;
import com.pts.j4u.dto.CompanyRegisterDTO;
import com.pts.j4u.entity.Company;
import com.pts.j4u.service.CompanyService;
import com.pts.j4u.service.JobService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/company")
@Tag(name = "Company")
public class CompanyController extends BaseController {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private JobService jobService;

    @Operation(summary = "Create new company")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update successfull", content = @Content(schema = @Schema(example = "Tên Company lưu thành công")))})
    @PostMapping(value = "", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> create(@RequestPart(name = "company", required = false) String company,
                                    @RequestPart(name = "file", required = false) MultipartFile file) {
        CompanyCreateDTO companyCreateDTO = this.companyService.readJson(company, file);
        CompanyDTO companyDTO = this.companyService.save(companyCreateDTO);
        String message = String.format(this.messageSource.getMessage("SaveSuccessfully", null, null),
                companyDTO.getName());
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON_UTF8).body(message);
    }

    @PostMapping(value = "register", consumes = {MediaType.APPLICATION_JSON_VALUE,
            MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> register(@RequestPart(name = "company", required = true) String company,
                                      @RequestPart(name = "file", required = false) MultipartFile file) {
        CompanyRegisterDTO registerDTO = this.companyService.readJsonRegister(company, file);
        CompanyDTO companyDTO = this.companyService.register(registerDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(companyDTO);
    }

    @Operation(summary = "Update Company")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update successfull", content = @Content(schema = @Schema(example = "Tên Company cập nhật thành công"))),
            @ApiResponse(responseCode = "404", description = "Company not found with id, update fail.")})
    @PutMapping("{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody CompanyDTO dto) {
        CompanyDTO updated = this.companyService.update(id, dto);
        return ResponseEntity
                .ok(String.format(messageSource.getMessage("UpdateSuccessfully", null, null), updated.getName()));
    }

    @PutMapping(value = "v1/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> updateV1(@RequestPart(name = "company", required = false) String company,
                                      @RequestPart(name = "file", required = false) MultipartFile file, @PathVariable Long id) {

        CompanyCreateDTO companyCreateDTO = this.companyService.readJson(company, file);
        CompanyDTO companyDTO = this.companyService.update(companyCreateDTO, id);
        return ResponseEntity.ok(companyDTO);
    }

    @Operation(summary = "Find all", description = "Find All Company")
    @GetMapping("")
    public ResponseEntity<?> findAll(@RequestParam(required = false, defaultValue = "0") int no,
                                     @RequestParam(required = false, defaultValue = "5") int limit) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(this.companyService.findAll(no, limit));
    }

    @Operation(summary = "Find by Id")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Find Id Successfull"),
            @ApiResponse(responseCode = "404", description = "Company not found with id")})
    @GetMapping("{id}")
    public ResponseEntity<?> findById(@PathVariable long id) {
        return ResponseEntity.ok(this.companyService.findById(id));
    }

    @Operation(summary = "Active company with ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Active Successfull", content = @Content(schema = @Schema(example = "Tên công ty đã được kích hoạt"))),
            @ApiResponse(responseCode = "404", description = "Company not found with id")})
    @PutMapping("{id}/active")
    public ResponseEntity<?> active(@PathVariable Long id) {
        Company company = this.companyService.changeStatus(id, StatusEnum.Active);
        return ResponseEntity
                .ok(String.format(messageSource.getMessage("ActiveSuccessfully", null, null), company.getName()));
    }

    @Operation(summary = "Delete Company with ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Delete Successfull", content = @Content(schema = @Schema(example = "Tên company đã được xóa."))),
            @ApiResponse(responseCode = "404", description = "Company not found with id")})
    @DeleteMapping("{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Company company = this.companyService.changeStatus(id, StatusEnum.Deleted);
        return ResponseEntity
                .ok(String.format(messageSource.getMessage("DisableSuccessfully", null, null), company.getName()));
    }

    @Operation(summary = "Filter all company with ID Status", description = "ID của Status")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Filter Company Successfull")})
    @GetMapping("/status/{id}")
    public ResponseEntity<?> findAllActive(@PathVariable int id) {
        return ResponseEntity.ok(this.companyService.findByStatus(id));
    }

    @GetMapping("{id}/jobs")
    public ResponseEntity<?> findJobs(@PathVariable long id, @RequestParam Map<String, String> params) {
        String no = params.getOrDefault("no", "0");
        String limit = params.getOrDefault("limit", "5");
        return ResponseEntity.ok(this.jobService.findByCompanyId(id, Integer.valueOf(no), Integer.valueOf(limit)));
    }

    @GetMapping("search")
    public ResponseEntity<?> findByName(@RequestParam Map<String, String> params) {
        String name = params.getOrDefault("name", "");
        String status = params.getOrDefault("status", "0");
        String no = params.getOrDefault("no", "0");
        String limit = params.getOrDefault("limit", "5");
        return ResponseEntity
                .ok(companyService.findAll(name, Integer.valueOf(status), Integer.valueOf(no), Integer.valueOf(limit)));
    }

    @DeleteMapping("delete-force")
    public ResponseEntity<?> disableList(@RequestParam(value = "id", required = false) List<Long> listId) {
        this.companyService.deleteForceByList(listId);
        return ResponseEntity.ok("Deleted force successfully");
    }
    @Operation(summary = "Get list candidate applied job by id job")
    @GetMapping("{idJob}/applied/candidates")
    public ResponseEntity<?> getCandidatesAppliedByIdJob(@PathVariable Long idJob) {
        return ResponseEntity.ok(this.companyService.getCandidatesByIdJob(idJob));
    }

    @Operation(summary = "Get company with user id")
    @GetMapping("user")
    public ResponseEntity<?> getByUserId(@RequestParam(required = true) long userId) {
        return setResponse(this.companyService.getByUserId(userId));
    }

    @Operation(summary = "Get user by Company id")
    @GetMapping("{id}/users")
    public ResponseEntity<?> getUsersById(@RequestParam(required = true) long companyId) {
        return setResponse(this.companyService.getUsersByCompanyId(companyId));
    }
}
