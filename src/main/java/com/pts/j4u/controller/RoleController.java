package com.pts.j4u.controller;

import com.pts.j4u.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.pts.j4u.entity.Role;
import com.pts.j4u.service.RoleService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("api/role")
@Tag(name = "Role")
public class RoleController extends BaseController {
	@Autowired
	private RoleService roleService;
	@Autowired
	private UserService userService;

	@Operation(summary = "Find All Role")
	@GetMapping("")
	public ResponseEntity<?> findAll() {
		return ResponseEntity.ok(this.roleService.findAll());
	}

	@Operation(summary = "Find role by ID")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = Role.class))),
			@ApiResponse(responseCode = "404", content = @Content(schema = @Schema(example = "ID Role không tìm thấy."))) })
	@GetMapping("{id}")
	public ResponseEntity<?> findById(@PathVariable int id) {
		return ResponseEntity.ok(this.roleService.findById(id));
	}

	@Operation(summary = "Create Role")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(example = "Tên role thêm thành công."))) })
	@PostMapping("")
	public ResponseEntity<?> create(@RequestBody Role role) {
		Role roleSaved = this.roleService.save(role);
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(String.format("Role %s saved successfully", roleSaved.getName()));
	}

	@Operation(summary = "Update Role")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(example = "Tên role cập nhật thành công."))),
			@ApiResponse(responseCode = "404", content = @Content(schema = @Schema(example = "Id role không tìm thấy."))) })
	@PutMapping("{id}")
	public ResponseEntity<?> update(@PathVariable int id, @RequestBody Role role) {
		Role roleUpdated = this.roleService.update(id, role);
		return ResponseEntity.ok(String.format("Role %s updated successfully", roleUpdated.getName()));
	}
	@Operation(summary = "get user by role id")
	@GetMapping("{id}/users")
	public ResponseEntity<?> findUserByRoleId(@PathVariable Integer id,
											  @RequestParam(value = "no",defaultValue = "0") Integer pageNumber,
											  @RequestParam(value = "limit",defaultValue = "10") Integer pageSize){
		return setResponse(this.userService.findUserByRoleId(id,pageNumber,pageSize));
	}

}
