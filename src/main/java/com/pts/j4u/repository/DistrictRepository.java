package com.pts.j4u.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pts.j4u.entity.District;

import java.util.List;

@Repository
public interface DistrictRepository extends JpaRepository<District, Long> {
    @Query(value = "SELECT d FROM District d JOIN FETCH d.province WHERE d.province.id=:provinceId")
    List<District> findByProvinceId(@Param("provinceId") long provinceId);
}
