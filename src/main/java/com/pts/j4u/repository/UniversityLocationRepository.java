package com.pts.j4u.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pts.j4u.entity.UniversityLocation;
@Repository
public interface UniversityLocationRepository extends JpaRepository<UniversityLocation, Long> {
	@Query("SELECT ul FROM UniversityLocation ul WHERE ul.university.id = :id")
	List<UniversityLocation> findByUniversityId(@Param("id") Long id);
	@Modifying
	@Query("DELETE FROM UniversityLocation ul WHERE ul.university.id = :id")
	void deleteByUniversityId(@Param("id")Long id);
}
