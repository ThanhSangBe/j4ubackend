package com.pts.j4u.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pts.j4u.entity.Major;
@Repository
public interface MajorRepository extends JpaRepository<Major, Long> {

}
