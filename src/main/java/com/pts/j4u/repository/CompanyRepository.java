package com.pts.j4u.repository;

import com.pts.j4u.entity.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
    @Query(value = "SELECT c FROM Company c WHERE c.status.id = :id")
    List<Company> getByStatusId(@Param("id") Integer id);


    @Query(value = "SELECT u FROM Company u WHERE u.status.id=1 or u.status.id=4")
    Page<Company> findAll(Pageable pageable);

    boolean existsByCode(String code);

    boolean existsByEmail(String email);

    boolean existsByWebsite(String website);

    @Query(value = "SELECT c FROM Company c WHERE c.name LIKE %:name%")
    Page<Company> findByName(Pageable pageable, @Param("name") String name);

    @Query(value = "SELECT c FROM Company c WHERE c.status.id=:id")
    Page<Company> findByStatusId(Pageable pageable, @Param("id") int id);

    @Query(value = "SELECT c FROM Company c WHERE c.name LIKE %:name% and c.status.id=:id")
    Page<Company> findByNameAndStatusId(Pageable pageable, @Param("name") String name, @Param("id") int id);

    @Modifying
    @Query(value = "DELETE FROM Company c WHERE c.id=:id")
    void deleteByIdCompany(@Param("id") Long id);

    @Query(value = "SELECT c " +
            "FROM User u " +
            "INNER JOIN Company c ON u.company.id = c.id " +
            "WHERE u.id=:id")
    Optional<Company> getByUserId(@Param("id") long userId);
}
