package com.pts.j4u.repository;

import com.pts.j4u.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    Optional<User> findByEmail(String email);

    @Modifying
    @Query(value = "DELETE FROM User u WHERE u.id=:id")
    void deleteByIdUser(@Param("id") Long id);

    @Modifying
    @Query(value = "DELETE FROM User u WHERE u.username=:username")
    void deleteByUsername(@Param("username") String username);

    @Query(value = "SELECT u " +
            "FROM User u " +
            "INNER JOIN Company c ON u.company.id = c.id " +
            "WHERE c.id=:id")
    List<User> getByCompanyId(@Param("id") long companyId);

    @Query(nativeQuery = true, value = "SELECT MONTH(u.create_date), COUNT(u.id) " +
            "FROM j4u_db.user u " +
            "WHERE MONTH(u.create_date) <= MONTH(current_date())  " +
            "AND MONTH(u.create_date) >= MONTH(current_date())- :numberMonth " +
            "GROUP BY MONTH(u.create_date);")
    List<Object[]> statisticsByUserRegister6Month(@Param("numberMonth") int month);

    @Query(nativeQuery = true, value = "SELECT u.verify_email ,count(u.id) " +
            "FROM j4u_db.user u " +
            "GROUP BY u.verify_email;")
    List<Object> statisticsByUserRegisterVerifyEmail();

    @Query(nativeQuery = true, value = "SELECT r.name, count(u.id) as total\n" +
            "FROM j4u_db.user u\n" +
            "INNER JOIN j4u_db.user_role ul ON u.id=ul.user_id \n" +
            "INNER JOIN j4u_db.role r ON ul.role_id =r.id \n" +
            "GROUP BY r.name;")
    List<Object[]> statisticsByRole();

    //    @Query(nativeQuery = true, value = "SELECT u.* " +
//            "FROM j4u_db.user u " +
//            "INNER JOIN j4u_db.user_role ur WHERE ur.user_id=u.id " +
//            "AND ur.role_id=:id")
//    List<User> getUsersByRoleId(@Param("id") Integer roleId);
    @Query(value = "SELECT u " +
            "FROM User u " +
            "JOIN UserRole ur ON ur.user.id=u.id " +
            "WHERE ur.role.id=:id")
    Page<User> getUsersByRoleId(@Param("id") Integer roleId, Pageable pageable);
}
