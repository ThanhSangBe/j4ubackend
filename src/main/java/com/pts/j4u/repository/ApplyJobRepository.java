package com.pts.j4u.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pts.j4u.entity.ApplyJob;
@Repository
public interface ApplyJobRepository extends JpaRepository<ApplyJob, Long> {
	@Query("SELECT aj FROM ApplyJob aj WHERE aj.job.id = :id")
	List<ApplyJob> findByJobId(@Param("id") Long idJob);
	@Query("SELECT aj FROM ApplyJob aj WHERE aj.candidate.id = :id")
	List<ApplyJob> findByIdCandidate(@Param("id") Long idCandidate);
}
