package com.pts.j4u.repository;

import com.pts.j4u.entity.Demand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemandRepository extends JpaRepository<Demand, Long> {
    @Query(value = "SELECT d " +
            "FROM Demand d " +
            "INNER JOIN University u ON d.university.id = u.id " +
            "WHERE u.id=:id")
    Page<Demand> findByUniversityId(@Param("id") Long id, Pageable pageable);
    @Query(nativeQuery = true,value = "SELECT MONTH(d.create_date),COUNT(d.id)\n" +
            "\tFROM j4u_db.demand d\n" +
            "\tWHERE MONTH(d.create_date) <= MONTH(current_date()) AND MONTH(d.create_date) >=  MONTH(current_date()) - :numberMonth\n" +
            "\tGROUP BY MONTH(d.create_date);")
    List<Object[]> statisticByNumberLastMonth(@Param("numberMonth") Integer number);
}
