package com.pts.j4u.repository.specification;

import com.pts.j4u.common.StatusCommon;
import com.pts.j4u.entity.*;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public final class JobSpecification implements Specification<Job> {
    @Override
    public Predicate toPredicate(Root<Job> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return null;
    }

    public static Specification<Job> findAll(String name, String company, Integer jobTypeId, Long provinceId, Long districtId) {
        return ((root, query, criteriaBuilder) ->
        {
            Join<Status, Job> rootStatus = root.join("status", JoinType.INNER);
            List<Predicate> predicates = new ArrayList<>();
            //add status = active
            predicates.add(criteriaBuilder.equal(rootStatus.get("id"), StatusCommon.STATUS_ACTIVE_ID));

            if (name != null && !name.equals(""))
                predicates.add(criteriaBuilder.like(root.get("name").as(String.class), "%" + name + "%"));
            //filter jobtype id
            if (jobTypeId != null && jobTypeId != 0) {
                Join<JobTypeJob, Job> rootRobTypeJobs = root.join("jobTypeJobs", JoinType.INNER);
                Join<JobType, JobTypeJob> rootJobType = rootRobTypeJobs.join("jobType", JoinType.INNER);
                predicates.add(criteriaBuilder.equal(rootJobType.get("id"), jobTypeId));
            }
            //find by province id
            if (provinceId != null || districtId != null || company != null) {
                Join<Company, Job> rootCompany = root.join("company", JoinType.INNER);
                //find name company
                if (company != null) {
                    predicates.add(criteriaBuilder.like(rootCompany.get("name"), "%" + company + "%"));
                }
                Join<CompanyLocation, Company> rootCompanyLocation = rootCompany.join("companyLocations", JoinType.INNER);
                Join<Location, CompanyLocation> rootLocation = rootCompanyLocation.join("location", JoinType.INNER);
                Join<District, Location> rootDistrict = rootLocation.join("district", JoinType.INNER);
                if (districtId != null) {
                    predicates.add(criteriaBuilder.equal(rootDistrict.get("id"), districtId));
                }
                if (provinceId != null) {
                    Join<Province, Location> rootProvince = rootDistrict.join("province", JoinType.INNER);
                    predicates.add(criteriaBuilder.equal(rootProvince.get("id"), provinceId));
                }
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        });
    }

}
