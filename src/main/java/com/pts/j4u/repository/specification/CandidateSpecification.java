package com.pts.j4u.repository.specification;

import com.pts.j4u.entity.Candidate;
import com.pts.j4u.entity.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public final class CandidateSpecification implements Specification<Candidate> {

    @Override
    public Predicate toPredicate(Root<Candidate> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return null;
    }

    public static Specification<Candidate> search(String firstName, String lastName) {
        return ((root, query, criteriaBuilder) -> {
            Join<User, Candidate> rootUser = root.join("user", JoinType.INNER);
            Predicate predicateFirstName = null;
            Predicate predicateLastName = null;
            if (firstName != null)
                predicateFirstName = criteriaBuilder.like(rootUser.get("firstName").as(String.class), "%" + firstName + "%");
            if (lastName != null)
                predicateLastName = criteriaBuilder.like(rootUser.get("lastName").as(String.class), "%" + lastName + "%");
            if (predicateFirstName != null && predicateLastName == null) {
                return criteriaBuilder.and(predicateFirstName);
            } else if (predicateFirstName == null && predicateLastName != null) {
                return criteriaBuilder.and(predicateLastName);
            } else if (predicateFirstName != null && predicateLastName != null) {
                return criteriaBuilder.or(predicateFirstName, predicateLastName);
            }
            return null;
        });

    }

}
