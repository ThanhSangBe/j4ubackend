package com.pts.j4u.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pts.j4u.entity.Job;

import java.util.List;


@Repository
public interface JobRepository extends JpaRepository<Job, Long>, JpaSpecificationExecutor<Job> {
	@Query(value = "SELECT j " + "FROM Job j INNER JOIN Company c ON j.company.id = c.id WHERE c.id = :companyId")
	Page<Job> findByCompanyId(@Param("companyId") Long companyId, Pageable pageable);

	@Query(value = "SELECT j FROM Job j INNER JOIN Status s ON j.status.id=s.id ORDER BY j.id DESC")
	Page<Job> findAll(Pageable pageable);

	@Query(value = "SELECT j FROM Job j INNER JOIN Status s ON j.status.id=s.id WHERE j.status.id = :statusId ORDER BY j.id DESC")
	Page<Job> findAllByStatusId(Pageable pageable, int statusId);
	@Query(nativeQuery = true,value = "SELECT MONTH(j.create_date),COUNT(j.id)\n" +
			"FROM j4u_db.job j \n" +
			"WHERE MONTH(j.create_date) <= MONTH(current_date()) AND MONTH(j.create_date) >=  MONTH(current_date()) - :numberMonth\n" +
			"GROUP BY MONTH(j.create_date);")
	List<Object[]> statisticByNumberLastMonth(@Param("numberMonth") Integer number);

}
