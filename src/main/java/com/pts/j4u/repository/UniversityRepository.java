package com.pts.j4u.repository;

import com.pts.j4u.entity.University;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UniversityRepository extends JpaRepository<University, Long> {
    @Query(value = "SELECT u FROM University u WHERE u.status.id=1 or u.status.id=4")
    Page<University> findAll(Pageable pageable);

    @Query(value = "SELECT u FROM University u WHERE u.name LIKE %:name% ")
    Page<University> findByName(Pageable pageable, @Param("name") String name);

    @Query(value = "SELECT u FROM University u WHERE u.status.id=:id")
    Page<University> findByStatusId(Pageable pageable, @Param("id") int statisId);

    @Query(value = "SELECT u FROM University u WHERE u.status.id=:id AND u.name LIKE %:name%")
    Page<University> findByStatusIdAndName(Pageable pageable, @Param("id") int statisId, @Param("name") String name);

    @Query(value = "SELECT u " +
            "FROM University u " +
            "INNER JOIN User user ON u.user.id=user.id " +
            "WHERE user.id=:id")
    Optional<University> findByUserId(@Param("id") long id);
}
