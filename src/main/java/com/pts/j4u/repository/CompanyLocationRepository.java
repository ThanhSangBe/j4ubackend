package com.pts.j4u.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pts.j4u.entity.CompanyLocation;

import java.util.List;

@Repository
public interface CompanyLocationRepository extends JpaRepository<CompanyLocation, Long> {
    @Query(value = "SELECT cl FROM CompanyLocation cl WHERE cl.company.id = :companyId")
    List<CompanyLocation> getByCompanyId(@Param("companyId") Long companyId);
}
