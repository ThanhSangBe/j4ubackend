package com.pts.j4u.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pts.j4u.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
	@Modifying
	@Query(value = "DELETE FROM UserRole c WHERE c.user.id=:id")
	void deleteByIdUser(@Param("id") Long id);
}
