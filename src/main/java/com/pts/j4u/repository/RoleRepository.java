package com.pts.j4u.repository;

import com.pts.j4u.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    @Query(value = "SELECT r FROM Role r, UserRole ur WHERE r.id = ur.role.id AND ur.user.id = :id")
    List<Role> getByUserId(@Param("id") Long id);

    @Query(value = "SELECT r.name FROM Role r, UserRole ur WHERE r.id = ur.role.id AND ur.user.id = :id")
    List<String> getNameByUserId(@Param("id") Long id);
}
