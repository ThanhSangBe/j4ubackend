package com.pts.j4u.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pts.j4u.entity.Candidate;

import java.util.List;
import java.util.Optional;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long>, JpaSpecificationExecutor<Candidate> {
    @Query(value = "SELECT c " +
            "FROM Candidate c JOIN FETCH c.user " +
            "WHERE c.user.firstName LIKE %:name% OR c.user.lastName LIKE %:name%")
    List<Candidate> findByFirstNameOrLastName(String name);
    @Query(value = "SELECT c " +
            "FROM Candidate c " +
            "INNER JOIN User u ON c.user.id=u.id " +
            "WHERE u.id=:id")
    Optional<Candidate> getByUserId(@Param("id") long userId);

}
