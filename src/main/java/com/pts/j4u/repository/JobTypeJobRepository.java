package com.pts.j4u.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pts.j4u.entity.JobTypeJob;

import java.util.List;

@Repository
public interface JobTypeJobRepository extends JpaRepository<JobTypeJob, Long> {
    @Query(value = "SELECT jt FROM JobTypeJob jt INNER JOIN Job j ON jt.job.id = j.id WHERE j.id = :jobId")
    List<JobTypeJob> findByJobId(@Param("jobId") Long jobId);

}
