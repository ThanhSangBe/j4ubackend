package com.pts.j4u.repository;

import com.pts.j4u.entity.Job;
import com.pts.j4u.entity.LikeJob;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikeJobRepository extends JpaRepository<LikeJob, Long> {
    @Modifying
    @Query(value = "DELETE " +
            "FROM LikeJob likeJob " +
            "WHERE likeJob.candidate.id=:candidateId AND likeJob.job.id=:jobId"
    )
//    @Query(value = "DELETE FROM LikeJob l " +
//            "WHERE l.id IN" +
//            "(SELECT l.id" +
//            "FROM Candidate c, Job j " +
//            "WHERE l.candidate.id = c.id AND l.job.id = j.id " +
//            "c.id=:candidateId AND j.id=:jobId " +
//            ")")
    void deleteByJobIdAndCandidateId(@Param("jobId") Long jobId, @Param("candidateId") Long candidateId);

    @Query(value = "SELECT j " +
                    "FROM LikeJob l " +
                    "INNER JOIN Candidate candidate ON l.candidate.id = candidate.id " +
                    "INNER JOIN Job j ON l.job.id = j.id " +
                    "WHERE candidate.id=:candidateId"
    )
    Page<Job> getJobLikedByCandidateId(@Param("candidateId") Long candidateId, Pageable pageable);
}
