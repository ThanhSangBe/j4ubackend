package com.pts.j4u.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UniversityCreateDTO  extends  UniversityDTO{
    private MultipartFile file;
}
