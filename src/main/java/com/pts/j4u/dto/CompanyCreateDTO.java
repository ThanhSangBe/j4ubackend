package com.pts.j4u.dto;

import com.pts.j4u.entity.Company;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CompanyCreateDTO  extends CompanyDTO {
    private MultipartFile file;
}
