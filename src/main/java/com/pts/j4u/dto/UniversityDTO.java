package com.pts.j4u.dto;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.pts.j4u.entity.Status;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UniversityDTO {
	private Long id;
	@NotEmpty(message = "{valid.name.notempty}")
	private String name;
	private String avatar;
	@NotEmpty(message = "{valid.shortname.notempty}")
	private String shortName;
	private String description;
	@NotEmpty(message = "{valid.website.notempty}")
	private String website;
	@Email(message = "{valid.email.incorrect}")
	private String email;
	private String phone;
	private List<LocationDTO> locations;
	private Status status;
}
