package com.pts.j4u.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.pts.j4u.entity.Role;
import com.pts.j4u.entity.Status;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDTO {
	private long id;
	@NotEmpty(message = "{valid.username.notempty}")
	private String username;
	private boolean gender;
	private String avatar;
	private String phone;
	private String firstName;
	private String lastName;
	@NotEmpty(message = "{valid.email.notempty}")
	@Email(message = "valid.email.incorrect")
	private String email;
	private Status status;
	private List<Role> role = new ArrayList<Role>();
}
