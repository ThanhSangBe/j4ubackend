package com.pts.j4u.dto;

import com.pts.j4u.entity.District;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LocationDTO {
	private Long id;
	private String address;
	private String note;
	private District district;
}
