package com.pts.j4u.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaginationDTO {
    private List<?> data;
    private boolean isFirst;
    private boolean isLast;
    private long totalPages;
    private long totalItems;
    private long sizeCurrentItems;
    private int numberOfCurrentPage;
    private int pageCurrent;

    public PaginationDTO(int pageSize, long lastPageNumber, int sizeCurrent, List<?> data, long totalItems, int pageCurrent)
    {
        this.isFirst = pageCurrent == 0 ? true : false;
        this.isLast = pageCurrent >= lastPageNumber ? true : false;
        this.totalPages = lastPageNumber;
        this.totalItems = totalItems;
        this.sizeCurrentItems = pageSize;
        this.numberOfCurrentPage = sizeCurrent;
        this.data = data;
        this.pageCurrent = pageCurrent;
    }
    public PaginationDTO(Page<?> page)
    {
        this.isFirst = page.isFirst();
        this.isLast = page.isLast();
        this.data = page.getContent();
        this.pageCurrent = page.getNumber();
        this.totalItems = page.getTotalElements();
        this.totalPages = page.getTotalPages();
    }

}
