package com.pts.j4u.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CompanyDTO {
	private long id;
	@NotEmpty(message = "{valid.name.notempty}")
	private String name;
	@NotEmpty(message = "{valid.email.notempty}")
	private String logo;
	private String description;
	@NotEmpty(message = "{valid.website.notempty}")
	private String website;
	@Email(message = "{valid.email.incorrect}")
	@NotEmpty(message = "{valid.email.notempty}")
	private String email;
	private String phone;
	private String tax;
	@NotEmpty(message = "{valid.code.notempty}")
	private String code;
	private List<LocationDTO> locations;
	private List<UserDTO> users;
	private StatusDTO status;
}
