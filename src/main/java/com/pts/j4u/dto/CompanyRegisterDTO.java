package com.pts.j4u.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CompanyRegisterDTO {
    private CompanyCreateDTO company;
    private UserRegisterDTO user;
}
