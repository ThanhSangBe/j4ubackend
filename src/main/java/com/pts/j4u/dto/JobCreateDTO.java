package com.pts.j4u.dto;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JobCreateDTO extends JobDTO {
    private String description;
    private String requirement;
    private String ortherInfo;
    private Long amount;
    private String files;
    private CompanySimpleDTO company;
}
