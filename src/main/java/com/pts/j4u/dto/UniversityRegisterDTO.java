package com.pts.j4u.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UniversityRegisterDTO implements Serializable {
    private UniversityCreateDTO university;
    private UserRegisterDTO user;
}
