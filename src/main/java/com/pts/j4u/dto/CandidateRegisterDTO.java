package com.pts.j4u.dto;

import org.springframework.web.multipart.MultipartFile;

import com.pts.j4u.entity.Major;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CandidateRegisterDTO {
	private Major major;
	private String cv;
	private MultipartFile fileCV;
	private UserRegisterDTO user;
}
