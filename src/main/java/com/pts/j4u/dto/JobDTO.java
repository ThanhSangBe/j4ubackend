package com.pts.j4u.dto;

import com.pts.j4u.entity.JobType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class JobDTO {
    private Long id;
    private String name;
    private String start;
    private String end;
    private List<JobType> jobTypes;
    private StatusDTO status;
    private Long salary;
}
