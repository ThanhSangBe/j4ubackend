package com.pts.j4u.dto;

import com.pts.j4u.entity.Major;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CandidateDTO {
	private Long id;
    private Major major;
    private String cv;
    private UserDTO userDTO;
}
