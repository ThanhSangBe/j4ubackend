package com.pts.j4u.dto;


import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserRegisterDTO extends UserDTO {
	private String password;
	private String confirmPassword;
	private MultipartFile file;
	
}
