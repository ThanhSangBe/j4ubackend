package com.pts.j4u.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LikeJobDTO {
    private long id;
    private LocalDateTime timeLike;
    private CandidateDTO candidate;
    private JobDTO job;
}
