package com.pts.j4u.dto;

import com.pts.j4u.entity.Status;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DemandDTO {
	private long id;
	private String name;
	private String desciption;
	private long amount;
	private String requirement;
	private String ortherInfo;
	private String startDate;
	private String endDate;
	private String files;
	private UniversityDTO universityDTO;
	private Status status;
}
