package com.pts.j4u.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApplyJobDTO {
	private long id;
	private LocalDateTime timeApply;
	private CandidateDTO candidate;
	private JobDTO job;
}
