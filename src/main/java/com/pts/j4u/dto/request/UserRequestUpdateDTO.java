package com.pts.j4u.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserRequestUpdateDTO implements Serializable {
    private boolean gender;
    private String avatar;
    private String phone;
    private String firstName;
    private String lastName;
    private String email;
}
