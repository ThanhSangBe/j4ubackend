package com.pts.j4u.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VerifyCodePasswordDTO implements Serializable {
    private String email;
    private String verifyCode;
    private String password;
}
