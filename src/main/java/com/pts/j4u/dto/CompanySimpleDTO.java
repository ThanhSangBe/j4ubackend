package com.pts.j4u.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CompanySimpleDTO {
    private Long id;
    private String name;
    private String code;
    private String email;
    private String website;
    private String logo;
    private List<JobDTO> jobs;
}
