package com.pts.j4u;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J4uApplication {

	public static void main(String[] args) {
		SpringApplication.run(J4uApplication.class, args);
	}

}
